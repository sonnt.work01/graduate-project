<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\Area;
use App\Models\Bill;
use App\Models\Admin;
use App\Models\AdminArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $admin_id = $request->session()->get('id');
        $admin = Admin::find($admin_id);
        $del_flag = $request->get('def_flag');

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }

        if ($del_flag == null) {
            $del_flag = 1;
        }
        // $listArea = AreaModel::where('del_flag', $del_flag)->get();
        // $listAdmin = Admin::where('role', 0)->where('del_flag', $del_flag)->get();
        // $listAdmin = Admin::all()->load('area');

        $listAdmin = AdminArea::join('admins', 'admins.id', '=', 'admin_areas.admin_id')
            ->join('areas', 'areas.id', '=', 'admin_areas.area_id')
            ->where('admins.role', 0)
            ->get();
        // dd($listAdmin);

        return view('admin_manager.admin_area.index', [
            'listAdmin' => $listAdmin,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        $admin_id_arr = array();
        $area_id_arr = array();
        $list = AdminArea::where('del_flag', 1)->get();
        // dd($list);
        foreach ($list as $value) {
            array_push($admin_id_arr, $value->admin_id);
        }
        foreach ($list as $value) {
            array_push($area_id_arr, $value->area_id);
        }
        $listAdmin = Admin::where('role', 0)->where('del_flag', 1)->whereNotIn('id', $admin_id_arr)->get();
        // $listAdmin = Admin::where('role', 0)->where('del_flag', 1)->get();
        // $listArea = Area::where('del_flag', 1)->whereNotIn('id', $area_id_arr)->get();
        $listArea = Area::where('del_flag', 1)->get();
        // dd($listAdmin);
        return view('admin_manager.admin_area.create', [
            'bill' => $bill,
            'admin' => $admin,
            'listArea' => $listArea,
            'listAdmin' => $listAdmin,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAdminXAreaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'admin_id' => 'required|',
            'area_id' => 'required|',
        ], [
            'admin_id.required' => "Nhân viên không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            // 'admin_id.unique' => "Nhân viên đã quản lý khu vực khác!",
            // 'area_id.unique' => "Khu vực đã có người quản lý!",
        ]);

        $admin_id = $request->get('admin_id');
        $count_admin = Admin::where('id', $admin_id)->where('del_flag', 1)->count();

        if ($count_admin == 0) {
            return redirect()->back()->with('error', 'Không tồn tại nhân viên này!');
        }

        $area_id = $request->get('area_id');
        $count_area = Area::where('id', $area_id)->where('del_flag', 1)->count();

        if ($count_area == 0) {
            return redirect()->back()->with('error', 'Không tồn tại khu vực này!');
        }

        $adminxarea = new AdminArea();
        $adminxarea->admin_id = $admin_id;
        $adminxarea->area_id = $area_id;
        $adminxarea->del_flag = 1;
        $adminxarea->save();
        return redirect()->back()->with('success', 'Thêm thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AdminXArea  $adminXArea
     * @return \Illuminate\Http\Response
     */
    public function show(AdminArea $adminXArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AdminXArea  $adminXArea
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // try {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }

        $admin_id_arr = array();
        $area_id_arr = array();
        $adminxarea = AdminArea::where('admin_id', $id)->firstOrFail();

        $list = AdminArea::where('del_flag', 1)->get();
        foreach ($list as $value) {
            array_push($admin_id_arr, $value->admin_id);
        }
        $area_id = $request->get('area_id');
        foreach ($list as $value) {
            array_push($area_id_arr, $value->area_id);
        }
        // $listAdmin = Admin::where('role', 0)->where('del_flag', 1)->whereNotIn('id', $admin_id_arr)->get();
        // $listArea = Area::where('del_flag', 1)->whereNotIn('id', $area_id_arr)->get();
        // dd($list);
        $listAdmin = Admin::where('id', $id)->where('role', 0)->where('del_flag', 1)->get();
        $listArea = Area::where('del_flag', 1)->get();

        return view('admin_manager.admin_area.edit', [
            'adminxarea' => $adminxarea,
            'bill' => $bill,
            'admin' => $admin,
            'listArea' => $listArea,
            'listAdmin' => $listAdmin,
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAdminXAreaRequest  $request
     * @param  \App\Models\AdminXArea  $adminXArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'admin_id' => 'required|',
            'area_id' => 'required|',
        ], [
            'admin_id.required' => "Nhân viên không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            // 'admin_id.unique' => "Nhân viên đã quản lý khu vực khác!",
            // 'area_id.unique' => "Khu vực đã có người quản lý!",
        ]);

        $adminxarea = AdminArea::find($id);

        $admin_id = $request->get('admin_id');
        dd($admin_id);

        $count_admin = Admin::where('id', $admin_id)->where('del_flag', 1)->count();

        if ($count_admin == 0) {
            return redirect()->back()->with('error', 'Không tồn tại nhân viên này!');
        }

        $area_id = $request->get('area_id');
        $count_area = Area::where('id', $area_id)->where('del_flag', 1)->count();

        if ($count_area == 0) {
            return redirect()->back()->with('error', 'Không tồn tại khu vực này!');
        }

        $adminxarea->admin_id = $admin_id;
        $adminxarea->area_id = $area_id;
        $adminxarea->del_flag = 1;
        $adminxarea->save();
        return redirect()->back()->with('success', 'Thêm thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AdminXArea  $adminXArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminArea $adminXArea)
    {
        //
    }
}
