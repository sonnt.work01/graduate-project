<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\Bill;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Redirect;

class AdminAuthenticateController extends Controller
{
    public function indexLogin()
    {
        return view('admin_manager.auth.login');
    }

    public function login(LoginRequest $request)
    {
        // dd(1);
        if ($admin = Admin::where('email', $request->email)->first()) {
            // if ($request->password != $user->password) {
            if (!Hash::check($request->password, $admin->password)) {
                return redirect()->back()->with('error', 'Mật khẩu không đúng!');
            } else {
                $request->session()->put('id', $admin->id);
                $request->session()->put('role', $admin->role);
                return Redirect::route('dashboard');
            }
        } else {
            return redirect()->back()->with('error', 'Email không hợp le!');
        }
    }

    public function indexForgetPass()
    {
        return view('admin_manager.auth.forget_pass');
    }

    public function forgetPassProcess(Request $request)
    {
        $emailAdmin = $request->email;
        // kiểm tra trên DB 
        $check_email = Admin::where('email', $emailAdmin)->count();
        if ($check_email != 0) {
            MailController::sendRandomPasswordAdmin($emailAdmin);
            return Redirect::route('login-admin')->with('success', 'Mật khẩu mới đã được gửi đến Email của bạn!');
        } else {
            return Redirect::route('login-admin')->with('error', 'Lỗi, Email của bạn chưa được đăng ký với chúng tôi!');
        }
    }

    public function logOut(Request $request)
    {
        $request->session()->flush();
        return Redirect::route('login-admin');
    }


    public function indexChangePass(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if($admin->role == 0){
        //     $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        // }else{
        $bill = Bill::where('status', 1)->get();
        // }
        return view('admin_manager.profile_admin.change_password', [
            'bill' => $bill,
            'admin' => $admin,
        ]);
    }

    public function changePassAdmin(Request $request, $id)
    {
        //pass old
        $old_password = $request->old_password;
        //pass new
        $new_password = $request->new_password;
        $passNewHash = Hash::make($new_password);
        //pass repeat
        $repeat_new_password = $request->repeat_new_password;

        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        if (!Hash::check($old_password, $admin->password)) {
            return response()->json([
                'hasError' => true,
                'message' => 'Mật khẩu hiện tại không đúng!!!',
            ]);
        }
        
        $validated = $request->validate([
            'old_password' => 'required|',
            'new_password' => 'required|min:8',
        ], [
            'old_password.required' => "Mật khẩu hiện tại không được để trống!",
            'new_password.required' => "Mật khẩu mới không được để trống!",
            'new_password.min' => "Mật khẩu mới tối thiểu 8 ký tự!",
        ]);

        if ($id != $id_admin) {
            return redirect()->back()->with('error', 'Tài khoản không hợp lệ!!!');
        }

        if ($new_password == $repeat_new_password) {
            $passCustomer = Admin::find($id_admin);
            $passCustomer->password = $passNewHash;
            $passCustomer->save();
            return response()->json([
                'hasError' => false,
                'message' => 'Đổi mật khẩu thành công!!!',
            ]);
        } else {
            return response()->json([
                'hasError' => true,
                'message' => 'Mật khẩu xác nhận không đúng!!!',
            ]);
        }
    }
}
