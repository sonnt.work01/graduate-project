<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\Area;
use App\Models\Bill;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreAdminRequest;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $del_flag = $request->def_flag;
        if ($del_flag == null) {
            $del_flag = 1;
        }
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        $listAdmin = Admin::where('role', 0)->where('del_flag', $del_flag)->get();

        return view('admin_manager.admin.index', [
            'listAdmin' => $listAdmin,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        $listArea = Area::all();
        return view('admin_manager.admin.create', [
            'listArea' => $listArea,
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminRequest $request)
    {
        try {
            $passwordHas = Hash::make($request->password);
            
            $admin = new Admin();
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->password = $passwordHas;
            $admin->gender = $request->gender;
            $admin->phone = $request->phone;
            $admin->date_birth = $request->date_birth;
            $admin->address = $request->address;
            $admin->role = 0;
            $admin->del_flag = 1;
            $admin->save();
            return redirect()->back()->with('success', 'Thêm nhân viên thành công!');;
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Lỗi!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd(1);
    }

    public function edit(Request $request, $id)
    {
        try {
            $admin_edit = Admin::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = Admin::find($id_admin);
            // if ($admin->role == 0) {
            //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
            // } else {
                $bill = Bill::where('status', 1)->get();
            // }
            // $admin_edit = ListAdmin::find($id);
            $listArea = Area::all();
            return view('admin_manager.admin.edit', [
                'admin' => $admin,
                'listArea' => $listArea,
                'admin_edit' => $admin_edit,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::find($id);

        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'phone' => 'required|numeric|digits:10|regex:/(0)[0-9]{9}/',
            'date_birth' => 'required',
            'address' => 'required',
            'del_flag' => 'required',
        ], [
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'phone.digits' => "Số điện thoại không hợp lệ, chỉ có 10 chữ số!",
            'phone.max' => "Số điện thoại tối đa 10 số!",
            'phone.numeric' => "Số điện thoại chỉ được nhập ký tự dạng số!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);
        $admin->name = $request->name;
        $admin->date_birth = $request->date_birth;
        $admin->gender = $request->gender;
        $admin->phone = $request->phone;
        $admin->address = $request->address;
        $admin->del_flag = $request->del_flag;
        $admin->save();
        return Redirect::route('profile-admin')->with('success', 'Cập nhật thông tin thành công!');;
    }
}
