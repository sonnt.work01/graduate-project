<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\Area;
use App\Models\Bill;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $del_flag = $request->get('def_flag');
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        if ($del_flag == null) {
            $del_flag = 1;
        }
        $listArea = Area::where('del_flag',1)->get();
        // dd($listArea);

        return view('admin_manager.area.index', [
            'listArea' => $listArea,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $admin_id_arr = array();
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // $listArea = Area::where('del_flag', 1)->get();
        // foreach ($listArea as $value) {
        //     array_push($admin_id_arr, $value->admin_id);
        // }
        // $listAdmin = Admin::where('role', 0)->where('del_flag', 1)->whereNotIn('id', $admin_id_arr)->get();
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        return view('admin_manager.area.create', [
            'admin' => $admin,
            'bill' => $bill,
            // 'listAdmin' => $listAdmin,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'area_name' => 'required',
            // 'admin_id' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png',
            'diagram' => 'required|mimes:jpeg,jpg,png',
            'area_address' => 'required',
        ], [
            'area_name.required' => "Tên khu vực không được để trống!",
            // 'admin_id.required' => "Người quản lý không được để trống!",
            'image.required' => "Ảnh khu vực không được để trống!",
            'image.mimes' => "File ảnh không hợp lệ!",
            'diagram.required' => "Ảnh sơ đồ không được để trống!",
            'diagram.mimes' => "File sơ đồ không hợp lệ!",
            'area_address.required' => "Địa chỉ không được để trống!",
        ]);
        //image
        $image = $request->file('image');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $newImageName);
        //diagram
        $diagram = $request->file('diagram');
        $newDiagramName = time() . '.' . $diagram->getClientOriginalExtension();
        $request->diagram->move(public_path('images'), $newDiagramName);
        //
        $areaName = $request->get('area_name');
        $areaAddress = $request->get('area_address');
        $count_areaName = Area::where('area_name', 'like', "%$areaName%")->where('del_flag', 1)->count();

        if ($count_areaName != 0) {
            return redirect()->back()->with('error', 'Tên khu vực đã tồn tại!');
        }
        $area = new Area();
        $area->area_name = $areaName;
        $area->image_path = $newImageName;
        $area->diagram = $newDiagramName;
        $area->area_address = $areaAddress;
        $area->del_flag = 1;
        $area->save();
        return redirect()->back()->with('success', 'Thêm khu vực thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $area = Area::where('id', $id)->firstOrFail();

            $id_admin = $request->session()->get('id');
            $admin = Admin::find($id_admin);
            // if ($admin->role == 0) {
            //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
            // } else {
                $bill = Bill::where('status', 1)->get();
            // }

            return view('admin_manager.area.edit', [
                'area' => $area,
                'admin' => $admin,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'area_name' => 'required',
            // 'image' => 'required',
            // 'diagram' => 'required',
            'area_address' => 'required',
            'del_flag' => 'required',
        ], [
            'area_name.required' => "Tên khu vực không được để trống!",
            // 'image.required' => "Ảnh khu vực không được để trống",
            // 'diagram.required' => "Ảnh sơ đồ không được để trống",
            'area_address.required' => "Địa chỉ không được để trống!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $area = Area::find($id);

        $image = $request->file('image');
        $diagram = $request->file('diagram');
        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_image;
        }
        if ($diagram != '') {
            $diagram_name = time() . '.' . $diagram->getClientOriginalExtension();
            $request->diagram->move(public_path('images'), $diagram_name);
        } else {
            $diagram_name = $request->hidden_diagram;
        }

        $areaName = $request->get('area_name');
        $count_areaName = Area::where('area_name', 'like', "%$areaName%")->where('del_flag', 1)->where('id', '<>', $id)->count();

        if ($count_areaName != 0) {
            return redirect()->back()->with('error', 'Tên khu vực đã tồn tại!');
        }
        // $check_delflag = Bill::where('area_id', $id)->where('status', '1')->orwhere('area_id', $id)->where('active', '2')->count();
        $del_flag = $request->get('del_flag');
        // if ($check_delflag != 0 && $del_flag == 0) {
        //     return redirect()->back()->with('error', 'Không thể đổi trạng thái vì có hoá đơn chưa xử lý xong!');
        // }

        $area->area_name = $areaName;
        $area->area_address = $request->get('area_address');
        $area->del_flag = $del_flag;
        $area->image_path = $image_name;
        $area->diagram = $diagram_name;
        $area->save();
        return Redirect::route('area.index')->with('success', 'Cập nhật khu vực thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        //
    }
}
