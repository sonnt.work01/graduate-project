<?php

namespace App\Http\Controllers\Admin_manager;

use Carbon\Carbon;
use App\Models\Bill;
use App\Models\User;
use App\Models\Admin;
use App\Models\Event;
use App\Models\Pitch;
use App\Models\BillDetail;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $area_id = $request->area_id;
        // if ($area_id == null) {
        //     $area = AreaModel::first();
        //     $area_id = $area->id;
        // }
        $bill_active = $request->bill_active;
        if ($bill_active == '') {
            $bill_active = 1;
        }
        $isAccount = $request->isAccount;
        if ($isAccount == '') {
            $isAccount = 'account';
        }
        // dd($bill_active);
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->orderBy('id', 'DESC')->get();
        // dd($bill);
        // }
        $search = $request->search;
        if ($search == '') {
            if ($isAccount == 'account') {
                $listBill = Bill::select('bills.*', 'users.name as user_name', 'users.phone', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                    ->join('users', 'users.id', '=', 'bills.user_id')
                    ->where('bills.status', $bill_active)
                    ->orderBy('bills.created_at', 'DESC')
                    ->get();
            } else {
                $listBill = Bill::select('bills.*', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                    ->where('bills.status', $bill_active)
                    ->where('user_id', null)
                    ->orderBy('bills.created_at', 'DESC')
                    ->get();
            }
        } else {
            if ($isAccount == 'account') {
                $listBill = Bill::select('bills.*', 'users.name as user_name', 'users.phone', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                    ->join('users', 'users.id', '=', 'bills.user_id')
                    ->where('bills.status', $bill_active)
                    ->where('bills.created_at', 'like', "%$search%")
                    ->orderBy('bills.created_at', 'DESC')
                    ->get();
            } else {
                $listBill = Bill::select('bills.*', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                    ->where('bills.status', $bill_active)
                    ->where('bills.created_at', 'like', "%$search%")
                    ->where('user_id', null)
                    ->orderBy('bills.created_at', 'DESC')
                    ->get();
            }
        }

        // dd($listBill);
        return view('admin_manager.bill.index', [
            'listBill' => $listBill,
            'search' => $search,
            'admin' => $admin,
            // 'area' => $area,
            'area_id' => $area_id,
            'bill_active' => $bill_active,
            'isAccount' => $isAccount,
            'bill' => $bill,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $id)
    {
        $search = $request->get('search');

        $bill_active = $request->get('bill_active');
        if ($bill_active == '') {
            $bill_active = 1;
        }

        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        $listBill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->where('bill_details.bill_id', $id)
            ->get();

        if ($admin->role == 0) {
            $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        } else {
            $bill = Bill::where('status', 1)->get();
        }
        return view('admin_manager.bill.detail', [
            'listBill' => $listBill,
            'search' => $search,
            'admin' => $admin,
            'bill_active' => $bill_active,
            'bill' => $bill,
        ]);
    }

    public function update(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $bill = Bill::find($id);
        $payment_type = $request->payment_type;

        // if ($payment_type != 0 && $payment_type != 1) {
        //     return redirect()->back()->with('error', 'Lỗi!!! Không có hình thức thanh toán này!!!');
        // }
        /**
         * duyệt đơn
         */
        if ($bill->status == 1) {
            $bill->status = 2;
            $bill->admin_id = $id_admin;
            $bill->save();

            $billDetail = BillDetail::where('bill_id', $bill->id)->get();

            foreach ($billDetail as $item) {
                $pitch = Pitch::find($item->pitch_id);

                $event = new Event();
                $event->pitch_id = $pitch->id;
                $event->time_start = $item->time_start;
                $event->time_end = $item->time_end;
                $event->save();
            }

            if ($bill->user_id != null) {
                $customer = User::find($bill->user_id);

                $bill = Bill::where('user_id', $customer->id)
                    ->select('bills.*', 'users.name', 'admins.name as admin_name', 'users.email', 'users.phone', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                    ->join('users', 'users.id', '=', 'bills.user_id')
                    ->join('admins', 'admins.id', '=', 'bills.admin_id')
                    ->where('bills.status', 2)
                    ->orderBy('bills.created_at', 'DESC')
                    ->first();

                $billDetail = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->where('bill_details.bill_id', $bill->id)
                    ->get();

                $data["email"] = $customer->email;
                $data["title"] = "Back Khoa Sports";
                $data["bill"] = $bill;
                $data["billDetail"] = $billDetail;

                $pdf = PDF::loadView('customer_manager.mail.mail_order', $data);
                Mail::send('customer_manager.mail.mail_order', $data, function ($message) use ($data, $pdf) {
                    $message->to($data["email"], $data["email"])
                        ->subject($data["title"])
                        ->attachData($pdf->output(), "invoice.pdf");
                });
            }
            return Redirect::route('bill.index')->with('success', 'Hóa đơn ' . 'BKS' . $id . ' được duyệt thành công!');

            /**
             * huỷ đơn
             */
        } else if ($bill->status == 2) {
            $bill->status = 4;
            $bill->payment_type = $payment_type;
            $bill->admin_id = $id_admin;
            $bill->save();

            if ($bill->user_id != null) {
                $customer = User::find($bill->user_id);

                $point = $customer->points;

                $customer->points = $point + round($bill->total_price / 100);
                $customer->save();

                $bill = Bill::where('user_id', $customer->id)
                    ->select('bills.*', 'users.name', 'users.email', 'admins.name as admin_name', 'users.phone', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                    ->join('users', 'users.id', '=', 'bills.user_id')
                    ->join('admins', 'admins.id', '=', 'bills.admin_id')
                    ->where('bills.status', 4)
                    ->orderBy('bills.created_at', 'DESC')
                    ->first();

                $billDetail = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->where('bill_details.bill_id', $bill->id)
                    ->get();

                if (!$bill->bill_periodic_id) {
                    $data["email"] = $customer->email;
                    $data["title"] = "Back Khoa Sports";
                    $data["bill"] = $bill;
                    $data["billDetail"] = $billDetail;
                    $pdf = PDF::loadView('customer_manager.mail.mail_order_final', $data);

                    Mail::send('customer_manager.mail.mail_order_final', $data, function ($message) use ($data, $pdf) {
                        $message->to($data["email"], $data["email"])
                            ->subject($data["title"])
                            ->attachData($pdf->output(), "invoice.pdf");
                    });
                } else {
                    $data["email"] = $customer->email;
                    $data["title"] = "Back Khoa Sports";
                    $data["bill_periodic_data"] = $bill;
                    $data["billDetail_arr"] = $billDetail;
                    $pdf = PDF::loadView('customer_manager.mail.mail_periodic_final_single', $data);

                    Mail::send('customer_manager.mail.mail_periodic_final_single', $data, function ($message) use ($data, $pdf) {
                        $message->to($data["email"], $data["email"])
                            ->subject($data["title"])
                            ->attachData($pdf->output(), "invoice.pdf");
                    });
                }
            }
            return redirect()->back()->with('success', 'Hóa đơn đã được thanh toán!');
        } else {
            return redirect()->back()->with('error', 'Lỗi thao tác!!!');
        }
    }

    /**
     * Huỷ đơn
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $bill = Bill::find($id);

        if ($bill->status == 1) {
            $customer = User::find($bill->user_id);

            $point = $customer->points;

            $customer->points = $point + $bill->point;
            $customer->save();

            $bill->status = 3;
            $bill->admin_id = $id_admin;
            $bill->cancellation_reason = $request->cancellation_reason;
            $bill->save();
            return Redirect::route('bill.index')->with('success', 'Hóa đơn ' . 'BKS' . $id . ' đã được hủy!');
        } else {
            return Redirect::route('bill.index')->with('error', 'Lỗi thao tác!!!!');
        }
    }

    public function destroyPeriodicSingle(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $bill = Bill::find($id);

        if ($bill->status == 2) {
            $bill->status = 3;
            $bill->admin_id = $id_admin;
            $bill->cancellation_reason = $request->cancellation_reason;
            $bill->save();

            return Redirect::route('bill.index')->with('success', 'Hóa đơn ' . 'BKS' . $id . ' đã được hủy!');
        } else {
            return Redirect::route('bill.index')->with('error', 'Lỗi thao tác!!!!');
        }
    }

    public function exportPDF($id)
    {
        try {
            $customer = User::find($id);

            $bill = Bill::where('user_id', $id)
                ->select('bills.*', 'users.name', 'users.email', 'users.phone', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                ->join('users', 'users.id', '=', 'bills.user_id')
                ->where('bills.status', 1)
                ->orderBy('bills.created_at', 'DESC')
                ->first();

            $billDetail = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                ->where('bill_details.bill_id', $bill->id)
                ->get();

            $data["email"] = $customer->email;
            $data["title"] = "Back Khoa Sports";
            $data["bill"] = $bill;
            $data["billDetail"] = $billDetail;

            $pdf = PDF::loadView('customer_manager.mail.mail_order', $data);

            return $pdf->download('invoice.pdf');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function exportBillPDFAccount($id)
    {
        try {
            $bill = Bill::select('bills.*', 'users.name', 'admins.name as admin_name', 'users.email', 'users.phone', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                ->join('users', 'users.id', '=', 'bills.user_id')
                ->join('admins', 'admins.id', '=', 'bills.admin_id')
                ->where('bills.id', $id)
                ->first();

            $billDetail = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                ->where('bill_details.bill_id', $bill->id)
                ->get();

            $data["bill"] = $bill;
            $data["billDetail"] = $billDetail;

            if ($bill->status == 2) {
                $pdf = PDF::loadView('customer_manager.mail.mail_order', $data);
            } else if ($bill->status == 4) {
                $pdf = PDF::loadView('customer_manager.mail.mail_order_final', $data);
            }

            return $pdf->download('invoice.pdf');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function exportBillPDFNoAccount($id)
    {
        try {
            $bill = Bill::select('bills.*', 'admins.name as admin_name', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                ->join('admins', 'admins.id', '=', 'bills.admin_id')
                ->where('bills.id', $id)
                ->first();

            $billDetail = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                ->where('bill_details.bill_id', $bill->id)
                ->get();

            $data["bill"] = $bill;
            $data["billDetail"] = $billDetail;

            if ($bill->status == 2) {
                $pdf = PDF::loadView('customer_manager.mail.mail_order_no_account', $data);
            } else if ($bill->status == 4) {
                $pdf = PDF::loadView('customer_manager.mail.mail_order_no_account_final', $data);
            }
            return $pdf->download('invoice.pdf');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
