<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\Bill;
use App\Models\User;
use App\Models\Admin;
use App\Models\Event;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\BillDetail;
use App\Models\BillPeriodic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class BillPeriodicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $area_id = $request->area_id;
        // if ($area_id == null) {
        //     $area = AreaModel::first();
        //     $area_id = $area->id;
        // }
        $bill_active = $request->bill_active;
        if ($bill_active == '') {
            $bill_active = 1;
        }

        $isAccount = $request->isAccount;
        if ($isAccount == '') {
            $isAccount = 'account';
        }
        // dd($bill_active);
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
        $bill = BillPeriodic::where('status', 1)->orderBy('id', 'DESC')->get();
        // dd($bill);
        // }
        $search = $request->search;
        if ($search == '') {
            if ($isAccount == 'account') {
                $listBill = BillPeriodic::select('bill_periodics.*', 'users.name as user_name', 'users.phone')
                    ->join('users', 'users.id', '=', 'bill_periodics.user_id')
                    ->where('bill_periodics.status', $bill_active)
                    ->orderBy('bill_periodics.created_at', 'DESC')
                    ->get();
                } else {
                    $listBill = BillPeriodic::select('bill_periodics.*', 'bills.customer_name', 'bills.customer_phone')
                    ->join('bills', 'bills.bill_periodic_id', '=', 'bill_periodics.id')
                    ->where('bill_periodics.status', $bill_active)
                    ->where('bill_periodics.user_id', null)
                    ->orderBy('bill_periodics.created_at', 'DESC')
                    ->groupBy('bills.bill_periodic_id', 'bills.customer_name', 'bills.customer_phone')
                    ->groupBy('bills.bill_periodic_id', )
                    ->get();
            }
        } else {
            if ($isAccount == 'account') {
                $listBill = BillPeriodic::select('bill_periodics.*', 'users.name as user_name', 'users.phone')
                    ->join('users', 'users.id', '=', 'bill_periodics.user_id')
                    ->where('bill_periodics.status', $bill_active)
                    ->where('bill_periodics.created_at', 'like', "%$search%")
                    ->orderBy('bill_periodics.created_at', 'DESC')
                    ->get();
            } else {
                $listBill = BillPeriodic::select('bill_periodics.*', 'bills.customer_name', 'bills.customer_phone')
                    ->join('bills', 'bills.bill_periodic_id', '=', 'bill_periodics.id')
                    ->where('bill_periodics.status', $bill_active)
                    ->where('bill_periodics.created_at', 'like', "%$search%")
                    ->where('bill_periodics.user_id', null)
                    ->orderBy('bill_periodics.created_at', 'DESC')
                    ->groupBy('bills.bill_periodic_id', 'bills.customer_name', 'bills.customer_phone')
                    ->get();
            }
        }

        return view('admin_manager.bill.bill_periodic', [
            'listBill' => $listBill,
            'search' => $search,
            'admin' => $admin,
            'isAccount' => $isAccount,
            'area_id' => $area_id,
            'bill_active' => $bill_active,
            'bill' => $bill,
        ]);
    }

    public function detail(Request $request, $id)
    {
        $bill_active = $request->get('bill_active');
        if ($bill_active == '') {
            $bill_active = 1;
        }
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        $bill = Bill::select('bills.*')->where('bill_periodic_id', $id)
            ->join('bill_periodics', 'bill_periodics.id', '=', 'bills.bill_periodic_id')
            ->get();

        $listBill_arr = [];
        foreach ($bill as $key) {
            $listBill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                ->where('bill_details.bill_id', $key->id)
                ->first();
            array_push($listBill_arr, $listBill);
        }

        if ($admin->role == 0) {
            $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        } else {
            $bill = Bill::where('status', 1)->get();
        }
        return view('admin_manager.bill.bill_periodic_detail', [
            'listBill' => $listBill_arr,
            'admin' => $admin,
            'bill_active' => $bill_active,
            'bill' => $bill,
        ]);
    }

    public function exportBillPDFPeriodic($id)
    {
        try {

            $bill_periodic = BillPeriodic::find($id);

            $customer = User::find($bill_periodic->user_id);

            $bill_periodic_data = BillPeriodic::where('user_id', $customer->id)
                ->select('bill_periodics.*', 'users.name', 'admins.name as admin_name', 'users.email', 'users.phone')
                ->join('users', 'users.id', '=', 'bill_periodics.user_id')
                ->join('admins', 'admins.id', '=', 'bill_periodics.admin_id')
                ->orderBy('bill_periodics.created_at', 'DESC')
                ->first();

            $bill_data = Bill::where('user_id', $customer->id)
                ->where('bill_periodic_id', $bill_periodic_data->id)
                ->select('bills.*', 'users.name', 'admins.name as admin_name', 'users.email', 'users.phone')
                ->join('users', 'users.id', '=', 'bills.user_id')
                ->join('admins', 'admins.id', '=', 'bills.admin_id')
                ->orderBy('bills.created_at', 'DESC')
                ->get();

            $billDetail_arr = [];
            foreach ($bill_data as $key) {
                $billDetail_data = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->where('bill_details.bill_id', $key->id)
                    ->first();
                array_push($billDetail_arr, $billDetail_data);
            }

            $data["bill_periodic_data"] = $bill_periodic_data;
            $data["billDetail_arr"] = $billDetail_arr;

            if ($bill_periodic->status == 2) {
                $pdf = PDF::loadView('customer_manager.mail.mail_periodic_order', $data);
            } else if ($bill_periodic->status == 4) {
                $pdf = PDF::loadView('customer_manager.mail.mail_periodic_final', $data);
            }

            return $pdf->download('invoice.pdf');
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function exportBillPeriodicPDFNoAccount($id)
    {
        // try {
        $bill_periodic_data = BillPeriodic::where('bill_periodics.id', $id)
            ->select('bill_periodics.*', 'admins.name as admin_name')
            ->join('admins', 'admins.id', '=', 'bill_periodics.admin_id')
            ->orderBy('bill_periodics.created_at', 'DESC')
            ->first();

        $bill_data = Bill::where('bill_periodic_id', $bill_periodic_data->id)
            ->select('bills.*', 'admins.name as admin_name')
            ->join('admins', 'admins.id', '=', 'bills.admin_id')
            ->orderBy('bills.created_at', 'DESC')
            ->get();

        $infor_customer = Bill::where('bill_periodic_id', $bill_periodic_data->id)
            ->orderBy('bills.created_at', 'DESC')
            ->first();

        $billDetail_arr = [];
        foreach ($bill_data as $key) {
            $billDetail_data = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                ->where('bill_details.bill_id', $key->id)
                ->first();
            array_push($billDetail_arr, $billDetail_data);
        }

        $data["bill_periodic_data"] = $bill_periodic_data;
        $data["bill_data"] = $bill_data;
        $data["billDetail_arr"] = $billDetail_arr;
        $data["infor_customer"] = $infor_customer;

        if ($bill_periodic_data->status == 2) {
            $pdf = PDF::loadView('customer_manager.mail.mail_periodic_order_no_account', $data);
        } else if ($bill_periodic_data->status == 4) {
            $pdf = PDF::loadView('customer_manager.mail.mail_periodic_no_account_final', $data);
        }
        return $pdf->download('invoice.pdf');
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     // dd($th->getMessage());
        // }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBillPeriodicRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BillPeriodic  $billPeriodic
     * @return \Illuminate\Http\Response
     */
    public function show(Request $billPeriodic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BillPeriodic  $billPeriodic
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $billPeriodic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBillPeriodicRequest  $request
     * @param  \App\Models\BillPeriodic  $billPeriodic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $id_admin = $request->session()->get('id');
            $admin = Admin::find($id_admin);
            $bill_periodic = BillPeriodic::find($id);

            $bill = Bill::where('bill_periodic_id', $bill_periodic->id)->get();
            /**
             * duyệt đơn
             */
            if ($bill_periodic->status == 1) {
                $bill_periodic->status = 2;
                $bill_periodic->admin_id = $id_admin;
                $bill_periodic->save();


                foreach ($bill as $item) {
                    $item->status = 2;
                    $item->admin_id = $id_admin;
                    $item->save();

                    $billDetail = BillDetail::where('bill_id', $item->id)->first();

                    $event = new Event();
                    $event->pitch_id = $billDetail->pitch_id;
                    $event->time_start = $billDetail->time_start;
                    $event->time_end = $billDetail->time_end;
                    $event->save();
                }

                if ($bill_periodic->user_id != null) {
                    $customer = User::find($bill_periodic->user_id);

                    $bill_periodic_data = BillPeriodic::where('user_id', $customer->id)
                        ->select('bill_periodics.*', 'users.name', 'admins.name as admin_name', 'users.email', 'users.phone')
                        ->join('users', 'users.id', '=', 'bill_periodics.user_id')
                        ->join('admins', 'admins.id', '=', 'bill_periodics.admin_id')
                        ->where('bill_periodics.status', 2)
                        ->orderBy('bill_periodics.created_at', 'DESC')
                        ->first();

                    $bill_data = Bill::where('user_id', $customer->id)
                        ->where('bill_periodic_id', $bill_periodic_data->id)
                        ->select('bills.*', 'users.name', 'admins.name as admin_name', 'users.email', 'users.phone')
                        ->join('users', 'users.id', '=', 'bills.user_id')
                        ->join('admins', 'admins.id', '=', 'bills.admin_id')
                        ->where('bills.status', 2)
                        ->orderBy('bills.created_at', 'DESC')
                        ->get();

                    $billDetail_arr = [];
                    foreach ($bill_data as $key) {
                        $billDetail_data = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                            ->where('bill_details.bill_id', $key->id)
                            ->first();
                        array_push($billDetail_arr, $billDetail_data);
                    }

                    $data["email"] = $customer->email;
                    $data["title"] = "Back Khoa Sports";
                    $data["bill_periodic_data"] = $bill_periodic_data;
                    $data["bill_data"] = $bill_data;
                    $data["billDetail_arr"] = $billDetail_arr;

                    $pdf = PDF::loadView('customer_manager.mail.mail_periodic_order', $data);

                    Mail::send('customer_manager.mail.mail_periodic_order', $data, function ($message) use ($data, $pdf) {
                        $message->to($data["email"], $data["email"])
                            ->subject($data["title"])
                            ->attachData($pdf->output(), "invoice.pdf");
                    });
                }
                // return Redirect::route('bill.index')->with('success', 'Hóa đơn ' . 'BKS' . $id . ' được duyệt thành công!');
                return Redirect::route('bill-periodic')->with('success', 'Hóa đơn ' . 'BKSĐK' . $id . ' được duyệt thành công!');

                /**
                 * thanh toán
                 */
            } else if ($bill_periodic->status == 2) {
                $bill_periodic->status = 4;
                $bill_periodic->admin_id = $id_admin;
                $bill_periodic->save();

                if ($bill_periodic->user_id != null) {
                    $customer = User::find($bill_periodic->user_id);

                    $bill_periodic_data = BillPeriodic::where('user_id', $customer->id)
                        ->select('bill_periodics.*', 'users.name', 'admins.name as admin_name', 'users.email', 'users.phone')
                        ->join('users', 'users.id', '=', 'bill_periodics.user_id')
                        ->join('admins', 'admins.id', '=', 'bill_periodics.admin_id')
                        ->where('bill_periodics.status', 4)
                        ->orderBy('bill_periodics.created_at', 'DESC')
                        ->first();

                    $bill_data = Bill::where('bill_periodic_id', $bill_periodic_data->id)
                        ->select('bills.*', 'users.name', 'users.email', 'admins.name as admin_name')
                        ->join('users', 'users.id', '=', 'bills.user_id')
                        ->join('admins', 'admins.id', '=', 'bills.admin_id')
                        ->orderBy('bills.created_at', 'DESC')
                        ->get();

                    $billDetail_arr = [];
                    foreach ($bill_data as $key) {
                        $billDetail_data = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                            ->where('bill_details.bill_id', $key->id)
                            ->first();
                        array_push($billDetail_arr, $billDetail_data);
                    }

                    $data["email"] = $customer->email;
                    $data["title"] = "Back Khoa Sports";
                    $data["bill_periodic_data"] = $bill_periodic_data;
                    $data["bill_data"] = $bill_data;
                    $data["billDetail_arr"] = $billDetail_arr;

                    $pdf = PDF::loadView('customer_manager.mail.mail_periodic_final', $data);

                    Mail::send('customer_manager.mail.mail_periodic_final', $data, function ($message) use ($data, $pdf) {
                        $message->to($data["email"], $data["email"])
                            ->subject($data["title"])
                            ->attachData($pdf->output(), "invoice.pdf");
                    });
                }
                return Redirect::route('bill-periodic')->with('success', 'Hóa đơn đã được thanh toán!');
            } else {
                return Redirect::route('bill-periodic')->with('error', 'Lỗi thao tác!!!');
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BillPeriodic  $billPeriodic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $bill_periodic = BillPeriodic::find($id);

        if ($bill_periodic->status == 1) {
            $bill_periodic->status = 3;
            $bill_periodic->admin_id = $id_admin;
            $bill_periodic->cancellation_reason = $request->cancellation_reason;
            $bill_periodic->save();

            $bill_data = Bill::where('bill_periodic_id', $bill_periodic->id)
                ->get();

            foreach ($bill_data as $key) {
                $key->status = 3;
                $key->admin_id = $id_admin;
                $key->cancellation_reason = $request->cancellation_reason;
                $key->save();
            }
            return Redirect::route('bill-periodic')->with('success', 'Hóa đơn ' . 'BKSĐK' . $id . ' đã được hủy!');
        } else {
            return Redirect::route('bill-periodic')->with('error', 'Lỗi thao tác!!!!');
        }
    }
}
