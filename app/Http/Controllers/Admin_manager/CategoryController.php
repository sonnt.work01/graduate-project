<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\Bill;
use App\Models\Admin;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }

        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }

        $listCategory = Category::where('del_flag', $del_flag)->get();

        return view('admin_manager.category.index', [
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
            'listCategory' => $listCategory,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }

        return view('admin_manager.category.create', [
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'category_name' => 'required',
            'price' => 'required|numeric',
            'deposit' => 'required|numeric',
        ], [
            'category_name.required' => "Tên khu vực không được để trống!",
            'price.required' => "Giá không được để trống!",
            'price.numeric' => "Giá phải là số!",
            'deposit.required' => "Phần trăm đặt cọc không được để trống!",
            'deposit.numeric' => "Phần trăm đặt cọc phải là số!",
        ]);

        if($request->deposit < 0){
            return redirect()->back()->with('error', 'Phần trăm không được để số âm!!!');
        }
        $category = new Category();
        $category->category_name = $request->category_name;
        $category->price = $request->price;
        $category->deposit = $request->deposit;
        $category->del_flag = 1;
        $category->save();
        return redirect()->back()->with('success', 'Thêm thể loại thành công!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        $bill = Bill::where('status', 1)->get();

        $category = Category::where('id', $id)->first();
        // dd($category);
        return view('admin_manager.category.edit', [
            'admin' => $admin,
            'bill' => $bill,
            'category' => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'category_name' => 'required',
            'price' => 'required|numeric',
            'deposit' => 'required|numeric',
        ], [
            'category_name.required' => "Tên khu vực không được để trống!",
            'price.required' => "Giá không được để trống!",
            'price.numeric' => "Giá thể loại phải là số!",
            'deposit.required' => "Phần trăm đặt cọc không được để trống!",
            'deposit.numeric' => "Phần trăm đặt cọc phải là số!",
        ]);

        if($request->deposit < 0){
            return redirect()->back()->with('error', 'Phần trăm không được để số âm!!!');
        }
        
        $category = Category::find($id);

        $category->category_name = $request->category_name;
        $category->price = $request->price;
        $category->deposit = $request->deposit;
        $category->del_flag = $request->del_flag;
        $category->save();
        return redirect()->back()->with('success', 'Cập nhật thể loại thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
