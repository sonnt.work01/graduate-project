<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\Bill;
use App\Models\Admin;
use App\Models\Event;
use App\Models\Pitch;
use App\Models\BillDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminArea;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $today = date('Y-m-d');
        if ($time_start == '') {
            $time_start = $today;
        }
        if ($time_end == '') {
            $time_end = $today;
        }
        $order = Event::where('time_start', 'like', "%$today%")->get();
        // dd($order);

        $admin_id = $request->session()->get('id');
        $admin = Admin::find($admin_id);

        $bill = Bill::where('status', 1)->get();
        $listPitch = Pitch::where('del_flag', 1)->get();
        $arr_pitch = array();
        $arr_doanh_thu_san = array();
        $arr_name_pitch = array();
        $total_doanh_thu = 0;
        foreach ($listPitch as $value) {
            array_push($arr_pitch, $value->id);
            array_push($arr_name_pitch, $value->pitch_name);
            $doanh_thu_san = Bill::join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                ->where('bill_details.pitch_id', $value->id)
                ->where('bills.updated_at', '>=', "$time_start" . " 0:0:0")
                ->where('bills.updated_at', '<=', "$time_end" . " 23:59:59")
                ->where('status', 4)
                ->sum('total_price') +  Bill::join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                ->where('bill_details.pitch_id', $value->id)
                ->where('bills.updated_at', '>=', "$time_start" . ' 0:0:0')
                ->where('bills.updated_at', '<=', "$time_end" . ' 23:59:59')
                ->where('status', 2)
                ->sum('deposit');
            array_push($arr_doanh_thu_san, $doanh_thu_san);
            $total_doanh_thu += $doanh_thu_san;
        }

        $events = Event::all();
        $event = [];

        foreach ($events as $row) {
            $pitch = Pitch::find($row->pitch_id);
            $bill_detail = BillDetail::where('time_start', $row->time_start)
                ->where('time_end', $row->time_end)
                ->where('pitch_id', $row->pitch_id)
                ->first();

            // $url = "http://127.0.0.1:8000/bill-detail/$bill_detail->bill_id";
            // $enddate = $row->time_end."24:00:00";
            $event[] = \Calendar::event(
                $pitch->pitch_name,
                false,
                new \DateTime($row->time_start),
                new \DateTime($row->time_end),
                $row->id,
                // [
                //     'color' => $row->color,
                // ]
            );
        }

        $calendar = \Calendar::addEvents($event)
            ->setOptions([
                'contentHeight' => 450,
                // 'themeSystem' => 'bootstrap3',
                'header' => [
                    'left' => 'prev,next today',
                    'center' => 'title',
                    'right' => 'month,agendaWeek,agendaDay,listMonth'
                ],
                'navLinks' => true,
                'editable' => true,
                'weekNumbers' => true,
                'eventLimit' => true,
                'eventClick' => true,
            ]);

        return view('admin_manager.dashboard.index', compact('events', 'calendar', 'arr_pitch', 'arr_doanh_thu_san', 'arr_name_pitch'), [
            'time_end' => $time_end,
            'time_start' => $time_start,
            'admin' => $admin,
            'bill' => $bill,
            'today' => $today,
            'order' => $order,
            'total_doanh_thu' => $total_doanh_thu
        ]);
    }

    public function chartFilter(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $date_start = $request->date_start;
        $date_end = $request->date_end;
        $today = date('Y-m-d');
        if ($date_start == '') {
            $date_start = $today;
        }
        if ($date_end == '') {
            $date_end = $today;
        }

        if ($date_end < $date_start) {
            return response()->json([
                'hasError' => true,
                'message' => 'Ngày kết thúc phải muộn hơn hoặc bằng ngày bắt đầu!',
            ]);
        }

        $listPitch = Pitch::where('del_flag', 1)->get();
        $arr_pitch = array();
        $arr_doanh_thu_san = array();
        $arr_name_pitch = array();
        $total_doanh_thu = 0;
        foreach ($listPitch as $value) {
            array_push($arr_pitch, $value->id);
            array_push($arr_name_pitch, $value->pitch_name);
            $doanh_thu_san = Bill::join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                ->where('bill_details.pitch_id', $value->id)
                ->where('bills.updated_at', '>=', "$date_start . ' 0:0:0'")
                ->where('bills.updated_at', '<=', "$date_end . ' 23:59:59'")
                ->where('status', 4)
                ->sum('total_price') +  Bill::join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                ->where('bill_details.pitch_id', $value->id)
                ->where('bills.updated_at', '>=', "$date_start . ' 0:0:0'")
                ->where('bills.updated_at', '<=', "$date_end" . ' 23:59:59')
                ->where('status', 2)
                ->sum('deposit');
            array_push($arr_doanh_thu_san, $doanh_thu_san);
            $total = $total_doanh_thu += $doanh_thu_san;
            $total_format = number_format($total, 0, '', ',') . 'đ';
        }

        return response()->json([
            'hasError' => false,
            'arr_doanh_thu_san' => $arr_doanh_thu_san,
            'total_format' => $total_format,
            'arr_pitch' => $arr_pitch,
            'arr_name_pitch' => $arr_name_pitch,
        ]);
    }
}
