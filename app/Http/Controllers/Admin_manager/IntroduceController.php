<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\Bill;
use App\Models\Admin;
use App\Models\Introduce;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class IntroduceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }

        $listIntroduce = Introduce::where('del_flag', 1)->get();

        return view('admin_manager.introduce.index', [
            'listIntroduce' => $listIntroduce,
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }
        return view('admin_manager.introduce.create', [
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'company' => 'required|max:255',
            'avatar' => 'required|mimes:jpeg,jpg,png',
            'content' => 'required',
            'address' => 'required',
            'date_submitted' => 'required'
        ], [
            'company.required' => "Tên công ty không được để trống!",
            'company.max' => "Tên công ty không được lớn hớn 255 ký tự!",
            'avatar.required' => "Bạn chưa chọn ảnh!",
            'avatar.mimes' => "File ảnh không hợp lệ!",
            'content.required' => "Nội dung không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
            'date_submitted.required' => "Bạn chưa chọn ngày!"
        ]);
        //image
        $image = $request->file('avatar');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->avatar->move(public_path('images'), $newImageName);
        //
        $company = $request->company;
        $content = $request->content;
        $address = $request->address;
        $date_submitted = $request->date_submitted;

        $introduce = new Introduce();
        $introduce->company = $company;
        $introduce->avatar = $newImageName;
        $introduce->content = $content;
        $introduce->address = $address;
        $introduce->date_submitted = $date_submitted;
        $introduce->del_flag = 1;
        $introduce->save();
        return redirect()->back()->with('success', 'Thêm thông tin thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $introduce = Introduce::where('id', $id)->firstOrFail();

    //     return response()->json([
    //         'introduce' => $introduce,
    //         'code' => 200,
    //     ], 200);
    // }
    public function edit(Request $request, $id)
    {
        try {
            $introduce = Introduce::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = Admin::find($id_admin);
            // if ($admin->role == 0) {
            //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
            // } else {
                $bill = Bill::where('status', 1)->get();
            // }
            // $introduce = IntroduceModel::find($id);
            return view('admin_manager.introduce.edit', [
                'introduce' => $introduce,
                'admin' => $admin,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'company' => 'required|max:255',
            // 'avatar' => 'required',
            'content' => 'required',
            'address' => 'required',
            'date_submitted' => 'required',
            'del_flag' => 'required',
        ], [
            'company.required' => "Tên công ty không được để trống!",
            'company.max' => "Tên công ty không được lớn hớn 255 ký tự!",
            // 'avatar.required' => "Bạn chưa chọn ảnh",
            'content.required' => "Nội dung không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
            'date_submitted.required' => "Bạn chưa chọn ngày!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $introduce = Introduce::find($id);

        $image = $request->file('avatar');
        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->avatar->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_avatar;
        }

        $introduce->company = $request->company;
        $introduce->content = $request->content;
        $introduce->address = $request->address;
        $introduce->date_submitted = $request->date_submitted;
        $introduce->del_flag = $request->del_flag;
        $introduce->avatar = $image_name;
        $introduce->save();
        return Redirect::route('introduce.index')->with('success', 'Cập nhật thông tin thành công!');
    }
}
