<?php

namespace App\Http\Controllers\Admin_manager;

use Carbon\Carbon;

use App\Models\Bill;
use App\Models\Admin;
use App\Models\Pitch;
use App\Models\BillDetail;
use App\Models\Maintenance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }

        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }

        $listMaintenances = Maintenance::select('maintenances.*', 'pitches.pitch_name',)
            ->join('pitches', 'pitches.id', '=', 'maintenances.pitch_id')
            ->where('maintenances.del_flag', $del_flag)->get();

        return view('admin_manager.maintenances.index', [
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
            'listMaintenances' => $listMaintenances,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }
        $listPitch = Pitch::all();

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        return view('admin_manager.maintenances.create', [
            'admin' => $admin,
            'bill' => $bill,
            'listPitch' => $listPitch,
            'today' => $today,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pitch_id' => 'required',
            'description' => 'required|',
            'time_start' => 'required|',
            'time_end' => 'required|',
        ], [
            'pitch_id.required' => "Chưa chọn sân!",
            'description.required' => "Mô tả không được để trống!",
            'time_start.required' => "Chưa chọn thời gian bắt đầu!",
            'time_end.required' => "Chưa chọn thời gian kết thúc!",
        ]);

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        $time_start = Carbon::parse($request->time_start);
        $time_end = Carbon::parse($request->time_end);

        $bill_7 = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'pitches.id')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->join('bills', 'bills.id', '=', 'bill_details.bill_id')

            ->where('pitches.del_flag', 1)
            ->where('pitches.id', $request->pitch_id)
            ->where('pitches.category_id', 1)
            ->where('bills.status', '<>', 3)
            ->where('bill_details.time_start', '<=', $time_start)
            ->where('bill_details.time_end', '>=', $time_start)

            ->orWhere('bill_details.time_start', '<=', $time_end)
            ->where('bill_details.time_end', '>=', $time_end)
            ->where('pitches.del_flag', 1)
            ->where('pitches.id', $request->pitch_id)
            ->where('pitches.category_id', 1)
            ->where('bills.status', '<>', 3)

            ->orWhere('bill_details.time_start', '>=', $time_start)
            ->where('bill_details.time_end', '<=', $time_end)
            ->where('pitches.del_flag', 1)
            ->where('pitches.id', $request->pitch_id)
            ->where('pitches.category_id', 1)
            ->where('bills.status', '<>', 3)
            ->get();

        $bill_11 = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'pitches.category_id', 'pitches.id')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
            ->where('pitches.category_id', 2)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)
            ->where('bill_details.time_start', '<=', $time_start)
            ->where('bill_details.time_end', '>=', $time_start)

            ->orWhere('bill_details.time_start', '<=', $time_end)
            ->where('bill_details.time_end', '>=', $time_end)
            ->where('pitches.category_id', 2)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)

            ->orWhere('bill_details.time_start', '>=', $time_start)
            ->where('bill_details.time_end', '<=', $time_end)
            ->where('pitches.category_id', 2)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)
            ->get();

        if ($time_start > $time_end) {
            return redirect()->back()->with('error', 'Thời gian bắt đầu không được nhỏ hơn thời gian hoàn thành!!!');
        }

        if(count($bill_7) != 0 || count($bill_11) != 0){
            return redirect()->back()->with('error', 'Có sân đã đặt hoặc đang đá nên không thể lên lịch bảo trì. Trong trường hợp cần phải bảo trì sân, phải xử lý hoặc huỷ tất cả hoá đơn trùng với thời gian bảo trì!!!');
        }

        $maintenance = new Maintenance();
        $maintenance->pitch_id = $request->pitch_id;
        $maintenance->description = $request->description;
        $maintenance->time_start = $time_start;
        $maintenance->time_end = $time_end;
        $maintenance->del_flag = 1;
        $maintenance->save();
        return redirect()->back()->with('success', 'Đã thêm bảo trì mới!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Maintenance  $maintenance
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }
        $maintenances = Maintenance::select('maintenances.*', 'pitches.pitch_name')
            ->join('pitches', 'pitches.id', '=', 'maintenances.pitch_id')
            ->where('maintenances.id', $id)
            ->first();
        // dd($maintenances);
        $listPitch = Pitch::all();

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        return view('admin_manager.maintenances.edit', [
            'admin' => $admin,
            'bill' => $bill,
            'maintenances' => $maintenances,
            'listPitch' => $listPitch,
            'today' => $today,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Maintenance  $maintenance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'pitch_id' => 'required',
            'description' => 'required|',
            'time_start' => 'required|',
            'time_end' => 'required|',
        ], [
            'pitch_id.required' => "Chưa chọn sân!",
            'description.required' => "Mô tả không được để trống!",
            'time_start.required' => "Chưa chọn thời gian bắt đầu!",
            'time_end.required' => "Chưa chọn thời gian kết thúc!",
        ]);


        $time_start = $request->time_start;
        $time_end = $request->time_end;
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        if ($time_start > $time_end) {
            return redirect()->back()->with('error', 'Thời gian bắt đầu không được nhỏ hơn thời gian hoàn thành!!!');
        }

        $maintenances = Maintenance::find($id);
        $maintenances->pitch_id = $request->pitch_id;
        $maintenances->description = $request->description;
        $maintenances->time_start = $time_start;
        $maintenances->time_end = $time_end;
        // $maintenances->del_flag = $request->del_flag;
        $maintenances->save();
        return redirect()->back()->with('success', 'Đã cập nhật thông tin bảo trì!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Maintenance  $maintenance
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $maintenance = Maintenance::find($id);
        // dd($maintenance);

        $maintenance->del_flag = 0;
        $maintenance->save();

        return redirect()->back()->with('success', 'Một sân đã bảo trì xong!');
    }
}
