<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;

use Carbon\Carbon;
use App\Models\Area;
use App\Models\Bill;
use App\Models\User;
use App\Models\Admin;
use App\Models\Pitch;
use App\Models\Introduce;
use App\Models\BillDetail;
use App\Models\Maintenance;
use App\Models\BillPeriodic;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class OrderAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        $search = $request->search;
        if ($search == '') {
            $search = $today;
        }

        $minDate1Month = strtotime('+1 month', strtotime($today));
        $minDate1Month = date('Y-m-d', $minDate1Month);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }

        $start = null;
        $end = null;

        $listPitch = Pitch::where('del_flag', 1)->get();
        $array_pitch = [];
        // $listBill = BillModel::all();
        // $listTime = TimeModel::where('del_flag', 1)->get();

        $listPitchTime = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'statuses.status_name', 'statuses.price_change', 'areas.area_name', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->join('areas', 'areas.id', '=', 'pitches.area_id')
            ->where('pitches.del_flag', 1)
            ->where('pitches.area_id', 1)->get();

        $listTime = Pitch::select('pitches.*', 'bill_details.time_start', 'bill_details.time_end')
            ->join('bill_details', 'bill_details.pitch_id', '=', 'pitches.id')
            ->where('pitches.del_flag', 1)
            ->where('bill_details.time_start', 'like', "%$search%")
            ->where('pitches.area_id', 1)
            ->orderBy('bill_details.time_start', 'ASC')
            ->get();

        $pitch = new Pitch();
        $arr_time = $pitch->arrTime();
        return view('admin_manager.order_admin.index', [
            'listPitchTime' => $listPitchTime,
            'listTime' => $listTime,
            'listPitch' => $listPitch,
            'search' => $search,
            'today' => $today,
            'array_pitch' => $array_pitch,
            'start' => $start,
            'end' => $end,
            'admin' => $admin,
            'bill' => $bill,
            'minDate1Month' => $minDate1Month,
            'arr_time' => $arr_time,
        ]);
    }

    public function pitchFilter(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        $date = $request->date;
        $time_order = $request->time_order_normal;
        $time = explode("-", $time_order);
        $start_in_arr = $time[0];
        $end_in_arr = $time[1];

        $pitchModel = new Pitch();
        $checkTime = $pitchModel->checkTime($start_in_arr, $end_in_arr);

        if (!$checkTime) {
            return response()->json([
                'hasError' => true,
                'message' => 'Lỗi khung thời gian!!!',
            ]);
        }

        $time_start = Carbon::parse($start_in_arr . $date);
        $time_end = Carbon::parse($end_in_arr . $date);

        $start = $start_in_arr;
        $end = $end_in_arr;

        $time_now = Carbon::now();
        if ($time_start < $time_now) {
            return response()->json([
                'hasError' => true,
                'message' => 'Khung giờ đã quá muộn so với thời gian hiện tại!!!',
            ]);
        }

        if ($date < $today) {
            return response()->json([
                'hasError' => true,
                'message' => 'Chọn ngày phải lớn hơn hoặc bằng ngày hôm nay!!!',
            ]);
        }

        $listPitch = Pitch::where('del_flag', 1)->get();
        $pitchModel2 = new Pitch(); 
        list($bill_7, $maintenances_7, $bill_11, $maintenances_11) = $pitchModel2->fillPitch($time_start, $time_end);

        $array_pitch = [];
        if (count($bill_11) != 0 || count($maintenances_11) != 0) {
            foreach ($listPitch as $data) {
                array_push($array_pitch, $data->id);
            }
        } else {
            foreach ($bill_7 as $data) {
                array_push($array_pitch, $data->pitch_id);
            }
            foreach ($maintenances_7 as $data) {
                array_push($array_pitch, $data->pitch_id);
            }
        }

        return response()->json([
            'hasError' => false,
            'array_pitch' => $array_pitch,
            'date' => $date,
            'start' => $start,
            'end' => $end,
            'listPitch' => $listPitch,
            'message' => 'Success',
        ]);
    }

    public function filterOrderPeriodic(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        $first_date = $request->date_start_periodic;
        $date_end_periodic = $request->date_end_periodic;

        $time_order = $request->time_order_periodic;
        $time = explode("-", $time_order);
        $start_in_arr = $time[0];
        $end_in_arr = $time[1];
        //time validate

        $pitchModel = new Pitch();
        $checkTime = $pitchModel->checkTime($start_in_arr, $end_in_arr);

        if (!$checkTime) {
            return response()->json([
                'hasError' => true,
                'message' => 'Lỗi khung thời gian!!!',
            ]);
        }

        // đặt tối thiểu 1 tháng
        $minDate1Month = strtotime('+1 month', strtotime($first_date));
        $minDate1Month = date('Y-m-d', $minDate1Month);

        $start = $start_in_arr;
        $end = $end_in_arr;

        if ($first_date < $today) {
            return response()->json([
                'hasError' => true,
                'message' => 'Chọn ngày phải lớn hơn hoặc bằng ngày hôm nay!!!',
            ], 225);
        }

        if ($date_end_periodic < $minDate1Month) {
            return response()->json([
                'hasError' => true,
                'message' => 'Đặt tối thiểu ít nhất 1 tháng từ ngày bắt đầu!!!',
            ], 226);
        }

        $arr_date_order = [];
        for ($i = $first_date; $i <= $date_end_periodic; $i = date('Y-m-d', strtotime('+1 day', strtotime($i)))) {
            $dayOfTheWeek = Carbon::parse($i)->dayOfWeek;
            if (Carbon::parse($request->weekday)->dayOfWeek == $dayOfTheWeek) {
                array_push($arr_date_order, $i);
            }
        }

        $date_arr = [];
        $array_pitch = [];
        foreach ($arr_date_order as $key) {
            $date = date_format(date_create($key), "Y-m-d");
            array_push($date_arr, $date);

            // var_dump($date);
            $time_start_periodic = Carbon::parse($start_in_arr . $date);
            $time_end_periodic = Carbon::parse($end_in_arr . $date);

            $time_now = Carbon::now();
            if ($time_start_periodic < $time_now) {
                return response()->json([
                    'hasError' => true,
                    'message' => 'Lỗi!!! Đã qua khung giờ bạn chọn trong ngày ' . $date . ' !!!',
                ]);
            }

            $listPitch = Pitch::where('del_flag', 1)->get();
            // dd($time_start->toDateString());
            $pitchModel2 = new Pitch();

            list($bill_7, $maintenances_7, $bill_11, $maintenances_11) = $pitchModel2->fillPitch($time_start_periodic, $time_end_periodic);

            if (count($bill_11) != 0 || count($maintenances_11) != 0) {
                foreach ($listPitch as $data) {
                    array_push($array_pitch, $data->id);
                }
            } else {
                foreach ($bill_7 as $data) {
                    array_push($array_pitch, $data->pitch_id);
                }
                foreach ($maintenances_7 as $data) {
                    array_push($array_pitch, $data->pitch_id);
                }
            }
        }

        return response()->json([
            'hasError' => false,
            'weekday' => $request->weekday,
            'first_date' => $first_date,
            'array_pitch' => $array_pitch,
            'date' => $date_arr,
            'second_date' => $date_end_periodic,
            'listPitch' => $listPitch,
            'message' => 'Success',
            'start' => $start,
            'end' => $end,
        ]);
    }

    public function formOrder(Request $request, $area_id, $pitch_id)
    {
        try {
            $array_time = array();
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $today = date('Y-m-d');

            $id_admin = $request->session()->get('id');
            $admin = Admin::find($id_admin);

            $introduce = Introduce::where('del_flag', 1)->get();
            $listArea = Area::where('del_flag', 1)->get();
            $customer = User::where('del_flag', 1)->get();

            $bill = Bill::where('status', 1)->get();

            $date = $request->date;
            $time_start = $request->time_start;
            $time_end = $request->time_end;

            $area = Area::where('id', $area_id)->first();
            if (!$area) {
                redirect()->back()->with('error', 'Khu vực không hợp lệ!');
            }

            $pitch = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'categories.deposit', 'statuses.status_name', 'statuses.price_change', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `total_price`"))
                ->join('categories', 'categories.id', '=', 'pitches.category_id')
                ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
                ->join('areas', 'areas.id', '=', 'pitches.area_id')
                ->where('pitches.del_flag', 1)
                ->where('pitches.area_id', $area->id)
                ->where('pitches.id', $pitch_id)->first();

            $weekday = $request->weekday_periodic;
            //đặt thường
            if ($weekday == null) {
                //check pitch id
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitch_id', $pitch_id)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', Carbon::parse($time_start . $date))
                    ->where('bill_details.time_end', '=', Carbon::parse($time_end . $date))
                    ->get();

                $array_pitch = [];
                foreach ($bill as $data) {
                    array_push($array_pitch, $data->pitch_id);
                }
                if (in_array($pitch_id, $array_pitch)) {
                    return Redirect::route('order-admin')->with('error', $pitch->pitch_name . ', khung giờ ' . $time_start . ' - ' . $time_end . ' bạn chọn đã có người đặt!');
                }

                $start = Carbon::parse($time_start . $date);
                $end = Carbon::parse($time_end . $date);
                $minute = $start->diffInMinutes($end);

                $show_final_price = $pitch->price + ($pitch->price * $pitch->price_change / 100);

                // $show_final_price = $check_total / 60 * $minute;

                return view('admin_manager.order_admin.form', [
                    'admin' => $admin,
                    'customer' => $customer,
                    'pitch' => $pitch,
                    'date' => $date,
                    'weekday' => $weekday,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'area' => $area,
                    'today' => $today,
                    'introduce' => $introduce,
                    'listArea' => $listArea,
                    'minute' => $minute,
                    'show_final_price' => $show_final_price,
                    'bill' => $bill,
                ]);
            } else {
                //đặt định kỳ
                $first_date = $request->date_periodic_start;
                $second_date = $request->date_periodic_end;
                $weekday_periodic = $request->weekday_periodic;

                $arr_date_order = [];
                for ($i = $first_date; $i <= $second_date; $i = date('Y-m-d', strtotime('+1 day', strtotime($i)))) {
                    $dayOfTheWeek = Carbon::parse($i)->dayOfWeek;
                    if (Carbon::parse($weekday_periodic)->dayOfWeek == $dayOfTheWeek) {
                        array_push($arr_date_order, $i);
                    }
                }
                $date_arr = [];
                $array_pitch = [];
                foreach ($arr_date_order as $key) {
                    $date = date_format(date_create($key), "Y-m-d");
                    array_push($date_arr, $date);

                    $time_start_periodic = Carbon::parse($request->time_start_periodic . $date);
                    $time_end_periodic = Carbon::parse($request->time_end_periodic . $date);

                    $listPitch = Pitch::where('del_flag', 1)->get();

                    $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitch_id', $pitch_id)
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '=', $time_start_periodic)
                        ->where('bill_details.time_end', '=', $time_end_periodic)
                        ->get();

                    if (count($bill) != 0) {
                        foreach ($bill as $data) {
                            array_push($array_pitch, $data->pitch_id);
                            $timeError = $data->time_start . ' - ' . $data->time_end;
                        }
                    }
                }
                // dd($timeError);
                if (in_array($pitch_id, $array_pitch)) {
                    return Redirect::route('order-admin')->with('error', $pitch->pitch_name . ', khung giờ ' . $timeError . ' bạn chọn đã có người đặt!');
                }

                $start = Carbon::parse($time_start . $date);
                $end = Carbon::parse($time_end . $date);
                $minute = $start->diffInMinutes($end);

                $show_final_price = $pitch->price + ($pitch->price * $pitch->price_change / 100);

                // $show_final_price = $check_total / 60 * $minute;
                $date_return = implode("; ", $date_arr);
                $count_date_periodic = count($date_arr);
                $total_price = $show_final_price * $count_date_periodic;
                return view('admin_manager.order_admin.form', [
                    'admin' => $admin,
                    'customer' => $customer,
                    'pitch' => $pitch,
                    'date' => $date_return,
                    'weekday' => $weekday,
                    'first_date' => $first_date,
                    'second_date' => $second_date,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'area' => $area,
                    'today' => $today,
                    'introduce' => $introduce,
                    'listArea' => $listArea,
                    'minute' => $minute,
                    'show_final_price' => $show_final_price,
                    'bill' => $bill,
                    'count_date_periodic' => $count_date_periodic,
                    'total_price' => $total_price,
                ]);
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
            // dd($e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $weekday_periodic = $request->weekday_periodic;
        $payment_type = $request->payment_type;
        // if ($weekday_periodic == '') {
        //     $validated = $request->validate([
        //         'customer_id' => 'required|',
        //         'pitch_id' => 'required|',
        //         'day' => 'required',
        //         'time_start' => 'required',
        //         'time_end' => 'required',
        //     ], [
        //         'customer_id.required' => "Tên khách hàng không được để trống!",
        //         'pitch_id.required' => "Sân không được để trống!",
        //         'day.required' => "Ngày không được để trống!",
        //         'time_start.required' => "Thời gian bắt đầu không được để trống!",
        //         'time_end.required' => "Thời gian kết thúc không được để trống!",
        //     ]);
        // }
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        $customer_name = $request->customer_name;
        $customer_phone = $request->customer_phone;
        $check_account = $request->check_account;

        $day = $request->day;
        $time_start = Carbon::parse($request->time_start . $day);
        $time_end = Carbon::parse($request->time_end . $day);

        if ($payment_type == null) {
            return Redirect::route('order-admin')->with('error', 'Lỗi!!! Vui lòng chọn hình thức thanh toán!');
        }
        if ($time_start->diffInMinutes($time_end) < 60) {
            return Redirect::route('order-admin')->with('error', 'Lỗi!!! Thời gian kết thúc phải lớn hơn thời gian bắt đầu ít nhất 1 tiếng!');
        }
        if ($time_start->hour > $time_end->hour) {
            return Redirect::route('order-admin')->with('error', 'Lỗi!!! Thời gian kết thúc phải lớn hơn thời gian bắt đầu!');
        }

        $pitch_id = $request->pitch_id;
        $customer_id = $request->customer_id;

        $check = Pitch::select('pitches.*', 'statuses.price_change', 'categories.price', 'categories.deposit',)
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->where('pitches.id', $pitch_id)
            ->first();

        $total_price = $check->price + ($check->price * $check->price_change / 100);

        // $minute = $time_start->diffInMinutes($time_end);

        // $total_price = $total / 60 * $minute;

        $deposit = $total_price * $check->deposit / 100;
        $pitch_price = $check->price;
        $price_change = $check->price_change;

        // đặt thường
        if ($weekday_periodic == '') {
            // dd(0);
            if ($day < $today) {
                return Redirect::route('order-admin')->with('error', 'Lỗi!!! Chọn ngày phải lớn hơn hoặc bằng ngày hôm nay!');
            }
            if ($check->category_id == 1) {
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitch_id', $pitch_id)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();
                $maintenances_7 = Maintenance::Where('maintenances.time_start', '<', $time_start)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.time_end', '>', $time_start)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '<', $time_end)
                    ->where('maintenances.time_end', '>', $time_end)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '>', $time_start)
                    ->where('maintenances.time_end', '<', $time_end)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.del_flag', 1)
                    ->get();
                $bill_11 =  BillDetail::select('bill_details.*', 'pitches.pitch_name')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitches.category_id', 2)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();
            } else {
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();
                $bill_11 = $bill;
                $maintenances_11 = Maintenance::select('maintenances.*')
                    ->Where('maintenances.time_start', '<', $time_start)
                    ->where('maintenances.time_end', '>', $time_start)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '<', $time_end)
                    ->where('maintenances.time_end', '>', $time_end)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '>', $time_start)
                    ->where('maintenances.time_end', '<', $time_end)
                    ->where('maintenances.del_flag', 1)
                    ->get();
            }
            if (
                (count($bill) == 0 &&
                    $time_end > $time_start &&
                    count($bill_11) == 0 &&
                    isset($maintenances_7) &&
                    count($maintenances_7) == 0) ||
                (count($bill) == 0 &&
                    $time_end > $time_start &&
                    count($bill_11) == 0 &&
                    isset($maintenances_11) &&
                    count($maintenances_11) == 0)
            ) {
                //vãng lai
                if ($check_account == 1) {
                    $bill = new Bill();
                    $bill->customer_name = $customer_name;
                    $bill->customer_phone = $customer_phone;
                    $bill->deposit = $deposit;
                    $bill->total_price = $total_price;
                    $bill->status = 1;
                    $bill->point = 0;
                    $bill->payment_type = $payment_type;
                    $bill->save();

                    // dd($bill->id);
                    $billDetail = new BillDetail();
                    $billDetail->bill_id = $bill->id;
                    $billDetail->pitch_id = $pitch_id;
                    $billDetail->time_start = $time_start;
                    $billDetail->time_end = $time_end;
                    $billDetail->pitch_price = $pitch_price;
                    $billDetail->price_change = $price_change;
                    $billDetail->save();

                    return redirect()->route('order-admin-confirm', [$customer_phone]);
                } else {
                    //có tài khoản
                    $bill = new Bill();
                    $bill->user_id = $customer_id;
                    $bill->deposit = $deposit;
                    $bill->total_price = $total_price;
                    $bill->status = 1;
                    $bill->point = 0;
                    $bill->payment_type = $payment_type;
                    $bill->save();

                    $billDetail = new BillDetail();
                    $billDetail->bill_id = $bill->id;
                    $billDetail->pitch_id = $pitch_id;
                    $billDetail->time_start = $time_start;
                    $billDetail->time_end = $time_end;
                    $billDetail->pitch_price = $pitch_price;
                    $billDetail->price_change = $price_change;
                    $billDetail->save();

                    return redirect()->route('order-admin-confirm', [$customer_id]);
                }
            } else {
                return Redirect::route('order-admin')->with('error', $check->pitch_name . ', khung giờ ' . $time_start . ' - ' . $time_end . ' bạn chọn đã có người đặt!');
            }
        } else {
            // đặt định kì

            // dd(1);
            $first_date = $request->first_date_periodic;
            $second_date = $request->second_date_periodic;
            $weekday_periodic = $request->weekday_periodic;

            $time_end = $request->time_end;
            $time_start = $request->time_start;

            $arr_date_order = [];
            for ($i = $first_date; $i <= $second_date; $i = date('Y-m-d', strtotime('+1 day', strtotime($i)))) {
                $dayOfTheWeek = Carbon::parse($i)->dayOfWeek;
                if (Carbon::parse($weekday_periodic)->dayOfWeek == $dayOfTheWeek) {
                    array_push($arr_date_order, $i);
                }
            }

            $date_arr = [];
            foreach ($arr_date_order as $key) {
                $date = date_format(date_create($key), "Y-m-d");

                $time_start_periodic = Carbon::parse($time_start . $date);
                $time_end_periodic = Carbon::parse($time_end . $date);

                if ($check->category_id == 1) {
                    $bill_7 = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitch_id', $pitch_id)
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '=', $time_start_periodic)
                        ->where('bill_details.time_end', '=', $time_end_periodic)
                        ->get();
                    $maintenances_7 = Maintenance::Where('maintenances.time_start', '<', $time_start_periodic)
                        ->where('pitch_id', $pitch_id)
                        ->where('maintenances.time_end', '>', $time_start_periodic)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '<', $time_end_periodic)
                        ->where('maintenances.time_end', '>', $time_end_periodic)
                        ->where('pitch_id', $pitch_id)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '>', $time_start_periodic)
                        ->where('maintenances.time_end', '<', $time_end_periodic)
                        ->where('pitch_id', $pitch_id)
                        ->where('maintenances.del_flag', 1)
                        ->get();
                    $bill_11 =  BillDetail::select('bill_details.*', 'pitches.pitch_name')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitches.category_id', 2)
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '=', $time_start_periodic)
                        ->where('bill_details.time_end', '=', $time_end_periodic)
                        ->get();
                } else {
                    $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '=', $time_start_periodic)
                        ->where('bill_details.time_end', '=', $time_end_periodic)
                        ->get();
                    $bill_11 = $bill;
                    $maintenances_11 = Maintenance::select('maintenances.*')
                        ->Where('maintenances.time_start', '<', $time_start_periodic)
                        ->where('maintenances.time_end', '>', $time_start_periodic)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '<', $time_end_periodic)
                        ->where('maintenances.time_end', '>', $time_end_periodic)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '>', $time_start_periodic)
                        ->where('maintenances.time_end', '<', $time_end_periodic)
                        ->where('maintenances.del_flag', 1)
                        ->get();
                }
                if (
                    (count($bill_7) == 0 &&
                        $time_end > $time_start &&
                        count($bill_11) == 0 &&
                        isset($maintenances_7) &&
                        count($maintenances_7) == 0) ||
                    (count($bill_7) == 0 &&
                        $time_end > $time_start &&
                        count($bill_11) == 0 &&
                        isset($maintenances_11) &&
                        count($maintenances_11) == 0)
                ) {
                    array_push($date_arr, $date);
                } else {
                    return Redirect::route('order-admin')->with('error', $check->pitch_name . ', khung giờ ' . $time_start . ' - ' . $time_end . ' ngày ' . $date . ' bạn chọn đã có người đặt!');
                }
            }
            // vãng lai
            if ($check_account == 1) {
                $bill_periodic = new BillPeriodic();
                $bill_periodic->deposit = 1000000;
                $bill_periodic->status = 1;
                $bill_periodic->payment_type = $payment_type;
                $bill_periodic->save();

                foreach ($date_arr as $key) {
                    $bill = new Bill();
                    $bill->customer_name = $customer_name;
                    $bill->customer_phone = $customer_phone;
                    $bill->bill_periodic_id = $bill_periodic->id;
                    $bill->deposit = 0;
                    $bill->total_price = $total_price;
                    $bill->point = 0;
                    $bill->status = 1;
                    $bill->save();

                    $billDetail = new BillDetail();
                    $billDetail->bill_id = $bill->id;
                    $billDetail->pitch_id = $pitch_id;
                    $billDetail->time_start = Carbon::parse($time_start . $key);
                    $billDetail->time_end = Carbon::parse($time_end . $key);
                    $billDetail->pitch_price = $pitch_price;
                    $billDetail->price_change = $price_change;
                    $billDetail->save();
                }
                return Redirect::route('order-admin-confirm', [$customer_phone]);
            } else {
                // có tài khoản
                $bill_periodic = new BillPeriodic();
                $bill_periodic->user_id = $customer_id;
                $bill_periodic->deposit = 1000000;
                $bill_periodic->status = 1;
                $bill_periodic->payment_type = $payment_type;
                $bill_periodic->save();

                foreach ($date_arr as $key) {
                    $bill = new Bill();
                    $bill->user_id = $customer_id;
                    $bill->bill_periodic_id = $bill_periodic->id;
                    $bill->deposit = 0;
                    $bill->total_price = $total_price;
                    $bill->point = 0;
                    $bill->status = 1;
                    $bill->save();

                    $billDetail = new BillDetail();
                    $billDetail->bill_id = $bill->id;
                    $billDetail->pitch_id = $pitch_id;
                    $billDetail->time_start = Carbon::parse($time_start . $key);
                    $billDetail->time_end = Carbon::parse($time_end . $key);
                    $billDetail->pitch_price = $pitch_price;
                    $billDetail->price_change = $price_change;
                    $billDetail->save();
                }

                return redirect()->route('order-admin-confirm', [$customer_id]);
            }
        }
    }

    public function confirm(Request $request)
    {
        $customer_id = $request->id;

        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
        $bill = Bill::where('status', 1)->get();
        // }
        $customer = User::find($customer_id);

        $introduce = Introduce::where('del_flag', 1)->get();

        $bill_data = Bill::where('user_id', $customer_id)
            ->orderByRaw('id DESC')
            ->orWhere('customer_phone', $customer_id)
            ->orderByRaw('id DESC')
            ->first();

        $booking_date = $bill_data->created_at;
        $customer_name = $bill_data->customer_name;
        $customer_phone = $bill_data->customer_phone;
        $deposit = $bill_data->deposit;

        if ($bill_data->bill_periodic_id == '') {
            $total_price = $bill_data->total_price;

            $maxTimePayment = strtotime('+24 hours', strtotime($bill_data->created_at));
            $maxTimePayment = date('H:i:s | d-m-Y', $maxTimePayment);
            $bill_periodic = null;
            $count_date_periodic = '';
            $total_price_count = '';
        } else {
            $bill_periodic = BillPeriodic::where('id', $bill_data->bill_periodic_id)->first();
            $total_price = $bill_periodic->deposit;

            $billCount = Bill::where('bill_periodic_id', $bill_periodic->id)->get();
            $count_date_periodic = count($billCount);
            $total_price_count = $bill_data->total_price * $count_date_periodic;

            $maxTimePayment = strtotime('+24 hours', strtotime($bill_periodic->created_at));
            $maxTimePayment = date('H:i:s | d-m-Y', $maxTimePayment);
        }


        return view('admin_manager.order_admin.confirm', [
            'introduce' => $introduce,
            'booking_date' => $booking_date,
            'deposit' => $deposit,
            'total_price' => $total_price,
            'admin' => $admin,
            'bill' => $bill,
            'bill_data' => $bill_data,
            'bill_periodic' => $bill_periodic,
            'customer' => $customer,
            'customer_name' => $customer_name,
            'customer_phone' => $customer_phone,
            'maxTimePayment' => $maxTimePayment,
            'total_price_count' => $total_price_count,
            'count_date_periodic' => $count_date_periodic,
        ]);
    }
}
