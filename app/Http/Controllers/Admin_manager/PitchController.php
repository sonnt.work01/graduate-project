<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;

use App\Models\Area;
use App\Models\Bill;
use App\Models\Admin;
use App\Models\Pitch;
use App\Models\Status;
use App\Models\Category;
use App\Models\AdminArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class PitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $area_id = $request->get('area_id');

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        $search = $request->get('search');
        if ($search == '') {
            $search = $today;
        }
        $listBill = Bill::where('created_at', $search)->paginate();

        $del_flag = $request->def_flag;

        if ($del_flag == null) {
            $del_flag = 1;
        }
        
        if ($area_id == null) {
            $area = Area::first();
            $area_id = $area->id;
        }
        $listArea = Area::where('del_flag', 1)->get();
        // if ($admin->role == 1) {
        // $listPitch = Pitch::where('area_id', $area_id)->where('del_flag', $del_flag)->get();

        $listPitch = Pitch::select('pitches.*', 'categories.category_name','areas.area_name', 'categories.price', 'statuses.status_name', 'statuses.price_change', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->join('areas', 'areas.id', '=', 'pitches.area_id')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->where('pitches.del_flag', $del_flag)
            ->get();
        // } else {
        //     $listPitch = Pitch::where('area_id', $area_id)->where('del_flag', $del_flag)->get();
        // }
        // dd($listPitch);
        return view('admin_manager.pitch.index', [
            'listPitch' => $listPitch,
            'admin' => $admin,
            'area_id' => $area_id,
            'listArea' => $listArea,
            'bill' => $bill,
            'del_flag' => $del_flag,
            'listBill' => $listBill,
            'search' => $search,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        if ($admin->role == 1) {
            $listArea = Area::where('del_flag', 1)->get();
        } else {
            $listArea = AdminArea::where('admin_id', $id_admin)->where('del_flag', 1)->get();
        }
        // dd($listArea);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        $category = Category::all();
        $status = Status::all();
        return view('admin_manager.pitch.create', [
            'listArea' => $listArea,
            'admin' => $admin,
            'bill' => $bill,
            'category' => $category,
            'status' => $status,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pitch_name' => 'required',
            'area_id' => 'required|',
            'image' => 'required|mimes:jpeg,jpg,png',
            'category_id' => 'required|',
            'location' => 'required',
        ], [
            'pitch_name.required' => "Tên khu vực không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            'image.required' => "Ảnh khu vực không được để trống!",
            'image.mimes' => "File ảnh không hợp lệ!",
            'category_id.required' => "Loại sân không được để trống!",
            'location.required' => "Vị trí không được để trống!",
        ]);
        //image
        $image = $request->file('image');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $newImageName);
        //
        $pitchName = $request->get('pitch_name');
        $status = $request->get('status_id');
        $description = $request->get('description');

        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $areaId = $request->get('area_id');

        $area = AdminArea::where('admin_id', $id_admin)->where('area_id', $areaId)->where('del_flag', 1)->count();
        if ($area == 0 && $admin->role == 0) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
        }
        $category_id = $request->get('category_id');
        $location = $request->get('location');

        $count = Pitch::where('area_id', $areaId)->where('location', 1)->where('del_flag', 1)->where('category_id', $category_id)->count();
        $count_pitchName = Pitch::where('area_id', $areaId)->where('pitch_name', 'like', "%$pitchName%")->where('del_flag', 1)->count();
        if ($count_pitchName != 0) {
            return redirect()->back()->with('error', 'Tên sân đã tồn tại!');
        }
        if ($count == 4 && $category_id == 1 && $location == 1) {
            return redirect()->back()->with('error', 'Đã đủ số sân ghép cho phép!');
        }
        if ($category_id == 2 && $location != 1) {
            return redirect()->back()->with('error', 'Tạo sân 11 thì phải là sân ghép!');
        };
        $pitch = new Pitch();
        $pitch->pitch_name = $pitchName;

        $pitch->area_id = $areaId;
        $pitch->location = $location;
        $pitch->status_id = $status;
        $pitch->category_id = $category_id;
        $pitch->image_path = $newImageName;
        $pitch->description = $description;
        $pitch->del_flag = 1;
        $pitch->save();
        return redirect()->back()->with('success', 'Thêm sân thành công!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pitch  $pitch
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            // $pitch = Pitch::where('id', $id)->firstOrFail();
            $pitch = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'statuses.status_name', 'statuses.price_change', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->where('pitches.id', $id)->firstOrFail();
            // dd($pitch);
            $id_admin = $request->session()->get('id');
            $admin = Admin::find($id_admin);
            // if ($admin->role == 0) {
            //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
            // } else {
                $bill = Bill::where('status', 1)->get();
            // }
            // if ($admin->role == 1) {
                $listArea = Area::where('del_flag', 1)->get();
            // } else {
            //     $listArea = AdminArea::where('admin_id', $id_admin)->where('del_flag', 1)->get();
            // }
            $category = Category::all();
            $status = Status::all();

            return view('admin_manager.pitch.edit', [
                'pitch' => $pitch,
                'listArea' => $listArea,
                'admin' => $admin,
                'bill' => $bill,
                'category' => $category,
                'status' => $status,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'pitch_name' => 'required|',
            'area_id' => 'required|',
            'image' => 'mimes:jpeg,jpg,png',
            'category_id' => 'required|',
            'location' => 'required',
            'del_flag' => 'required|',
        ], [
            'pitch_name.required' => "Tên khu vực không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            'image.mimes' => "File ảnh không hợp lệ!",
            'category_id.required' => "Loại sân không được để trống!",
            'location.required' => "Vị trí không được để trống!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $pitch = Pitch::find($id);
        // image

        $image = $request->file('image');
        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_image;
        }
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        $area_id = $request->get('area_id');
        $category_id = $request->get('category_id');
        // dd($category_id);
        $status_id = $request->get('status_id');
        $pitchName = $request->get('pitch_name');
        $location = $request->get('location');
        $description = $request->get('description');

        // if ($admin->role == 1) {
            $area = Area::where('id', $area_id)
                ->where('del_flag', 1)
                ->count();
        // } else {
        //     $area = Admin::where('admin_id', $id_admin)
        //         ->where('area_id', $area_id)
        //         ->where('del_flag', 1)
        //         ->count();
        // }

        if ($area == 0) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
        }

        if ($location == 1 && $pitch->location == 0) {
            $count = Pitch::where('area_id', $area_id)->where('location', 1)->where('category_id', 1)->where('del_flag', 1)->count();
            if ($count == 4 && $category_id == 1) {
                return redirect()->back()->with('error', 'Đã đủ số sân ghép cho phép!');
            }
        }
        if ($category_id == 2 && $location != 1) {
            return redirect()->back()->with('error', 'Tạo sân 11 thì phải là sân ghép!');
        };
        $count_pitchName = Pitch::where('area_id', $area_id)->where('pitch_name', 'like', "%$pitchName%")->where('del_flag', 1)->where('id', '<>', $id)->count();

        if ($count_pitchName != 0) {
            return redirect()->back()->with('error', 'Tên sân đã tồn tại!');
        }
        // $check_delflag = Bill::where('pitch_id', $id)
        //     ->where('active', '1')
        //     ->orwhere('pitch_id', $id)
        //     ->where('active', '2')
        //     ->count();
        // $del_flag = $request->get('del_flag');
        // if ($check_delflag != 0 && $del_flag == 0) {
        //     return redirect()->back()->with('error', 'Không thể đổi trạng thái vì có hoá đơn chưa xử lý xong!');
        // }
        $pitch->pitch_name = $pitchName;
        $pitch->area_id = $area_id;
        $pitch->category_id = $category_id;
        $pitch->status_id = $status_id;
        $pitch->location = $location;
        $pitch->description = $description;
        $pitch->image_path = $image_name;
        $pitch->del_flag = $request->get('del_flag');
        $pitch->save();

        return Redirect::route('pitch.index')->with('success', 'Cập nhật sân thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pitch  $pitch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pitch $pitch)
    {
        //
    }

    public function statusIndex(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        $del_flag = $request->def_flag;
        if ($del_flag == null) {
            $del_flag = 1;
        }

        $bill = Bill::where('status', 1)->get();

        $status = Status::where('del_flag', $del_flag)->get();

        return view('admin_manager.pitch.status_index', [
            'status' => $status,
            'admin' => $admin,
            'del_flag' => $del_flag,
            'bill' => $bill,
        ]);
    }

    public function statusCreate(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        $bill = Bill::where('status', 1)->get();

        return view('admin_manager.pitch.status_create', [
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    public function statusStore(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        
        $bill = Bill::where('status', 1)->get();

        $validated = $request->validate([
            'status_name' => 'required|',
            'price_change' => 'required|numeric',
        ], [
            'status_name.required' => "Tên tình trạng không được để trống!",
            'price_change.required' => "Phần trăm giá thay đổi không được để trống!",
            'price_change.numeric' => "Phần trăm giá thay đổi phải là số!",
        ]);

        $status = new Status();
        $status->status_name = $request->status_name;
        $status->price_change = $request->price_change;
        $status->del_flag = 1;
        $status->save();

        return redirect()->back()->with('success', 'Thêm tình trạng thành công!');
    }

    public function statusEdit(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);

        $bill = Bill::where('status', 1)->get();

        $status = Status::where('id', $id)->first();

        return view('admin_manager.pitch.status_edit', [
            'status' => $status,
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    public function statusUpdate(Request $request, $id)
    {
        $validated = $request->validate([
            'status_name' => 'required|',
            'price_change' => 'required|numeric',
            'del_flag' => 'required|',
        ], [
            'status_name.required' => "Tên tình trạng không được để trống!",
            'price_change.required' => "Phần trăm giá thay đổi không được để trống!",
            'price_change.numeric' => "Phần trăm giá thay đổi phải là số!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $status = Status::find($id);

        $status->status_name = $request->status_name;
        $status->price_change = $request->price_change;
        $status->del_flag = $request->del_flag;
        $status->save();

        return redirect()->back()->with('success', 'Cập nhật tình trạng thành công!');
    }
}
