<?php

namespace App\Http\Controllers\Admin_manager;
use Exception;

use App\Models\Bill;
use App\Models\Post;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        $del_flag = $request->get('def_flag');

        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }

        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }

        $search = $request->get('search');
        $listPost = Post::select('posts.*', 'admins.name')
        ->join('admins', 'admins.id', '=', 'posts.admin_id')
        ->where('posts.del_flag', $del_flag)->get();

        return view('admin_manager.post.index', [
            'listPost' => $listPost,
            'search' => $search,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        $id_admin = $request->session()->get('id');
        $admin = Admin::find($id_admin);
        if ($admin->role == 0) {
            $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        } else {
            $bill = Bill::where('status', 1)->get();
        }
        return view('admin_manager.post.create', [
            'admin' => $admin,
            'today' => $today,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'image' => 'required|mimes:jpeg,jpg,png',
            'admin_id' => 'required',
            'content' => 'required',
            'date_submitted' => 'required',
        ], [
            'title.required' => "Tên bài viết không được để trống!",
            'title.max' => "Tên bài viết tối đa 255 ký tự!",
            'admin_id.required' => "Người đăng không được lớn hớn 255 ký tự!",
            'image.required' => "Bạn chưa chọn ảnh!",
            'image.mimes' => "Ảnh không hợp lệ!",
            'content.required' => "Nội dung không được để trống!",
            'date_submitted.required' => "Bạn chưa chọn ngày!",
        ]);
        //image
        // cách 1
        $image = $request->file('image');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $newImageName);
        //cách 2
        // if ($request->hasFile('image')) {
        //     $file = $request->file('image');
        //     $extension = $file->getClientOriginalExtension();
        //     $fileName = time() . '.' . $extension;
        //     $file->move(public_path('images'), $fileName);
        // }
        //
        $title = $request->get('title');
        $admin_id = $request->get('admin_id');
        $content = $request->get('content');
        $date_submitted = $request->get('date_submitted');

        $post = new Post();
        $post->title = $title;
        $post->admin_id = $admin_id;
        $post->image_path = $newImageName;
        $post->content = $content;
        $post->date_submitted = $date_submitted;
        $post->del_flag = 1;
        $post->save();
        return redirect()->back()->with('success', 'Thêm bài viết thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $post = Post::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = Admin::find($id_admin);
            if ($admin->role == 0) {
                $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
            } else {
                $bill = Bill::where('status', 1)->get();
            }
            // $post = postModel::find($id);
            return view('admin_manager.post.edit', [
                'post' => $post,
                'admin' => $admin,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            // 'image' => 'required|mimes:jpeg,jpg,png',
            'admin_id' => 'required',
            'content' => 'required',
            'date_submitted' => 'required',
            'del_flag' => 'required',
        ], [
            'title.required' => "Tên bài viết không được để trống!",
            'title.max' => "Tên bài viết tối đa 255 ký tự!",
            'admin_id.required' => "Người đăng không được lớn hớn 255 ký tự!",
            // 'image.required' => "Bạn chưa chọn ảnh!",
            // 'image.mimes' => "File ảnh không hợp lệ!",
            'content.required' => "Nội dung không được để trống!",
            'date_submitted.required' => "Bạn chưa chọn ngày!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $post = Post::find($id);
        $image = $request->file('image');

        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_image;
        }
        $post->title = $request->get('title');
        $post->admin_id = $request->get('admin_id');
        $post->date_submitted = $request->get('date_submitted');
        $post->content = $request->get('content');
        $post->del_flag = $request->get('del_flag');
        $post->image_path = $image_name;
        $post->save();
        return Redirect::route('post.index')->with('success', 'Cập nhật bài viết thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
