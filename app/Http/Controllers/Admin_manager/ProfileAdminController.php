<?php

namespace App\Http\Controllers\Admin_manager;
use App\Models\Area;

use App\Models\Bill;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ProfileAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listArea = Area::all();
        $id_admin = $request->session()->get('id');
        $profile_admin = Admin::find($id_admin);
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = Bill::where('admin_id', '=', $admin->id)->where('status', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        return view('admin_manager.profile_admin.index', [
            'profile_admin' => $profile_admin,
            'listArea' => $listArea,
            'bill' => $bill,
            'admin' => $admin,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $listArea = Area::all();
        $id_admin = $request->session()->get('id');
        if ($id != $id_admin) {
            return redirect()->back()->with('error', 'Lỗi!');
        }
        $admin = Admin::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        // } else {
            $bill = Bill::where('status', 1)->get();
        // }
        return view('admin_manager.profile_admin.edit', [
            'listArea' => $listArea,
            'bill' => $bill,
            'admin' => $admin,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $profile_admin = Admin::find($id_admin);
        // dd($phone);
        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'phone' => 'required|digits:10|regex:/(0)[0-9]{9}/',
            'date_birth' => 'required',
            'gender' => 'required',
            'address' => 'required',
        ], [
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'phone.required' => "Số điện thoại không được để trống!",
            'phone.digits' => "Số điện thoại chỉ có 10 chữ số!",
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'gender.required' => "Giới tính không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
        ]);

        $profile_admin->name = $request->name;
        $profile_admin->date_birth = $request->date_birth;
        $profile_admin->gender = $request->gender;
        $profile_admin->phone = $request->phone;
        $profile_admin->address = $request->address;
        $profile_admin->save();

        return response()->json([
            'hasError' => false,
            'message' => 'Cập nhật thông tin thành công!!!'
        ]);
        // return Redirect::route('profile-admin')->with('success', "Cập nhật thông tin thành công!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
