<?php

namespace App\Http\Controllers\Customer_manager;

use App\Models\Area;
use App\Models\Bill;
use App\Models\User;
use App\Models\Introduce;
use App\Models\BillDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\BillPeriodic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BillCustomerController extends Controller
{
    public function index(Request $request)
    {
        $bill_active = $request->bill_active;
        if ($bill_active == '') {
            $bill_active = 1;
        }
        $introduce = Introduce::where('del_flag', 1)->first();
        $listArea = Area::all();
        $id_customer = Auth::id();

        $search = $request->search;
        if ($search == '') {
            $listBill = Bill::where('user_id', $id_customer)->select('bills.*', 'users.name', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                ->join('users', 'users.id', '=', 'bills.user_id')
                ->where('bills.status', $bill_active)
                ->orderBy('bills.created_at', 'DESC')
                ->get();
        } else {
            $listBill = Bill::where('user_id', $id_customer)->select('bills.*', 'users.name', DB::raw("(bills.total_price - bills.deposit) as 'missing'"))
                ->join('users', 'users.id', '=', 'bills.user_id')
                ->where('bills.status', $bill_active)
                ->where('bills.created_at', 'like', "%$search%")
                ->orderBy('bills.created_at', 'DESC')
                ->get();
        }

        $customer = User::find($id_customer);


        return view('customer_manager.bill_customer.index', [
            'customer' => $customer,
            'listBill' => $listBill,
            'search' => $search,
            'introduce' => $introduce,
            'bill_active' => $bill_active,
            'listArea' => $listArea,
        ]);
    }

    public function detail(Request $request, $id)
    {
        $id_customer = Auth::id();
        $customer = User::find($id_customer);
        $introduce = Introduce::where('del_flag', 1)->first();
        $listArea = Area::all();

        $bill_active = $request->get('bill_active');
        if ($bill_active == '') {
            $bill_active = 1;
        }

        $listBill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->where('bill_details.bill_id', $id)
            ->get();

        return view('customer_manager.bill_customer.detail', [
            'listBill' => $listBill,
            'bill_active' => $bill_active,
            'introduce' => $introduce,
            'listArea' => $listArea,
            'customer' => $customer,
        ]);
    }

    public function billPeriodic(Request $request)
    {
        $search = $request->search;

        $bill_active = $request->bill_active;
        if ($bill_active == '') {
            $bill_active = 1;
        }
        $introduce = Introduce::where('del_flag', 1)->first();
        $listArea = Area::all();
        $id_customer = Auth::id();

        $customer = User::find($id_customer);

        if ($search == '') {
            $listBill = BillPeriodic::select('bill_periodics.*', 'users.name')->where('user_id', $id_customer)
                ->join('users', 'users.id', '=', 'bill_periodics.user_id')
                ->where('bill_periodics.status', $bill_active)
                ->orderBy('bill_periodics.created_at', 'DESC')
                ->get();
        } else {
            $listBill = BillPeriodic::select('bill_periodics.*', 'users.name')->where('user_id', $id_customer)
                ->join('users', 'users.id', '=', 'bill_periodics.user_id')
                ->where('bill_periodics.status', $bill_active)
                ->where('bill_periodics.created_at', 'like', "%$search%")
                ->orderBy('bill_periodics.created_at', 'DESC')
                ->get();
        }
        // dd($listBill);
        return view('customer_manager.bill_customer.bill_periodic', [
            'customer' => $customer,
            'listBill' => $listBill,
            'search' => $search,
            'introduce' => $introduce,
            'bill_active' => $bill_active,
            'listArea' => $listArea,
        ]);
    }

    public function billPeriodicDetail(Request $request, $id)
    {
        $id_customer = Auth::id();
        $customer = User::find($id_customer);
        $introduce = Introduce::where('del_flag', 1)->first();
        $listArea = Area::all();

        $bill_active = $request->bill_active;
        if ($bill_active == '') {
            $bill_active = 1;
        }

        $bill = Bill::select('bills.*')->where('bill_periodic_id', $id)
            ->join('bill_periodics', 'bill_periodics.id', '=', 'bills.bill_periodic_id')
            ->get();
        // dd($bill);

        $listBill_arr = [];
        foreach ($bill as $key) {
            $listBill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
                ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                ->where('bill_details.bill_id', $key->id)
                ->first();
            array_push($listBill_arr, $listBill);
        }

        return view('customer_manager.bill_customer.bill_periodic_detail', [
            'listBill' => $listBill_arr,
            'bill_active' => $bill_active,
            'introduce' => $introduce,
            'listArea' => $listArea,
            'customer' => $customer,
        ]);
    }

    public function destroy_process(Request $request, $bill_id)
    {
        $id_customer = Auth::id();
        $customer = User::find($id_customer);
        $bill = Bill::find($bill_id);
        if ($bill->status == 1 && $bill->user_id == $id_customer) {

            $customer = User::find($bill->user_id);
            $point = $customer->points;
            $customer->points = $point + $bill->point;
            $customer->save();

            $bill->status = 3;
            $bill->cancellation_reason = $request->cancellation_reason;
            $bill->save();
            return Redirect::route('bill-page')->with('success', 'Hóa đơn đã được hủy!');
        } else {
            return Redirect::route('bill-page')->with('error', 'Lỗi thao tác!!!');
        }
    }

    public function destroyPeriodic(Request $request, $id)
    {
        try {

            $id_customer = Auth::id();
            $customer = User::find($id_customer);
            $bill_periodic = BillPeriodic::find($id);

            if ($bill_periodic->status == 1 && $bill_periodic->user_id == $id_customer) {
                $bill_periodic->status = 3;
                $bill_periodic->cancellation_reason = $request->cancellation_reason;
                $bill_periodic->save();

                $bill_data = Bill::where('bill_periodic_id', $bill_periodic->id)->get();

                foreach ($bill_data as $key) {
                    $key->status = 3;
                    $key->cancellation_reason = $request->cancellation_reason;
                    $key->save();
                }

                return Redirect::route('bill-periodic-customer')->with('success', 'Hóa đơn đã được hủy!');
            } else {
                return Redirect::route('bill-periodic-customer')->with('error', 'Lỗi thao tác!!!');
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
