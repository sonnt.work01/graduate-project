<?php

namespace App\Http\Controllers\Customer_manager;

use App\Models\Area;
use App\Models\User;
use App\Models\AdminArea;
use App\Models\Introduce;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contact = AdminArea::select('admins.*', 'areas.area_name', 'areas.area_address')
            ->join('areas', 'areas.id', '=', 'admin_areas.area_id')
            ->join('admins', 'admins.id', '=', 'admin_areas.admin_id')
            ->where('admins.del_flag', 1)->get();

        $area = Area::where('del_flag', 1)->get();
        
        $introduce = Introduce::where('del_flag', 1)->first();
        $id_customer = Auth::id();

        if (isset($id_customer)) {
            $customer = User::find($id_customer);
        } else {
            $customer = '';
        }
        return view('customer_manager.contact_us.index',
            [
                'contact' => $contact,
                'introduce' => $introduce,
                'customer' => $customer,
                'area' => $area,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
