<?php

namespace App\Http\Controllers\Customer_manager;

use App\Models\Area;
use App\Models\User;
use App\Models\Introduce;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Redirect;

class CustomerAuthenticateController extends Controller
{
    /**
     * Register
     */
    public function indexRegister()
    {
        return view('customer_manager.auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $passHash = Hash::make($request->password);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'date_birth' => $request->date_birth,
            'gender' => $request->gender,
            'password' => $passHash,
            'phone' => $request->phone,
            'del_flag' => 1,
            'confirm_flag' => 0,
            'verification_code' => sha1(time()),
        ]);

        MailController::sendVerifyMail($user->name, $user->email, $user->verification_code);
        return Redirect::route('login-customer')->with('success', 'Yêu cầu xác thực đã được gửi đến Email của bạn!');
    }

    public function verifyRegister(Request $request)
    {
        $verification_code = $request->code;

        $user = User::where('verification_code', $verification_code)->firstOrFail();

        $user->confirm_flag = 1;
        $user->verification_code = null;
        $user->update();
        return Redirect::route('login-customer')->with('success', 'Tài khoản của bạn đã được xác thực, đăng nhập đi!');
    }

    /**
     * Login
     */

    public function indexLogin()
    {
        return view('customer_manager.auth.login');
    }

    public function login(LoginRequest $request)
    {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'confirm_flag' => 1])) {
                return Redirect::route('home');
            }
            return redirect()->back()->with('error', 'Email, Mật khẩu không đúng hoặc Email chưa được xác thực!');
            // if ($user = User::where('email', $request->email)->first()) {
            //     // if ($request->password != $user->password) {
            //     if (!Hash::check($request->password, $user->password)) {
            //         return redirect()->back()->with('error', 'Mật khẩu không đúng!');
            //     } else {
            //         if (User::where('email', $request->email)
            //             ->where('confirm_flag', 1)->first()
            //         ) {
            //             $request->session()->put('id_customer', $user->id);
            //             return Redirect::route('home');
            //         } else {
            //             return redirect()->back()->with('error', 'Email chưa được xác nhận!');
            //         }
            //     }
            // } else {
            //     return redirect()->back()->with('error', 'Email khong hop le!');
            // }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return Redirect::route('home');
    }

    public function forgetPassView()
    {
        return view('customer_manager.auth.forget_pass');
    }

    public function forgetPassCus(Request $request)
    {
        $emailCus = $request->email;
        // kiểm tra trên DB 
        $check_email = User::where('email', $emailCus)->count();
        if ($check_email != 0) {
            MailController::sendRandomPasswordCustomer($emailCus);
            // SendMailCustomer::dispatch($emailCus)->delay(now()->addSeconds(2));
            return Redirect::route('login-customer')->with('success', 'Mật khẩu mới đã được gửi đến Email của bạn!');
        } else {
            return redirect()->back()->with('error', 'Lỗi, Email của bạn chưa được đăng ký với chúng tôi!');
        }
    }

    public function changePassView(Request $request)
    {
        $listArea = Area::all();
        $id_customer = $request->session()->get('id_customer');
        $customer = User::find($id_customer);
        $introduce = Introduce::where('del_flag', 1)->first();

        return view('customer_manager.profile_customer.change_password', [
            'customer' => $customer,
            'introduce' => $introduce,
            'listArea' => $listArea,
        ]);
    }

    public function changePassCus(Request $request, $id)
    {
        //pass old
        $old_password = $request->old_password;
        //pass new
        $new_password = $request->new_password;
        $passNewHash = Hash::make($new_password);
        //pass repeat
        $repeat_new_password = $request->repeat_new_password;

        $id_customer = Auth::id();
        $customer = User::find($id_customer);

        if (!Hash::check($old_password, $customer->password)) {
            return response()->json([
                'hasError' => true,
                'message' => 'Mật khẩu hiện tại không đúng!!!',
            ]);
        }
        
        $validated = $request->validate([
            'old_password' => 'required|',
            'new_password' => 'required|min:8',
        ], [
            'old_password.required' => "Mật khẩu hiện tại không được để trống!",
            'new_password.required' => "Mật khẩu mới không được để trống!",
            'new_password.min' => "Mật khẩu mới tối thiểu 8 ký tự!",
        ]);

        if ($id != $id_customer) {
            return response()->json([
                'hasError' => true,
                'message' => 'Tài khoản không hợp lệ!!!',
            ]);
        }

        if ($new_password == $repeat_new_password) {
            $passCustomer = User::find($id);
            $passCustomer->password = $passNewHash;
            $passCustomer->save();
            return response()->json([
                'hasError' => false,
                'message' => 'Đổi mật khẩu thành công!!!',
            ]);
        } else {
            return response()->json([
                'hasError' => true,
                'message' => 'Mật khẩu xác nhận không đúng!!!',
            ]);
        }
    }
}
