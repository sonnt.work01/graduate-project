<?php

namespace App\Http\Controllers\Customer_manager;

use App\Models\Area;
use App\Models\Post;
use App\Models\User;
use App\Models\Pitch;
use App\Models\Introduce;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IntroduceCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listBlog = Post::all();
        $listArea = Area::all();
        $id_customer = Auth::id();
        if (isset($id_customer)) {
            $customer = User::find($id_customer);
        } else {
            $customer = '';
        }

        $search = $request->search;
        $listArea = Area::all();
        $listPitch = Pitch::all();

        $introduce = Introduce::where('del_flag', 1)->first();

        return view('customer_manager.about_us.introduce', [
            'listPitch' => $listPitch,
            'search' => $search,
            'listArea' => $listArea,
            'customer' => $customer,
            'listBlog' => $listBlog,
            'introduce' => $introduce,
            'listArea' => $listArea,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
