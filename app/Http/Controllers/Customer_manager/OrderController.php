<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Bill;
use App\Models\User;
use App\Models\Pitch;
use App\Models\Introduce;
use App\Models\BillDetail;
use App\Models\Maintenance;
use App\Models\BillPeriodic;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    public function index(Request $request, $area_id, $pitch_id)
    {
        try {
            $array_time = array();
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $today = date('Y-m-d');

            $introduce = Introduce::where('del_flag', 1)->first();
            $listArea = Area::where('del_flag', 1)->get();
            $id_customer = Auth::id();
            $customer = User::find($id_customer);

            $date = $request->date;
            $time_start = $request->time_start;
            $time_end = $request->time_end;

            $weekday = $request->weekday_periodic;
            //đặt thường
            $pitch = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'categories.deposit', 'statuses.status_name', 'statuses.price_change', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `total_price`"))
                ->join('categories', 'categories.id', '=', 'pitches.category_id')
                ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
                ->join('areas', 'areas.id', '=', 'pitches.area_id')
                ->where('pitches.del_flag', 1)
                ->where('pitches.area_id', $area_id)
                ->where('pitches.id', $pitch_id)->first();

            $area = Area::where('id', $area_id)->first();
            $error = true;
            if ($weekday == null) {
                //check pitch id
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitch_id', $pitch_id)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', Carbon::parse($time_start . $date))
                    ->where('bill_details.time_end', '=', Carbon::parse($time_end . $date))
                    ->get();

                $array_pitch = [];
                foreach ($bill as $data) {
                    array_push($array_pitch, $data->pitch_id);
                }
                if (in_array($pitch_id, $array_pitch)) {
                    return Redirect::route('home')->with('error', $pitch->pitch_name . ', khung giờ ' . $time_start . ' - ' . $time_end . ' bạn chọn đã có người đặt!');
                }

                $start = Carbon::parse($time_start . $date);
                $end = Carbon::parse($time_end . $date);
                $minute = $start->diffInMinutes($end);

                $show_final_price = $pitch->price + ($pitch->price * $pitch->price_change / 100);

                // $show_final_price = $check_total / 60 * $minute;

                return view('customer_manager.order.index', [
                    'customer' => $customer,
                    'pitch' => $pitch,
                    'date' => $date,
                    'weekday' => $weekday,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'area' => $area,
                    'today' => $today,
                    'introduce' => $introduce,
                    'listArea' => $listArea,
                    'minute' => $minute,
                    'show_final_price' => $show_final_price,
                ]);
            } else {
                //đặt định kỳ
                $first_date = $request->date_periodic_start;
                $second_date = $request->date_periodic_end;
                $weekday_periodic = $request->weekday_periodic;

                $arr_date_order = [];
                for ($i = $first_date; $i <= $second_date; $i = date('Y-m-d', strtotime('+1 day', strtotime($i)))) {
                    $dayOfTheWeek = Carbon::parse($i)->dayOfWeek;
                    if (Carbon::parse($weekday_periodic)->dayOfWeek == $dayOfTheWeek) {
                        array_push($arr_date_order, $i);
                    }
                }
                $date_arr = [];
                $array_pitch = [];
                foreach ($arr_date_order as $key) {
                    $date = date_format(date_create($key), "Y-m-d");
                    array_push($date_arr, $date);

                    $time_start_periodic = Carbon::parse($time_start . $date);
                    $time_end_periodic = Carbon::parse($time_end . $date);

                    $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitch_id', $pitch_id)
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '=', $time_start_periodic)
                        ->where('bill_details.time_end', '=', $time_end_periodic)
                        ->get();

                    if(count($bill) != 0){
                        foreach ($bill as $data) {
                            array_push($array_pitch, $data->pitch_id);
                            $timeError = $data->time_start . ' - ' . $data->time_end;
                        }
                    }

                }
                // dd($timeError);
                if (in_array($pitch_id, $array_pitch)) {
                    return Redirect::route('home')->with('error', $pitch->pitch_name . ', khung giờ ' . $timeError . ' bạn chọn đã có người đặt!');
                }

                $start = Carbon::parse($time_start . $date);
                $end = Carbon::parse($time_end . $date);
                $minute = $start->diffInMinutes($end);

                $show_final_price = $pitch->price + ($pitch->price * $pitch->price_change / 100);

                // $show_final_price = $check_total / 60 * $minute;
                $date_return = implode("; ", $date_arr);
                $count_date_periodic = count($date_arr);
                $total_price = $show_final_price * $count_date_periodic;
                return view('customer_manager.order.index', [
                    'customer' => $customer,
                    'pitch' => $pitch,
                    'date' => $date_return,
                    'weekday' => $weekday,
                    'first_date' => $first_date,
                    'second_date' => $second_date,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'area' => $area,
                    'today' => $today,
                    'introduce' => $introduce,
                    'listArea' => $listArea,
                    'minute' => $minute,
                    'show_final_price' => $show_final_price,
                    'total_price' => $total_price,
                    'count_date_periodic' => $count_date_periodic,
                ]);
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
            // dd($e->getMessage());
        }
    }

    public function store(Request $request)
    {
        // try {
        $weekday_periodic = $request->weekday_periodic;
        $payment_type = $request->payment_type;

        // if ($weekday_periodic == null) {
        // $validated = $request->validate([
        //     'customer_id' => 'required|',
        //     'pitch_id' => 'required|',
        //     'day' => 'required',
        //     'time_start' => 'required',
        //     'time_end' => 'required',
        //     'payment_type' => 'required',
        // ], [
        //     'customer_id.required' => "Tên khách hàng không được để trống!",
        //     'pitch_id.required' => "Sân không được để trống!",
        //     'day.required' => "Ngày không được để trống!",
        //     'time_start.required' => "Thời gian bắt đầu không được để trống!",
        //     'time_end.required' => "Thời gian kết thúc không được để trống!",
        //     'payment_type.required' => "Vui lòng chọn hình thức thanh toán!",
        // ]);
        // } else {
        //     $validated = $request->validate([
        //         'customer_id' => 'required|',
        //         'pitch_id' => 'required|',
        //         'time_start' => 'required',
        //         'time_end' => 'required',
        //         'payment_type' => 'required',
        //     ], [
        //         'customer_id.required' => "Tên khách hàng không được để trống!",
        //         'pitch_id.required' => "Sân không được để trống!",
        //         'time_start.required' => "Thời gian bắt đầu không được để trống!",
        //         'time_end.required' => "Thời gian kết thúc không được để trống!",
        //         'payment_type.required' => "Vui lòng chọn hình thức thanh toán!",
        //     ]);
        // }

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        //bill
        $pitch_id = $request->pitch_id;
        $customer_id = $request->customer_id;

        $day = $request->day;
        $time_start = Carbon::parse($request->time_start . $day);
        $time_end = Carbon::parse($request->time_end . $day);

        if ($time_start->diffInMinutes($time_end) < 60) {
            return Redirect::route('home')->with('error', 'Lỗi!!! Thời gian kết thúc phải lớn hơn thời gian bắt đầu ít nhất 1 tiếng!');
        }
        if ($time_start->hour > $time_end->hour) {
            return Redirect::route('home')->with('error', 'Lỗi!!! Thời gian kết thúc phải lớn hơn thời gian bắt đầu!');
        }

        if ($payment_type == null) {
            return Redirect::route('home')->with('error', 'Lỗi!!! Vui lòng chọn hình thức thanh toán!');
        }

        $maxDate3Day = strtotime('+3 day', strtotime($today));
        $maxDate3Day = date('Y-m-d', $maxDate3Day);

        if ($day > $maxDate3Day) {
            return Redirect::route('home')->with('error', 'Lỗi!!! Chỉ được đặt trước tối đa 3 ngày sau ngày hôm nay!');
        }
        //infor pitch
        $check = Pitch::select('pitches.*', 'statuses.price_change', 'categories.price', 'categories.deposit')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->where('pitches.id', $pitch_id)
            ->first();

        $total = $check->price + ($check->price * $check->price_change / 100);
        $point_input = $request->point_input;

        if ($point_input == null) {
            $total_price = $total;
            $point_input = 0;
        } else {
            $total_price = $request->final_price;
            $point_input = $request->point_input;
        }

        $deposit = $total_price * $check->deposit / 100;
        $pitch_price = $check->price;
        $price_change = $check->price_change;

        if ($weekday_periodic == '') {
            if ($day < $today) {
                return Redirect::route('home')->with('error', 'Lỗi!!! Chọn ngày phải lớn hơn hoặc bằng ngày hôm nay!');
            }
            if ($check->category_id == 1) {
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitch_id', $pitch_id)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();

                $maintenances_7 = Maintenance::Where('maintenances.time_start', '<', $time_start)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.time_end', '>', $time_start)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '<', $time_end)
                    ->where('maintenances.time_end', '>', $time_end)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '>', $time_start)
                    ->where('maintenances.time_end', '<', $time_end)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.del_flag', 1)
                    ->get();

                $bill_11 =  BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitches.category_id', 2)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();
            } else {
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();
                $bill_11 = $bill;
                $maintenances_11 = Maintenance::select('maintenances.*')
                    ->Where('maintenances.time_start', '<', $time_start)
                    ->where('maintenances.time_end', '>', $time_start)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '<', $time_end)
                    ->where('maintenances.time_end', '>', $time_end)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '>', $time_start)
                    ->where('maintenances.time_end', '<', $time_end)
                    ->where('maintenances.del_flag', 1)
                    ->get();
            }

            if (
                (count($bill) == 0 &&
                    $time_end > $time_start &&
                    count($bill_11) == 0 &&
                    isset($maintenances_7) &&
                    count($maintenances_7) == 0) ||
                (count($bill) == 0 &&
                    $time_end > $time_start &&
                    count($bill_11) == 0 &&
                    isset($maintenances_11) &&
                    count($maintenances_11) == 0)
            ) {
                $bill = new Bill();
                $bill->user_id = $customer_id;
                $bill->deposit = $deposit;
                $bill->total_price = $total_price;
                $bill->point = $point_input;
                $bill->status = 1;
                $bill->payment_type = $payment_type;
                $bill->save();

                $user_id = Auth::id();
                $user = User::find($user_id);

                $points = $user->points;

                $user->points = $points - $point_input;
                $user->save();

                $billDetail = new BillDetail();
                $billDetail->bill_id = $bill->id;
                $billDetail->pitch_id = $pitch_id;
                $billDetail->time_start = $time_start;
                $billDetail->time_end = $time_end;
                $billDetail->pitch_price = $pitch_price;
                $billDetail->price_change = $price_change;
                $billDetail->save();

                return Redirect::route('confirm');
            } else {
                return Redirect::route('home')->with('error', $check->pitch_name . ', khung giờ ' . $request->time_start . ' - ' . $request->time_end . ' bạn chọn đã có người đặt!');
            }
        } else {
            // đặt định kì
            $first_date = $request->first_date_periodic;
            $second_date = $request->second_date_periodic;
            $weekday_periodic = $request->weekday_periodic;

            $time_end = $request->time_end;
            $time_start = $request->time_start;

            $arr_date_order = [];
            for ($i = $first_date; $i <= $second_date; $i = date('Y-m-d', strtotime('+1 day', strtotime($i)))) {
                $dayOfTheWeek = Carbon::parse($i)->dayOfWeek;
                if (Carbon::parse($weekday_periodic)->dayOfWeek == $dayOfTheWeek) {
                    array_push($arr_date_order, $i);
                }
            }

            $date_arr = [];
            foreach ($arr_date_order as $key) {
                $date = date_format(date_create($key), "Y-m-d");

                $time_start_periodic = Carbon::parse($time_start . $date);
                $time_end_periodic = Carbon::parse($time_end . $date);

                if ($check->category_id == 1) {
                    $bill_7 = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitch_id', $pitch_id)
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '=', $time_start_periodic)
                        ->where('bill_details.time_end', '=', $time_end_periodic)
                        ->get();

                    $maintenances_7 = Maintenance::Where('maintenances.time_start', '<', $time_start_periodic)
                        ->where('pitch_id', $pitch_id)
                        ->where('maintenances.time_end', '>', $time_start_periodic)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '<', $time_end_periodic)
                        ->where('maintenances.time_end', '>', $time_end_periodic)
                        ->where('pitch_id', $pitch_id)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '>', $time_start_periodic)
                        ->where('maintenances.time_end', '<', $time_end_periodic)
                        ->where('pitch_id', $pitch_id)
                        ->where('maintenances.del_flag', 1)
                        ->get();

                    $bill_11 =  BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitches.category_id', 2)
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '=', $time_start_periodic)
                        ->where('bill_details.time_end', '=', $time_end_periodic)
                        ->get();
                } else {
                    $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                        ->where('pitches.del_flag', 1)
                        ->where('bills.status', '<>', 3)
                        ->where('bill_details.time_start', '<=', $time_start_periodic)
                        ->where('bill_details.time_end', '>=', $time_end_periodic)
                        ->get();
                    $bill_11 = $bill;
                    $maintenances_11 = Maintenance::select('maintenances.*')
                        ->Where('maintenances.time_start', '<', $time_start_periodic)
                        ->where('maintenances.time_end', '>', $time_start_periodic)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '<', $time_end_periodic)
                        ->where('maintenances.time_end', '>', $time_end_periodic)
                        ->where('maintenances.del_flag', 1)

                        ->orWhere('maintenances.time_start', '>', $time_start_periodic)
                        ->where('maintenances.time_end', '<', $time_end_periodic)
                        ->where('maintenances.del_flag', 1)
                        ->get();
                }
                // var_dump(count($bill_7));
                // foreach ($bill_7 as $key) {
                //     var_dump($key->time_start);
                // }
                // var_dump($time_end > $time_start);
                // var_dump(count($bill_11) == 0);
                // var_dump(isset($maintenances_7));
                // var_dump(count($maintenances_7) == 0);
                // die();
                if (
                    (count($bill_7) == 0 &&
                        $time_end > $time_start &&
                        count($bill_11) == 0 &&
                        isset($maintenances_7) &&
                        count($maintenances_7) == 0) ||
                    (count($bill_7) == 0 &&
                        $time_end > $time_start &&
                        count($bill_11) == 0 &&
                        isset($maintenances_11) &&
                        count($maintenances_11) == 0)
                ) {
                    array_push($date_arr, $date);
                } else {
                    return Redirect::route('home')->with('error', $check->pitch_name . ', khung giờ ' . $time_start . ' - ' . $time_end . ' ngày ' . $date . ' bạn chọn đã có người đặt!');
                }
            }

            $bill_periodic = new BillPeriodic();
            $bill_periodic->user_id = $customer_id;
            $bill_periodic->deposit = 1000000;
            $bill_periodic->status = 1;
            $bill_periodic->payment_type = $payment_type;
            $bill_periodic->save();

            foreach ($date_arr as $key) {
                $bill = new Bill();
                $bill->user_id = $customer_id;
                $bill->bill_periodic_id = $bill_periodic->id;
                $bill->deposit = 0;
                $bill->total_price = $total_price;
                $bill->point = 0;
                $bill->status = 1;
                $bill->save();

                $billDetail = new BillDetail();
                $billDetail->bill_id = $bill->id;
                $billDetail->pitch_id = $pitch_id;
                $billDetail->time_start = Carbon::parse($time_start . $key);
                $billDetail->time_end = Carbon::parse($time_end . $key);
                $billDetail->pitch_price = $pitch_price;
                $billDetail->price_change = $price_change;
                $billDetail->save();
            }
            return Redirect::route('confirm-periodic');
        }
        // } catch (\Exception $e) {
        //     dd($e->getMessage());
        // }
    }

    public function storeCart(Request $request)
    {
        $id_session_customer = Auth::id();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d H-m-s');

        $point_form = $request->point_form;
        if ($point_form == null) {
            $point_form = 0;
        } else {
            $point_form = $request->point_form;
        }
        $payment_type = $request->payment_type;

        $carts = session()->get('cart');
        foreach ($carts as $data) {
            $pitch_id = $data['id'];
            $time_end = $data['time_end'];
            $time_start = $data['time_start'];
            $check = Pitch::where('id', $data['id'])->first();
            if ($check->category_id == 1) {
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitch_id', $pitch_id)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();

                $maintenances_7 = Maintenance::Where('maintenances.time_start', '<', $time_start)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.time_end', '>', $time_start)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '<', $time_end)
                    ->where('maintenances.time_end', '>', $time_end)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '>', $time_start)
                    ->where('maintenances.time_end', '<', $time_end)
                    ->where('pitch_id', $pitch_id)
                    ->where('maintenances.del_flag', 1)
                    ->get();

                $bill_11 =  BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitches.category_id', 2)
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();
            } else {
                $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'bills.status')
                    ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
                    ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
                    ->where('pitches.del_flag', 1)
                    ->where('bills.status', '<>', 3)
                    ->where('bill_details.time_start', '=', $time_start)
                    ->where('bill_details.time_end', '=', $time_end)
                    ->get();
                $bill_11 = $bill;
                $maintenances_11 = Maintenance::select('maintenances.*')
                    ->Where('maintenances.time_start', '<', $time_start)
                    ->where('maintenances.time_end', '>', $time_start)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '<', $time_end)
                    ->where('maintenances.time_end', '>', $time_end)
                    ->where('maintenances.del_flag', 1)

                    ->orWhere('maintenances.time_start', '>', $time_start)
                    ->where('maintenances.time_end', '<', $time_end)
                    ->where('maintenances.del_flag', 1)
                    ->get();
            }
            if (
                !((count($bill) == 0 &&
                    $time_end > $time_start &&
                    count($bill_11) == 0 &&
                    isset($maintenances_7) &&
                    count($maintenances_7) == 0) ||
                    (count($bill) == 0 &&
                        $time_end > $time_start &&
                        count($bill_11) == 0 &&
                        isset($maintenances_11) &&
                        count($maintenances_11) == 0))
            ) {
                return Redirect::route('home')->with('error', 'Lỗi!!! ' . $data['pitch_name'] . ' khung giờ ' . $data['time_start'] . ' - ' . $data['time_end'] . ' đã có người đặt trước.');
            }
        }
        $bill = new Bill();
        $bill->user_id = $id_session_customer;
        $bill->deposit = $request->deposit_cart_input;
        $bill->total_price = $request->final_cart_input;
        $bill->point = $point_form;
        $bill->status = 1;
        $bill->payment_type = $payment_type;
        $bill->save();

        $user_id = Auth::id();
        $user = User::find($user_id);

        $points = $user->points;

        $user->points = $points - $point_form;
        $user->save();

        foreach ($carts as $data) {
            $pitch = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'statuses.status_name', 'statuses.price_change', 'areas.area_name', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
                ->join('categories', 'categories.id', '=', 'pitches.category_id')
                ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
                ->join('areas', 'areas.id', '=', 'pitches.area_id')
                ->where('pitches.del_flag', 1)
                ->where('pitches.id', $data['id'])->first();

            $billDetail = new BillDetail();
            $billDetail->bill_id = $bill->id;
            $billDetail->pitch_id = $data['id'];
            $billDetail->time_start = $data['time_start'];
            $billDetail->time_end = $data['time_end'];
            $billDetail->pitch_price = $pitch->price;
            $billDetail->price_change = $pitch->price_change;
            $billDetail->save();
        }
        session()->forget('cart');

        return Redirect::route('confirm');
    }

    public function confirm()
    {
        $id_customer = Auth::id();
        if (isset($id_customer)) {
            $customer = User::find($id_customer);
        } else {
            $customer = '';
        }
        $listArea = Area::where('del_flag', 1)->get();
        $introduce = Introduce::where('del_flag', 1)->first();
        $bill = Bill::where('user_id', $id_customer)->orderByRaw('id DESC')->first();

        $maxTimePayment = strtotime('+24 hours', strtotime($bill->created_at));
        $maxTimePayment = date('H:i:s | d-m-Y', $maxTimePayment);

        return view('customer_manager.confirm.index', [
            'customer' => $customer,
            'listArea' => $listArea,
            'introduce' => $introduce,
            'bill' => $bill,
            'maxTimePayment' => $maxTimePayment,
        ]);
    }

    public function confirmPeriodic()
    {
        $id_customer = Auth::id();
        if (isset($id_customer)) {
            $customer = User::find($id_customer);
        } else {
            $customer = '';
        }
        $listArea = Area::where('del_flag', 1)->get();
        $introduce = Introduce::where('del_flag', 1)->first();

        $bill_periodic = BillPeriodic::where('user_id', $id_customer)->orderByRaw('id DESC')->first();

        $bill = Bill::where('bill_periodic_id', $bill_periodic->id)->first();

        $billCount = Bill::where('bill_periodic_id', $bill_periodic->id)->get();
        $count_date_periodic = count($billCount);
        $total_price = $bill->total_price * $count_date_periodic;

        $maxTimePayment = strtotime('+24 hours', strtotime($bill_periodic->created_at));
        $maxTimePayment = date('H:i:s | d-m-Y', $maxTimePayment);

        return view('customer_manager.confirm.confirm_periodic', [
            'customer' => $customer,
            'listArea' => $listArea,
            'introduce' => $introduce,
            'bill_periodic' => $bill_periodic,
            'bill' => $bill,
            'maxTimePayment' => $maxTimePayment,
            'count_date_periodic' => $count_date_periodic,
            'total_price' => $total_price,
        ]);
    }

    public function billPdf()
    {
        $data["email"] = "sonnt.work01@gmail.com";
        $data["title"] = "From ItSolutionStuff.com";
        $data["body"] = "This is Demo";

        $pdf = PDF::loadView('customer_manager.mail.mail_order', $data);

        Mail::send('customer_manager.mail.mail_order', $data, function ($message) use ($data, $pdf) {
            $message->to($data["email"], $data["email"])
                ->subject($data["title"])
                ->attachData($pdf->output(), "bill.pdf");
        });

        dd('Mail sent successfully');
    }

    public function point(Request $request)
    {
        $point = $request->point;
        $user = Auth::user();

        if ($user->points == 0) {
            return response()->json([
                'hasError' => true,
                'message' => 'Bạn không có điểm tích luỹ nào. Hãy đặt sân để tích luỹ thêm điểm!!!',
            ]);
        }

        if ($point > $user->points) {
            return response()->json([
                'hasError' => true,
                'message' => 'Bạn đang nhập vượt quá số điểm bạn có!!!',
            ]);
        }

        $pitch_id = $request->pitch_id;

        $day = $request->day;
        $time_start = Carbon::parse($request->time_start . $day);
        $time_end = Carbon::parse($request->time_end . $day);


        $pitch = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'categories.deposit', 'statuses.status_name', 'statuses.price_change', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->join('areas', 'areas.id', '=', 'pitches.area_id')
            ->where('pitches.del_flag', 1)
            ->where('pitches.id', $pitch_id)->firstOrFail();

        $total_price = $pitch->price + ($pitch->price * $pitch->price_change / 100);

        // $minute = $time_start->diffInMinutes($time_end);

        // $total_price = $pitch->final_price / 60 * $minute;

        $final = $total_price - $point;

        $max = $total_price;

        $min = 1000;

        if ($point < $min) {
            return response()->json([
                'hasError' => true,
                'message' => 'Nhập điểm tối thiểu là 1000 !!!',
            ]);
        }

        if ($point > $max) {
            return response()->json([
                'hasError' => true,
                'message' => 'Điểm quá giới hạn!!!',
            ]);
        }

        return response()->json([
            'hasError' => false,
            'point' => $point,
            'final' => $final,
            'message' => 'success',
        ]);
    }

    public function point_cart(Request $request)
    {
        $point_cart = $request->point_cart;
        $total_cart = $request->total_cart;


        $user = Auth::user();

        if ($user->points == 0) {
            return response()->json([
                'hasError' => true,
                'message' => 'Bạn không có điểm tích luỹ nào. Hãy đặt sân để tích luỹ thêm điểm!!!',
            ]);
        }

        if ($point_cart > $user->points) {
            return response()->json([
                'hasError' => true,
                'message' => 'Bạn đang nhập vượt quá số điểm bạn có!!!',
            ]);
        }

        $final_cart = $total_cart - $point_cart;

        $deposit_cart = $final_cart - ($final_cart * 0.5);

        $max = $total_cart;

        $min = 1000;

        if ($point_cart < $min) {
            return response()->json([
                'hasError' => true,
                'message' => 'Nhập điểm tối thiểu là 1000 !!!',
            ]);
        }

        if ($point_cart > $max) {
            return response()->json([
                'hasError' => true,
                'message' => 'Điểm quá giới hạn!!!',
            ]);
        }

        return response()->json([
            'hasError' => false,
            'point' => $point_cart,
            'deposit_cart' => $deposit_cart,
            'final' => $final_cart,
            'message' => 'success',
        ]);
    }
}
