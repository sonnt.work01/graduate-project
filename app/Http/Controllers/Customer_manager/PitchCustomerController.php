<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\User;
use App\Models\Pitch;
use App\Models\Introduce;
use App\Models\BillDetail;
use App\Models\Maintenance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Support\Facades\Auth;

class PitchCustomerController extends Controller
{
    public function pitchDetail(Request $request, $area_id, $pitch_id)
    {
        $introduce = Introduce::where('del_flag', 1)->first();
        $listArea = Area::where('del_flag', 1)->get();
        $id_customer = Auth::id();
        $customer = User::find($id_customer);
        try {
            $area = Area::where('id', $area_id)->firstOrFail();
            // $pitch = Pitch::where('id', "$pitch_id")->where('area_id', $area->id)->firstOrFail();

            $pitch = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'categories.deposit', 'statuses.status_name', 'statuses.price_change', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
                ->join('categories', 'categories.id', '=', 'pitches.category_id')
                ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
                ->join('areas', 'areas.id', '=', 'pitches.area_id')
                ->where('pitches.del_flag', 1)
                ->where('pitches.area_id', $area->id)
                ->where('pitches.id', $pitch_id)->firstOrFail();
           
            return view('customer_manager.pitch_customer.detail', [
                'customer' => $customer,
                'pitch' => $pitch,
                'area' => $area,
                'introduce' => $introduce,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
        }
    }

    public function addToCart(Request $request, $id)
    {
        // session()->flush('cart');
        $day = $request->day;
        $time_start = Carbon::parse($request->time_start . $day);
        $time_end = Carbon::parse($request->time_end . $day);

        $pitch = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'categories.deposit', 'statuses.status_name', 'statuses.price_change', 'areas.area_name', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->join('areas', 'areas.id', '=', 'pitches.area_id')
            ->where('pitches.del_flag', 1)
            ->where('pitches.id', $id)->first();
        $final_price = $pitch->final_price;
        $deposit = $final_price * $pitch->deposit / 100;
        if (session()->get('cart') == null) {
            $cart = array();
        } else {
            $cart = session()->get('cart');
        }

        $bill = BillDetail::select('bill_details.*', 'pitches.pitch_name')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->join('maintenances', 'maintenances.pitch_id', '=', 'bill_details.pitch_id')
            ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
            ->where('bill_details.pitch_id', $id)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)
            ->where('bill_details.time_start', '=', $time_start)
            ->where('bill_details.time_end', '=', $time_end)

            ->orWhere('maintenances.time_start', '<', $time_start)
            ->where('maintenances.time_end', '>', $time_start)
            ->where('maintenances.del_flag', 1)
            ->where('maintenances.pitch_id', $id)

            ->orWhere('maintenances.time_start', '<', $time_end)
            ->where('maintenances.time_end', '>', $time_end)
            ->where('maintenances.del_flag', 1)
            ->where('maintenances.pitch_id', $id)

            ->orWhere('maintenances.time_start', '>', $time_start)
            ->where('maintenances.time_end', '<', $time_end)
            ->where('maintenances.del_flag', 1)
            ->where('maintenances.pitch_id', $id)
            ->get();

        if (count($bill) == 0) {
            if (empty(session()->get('cart'))) {
                array_push($cart, [
                    'id' => $id,
                    'pitch_name' => $pitch->pitch_name,
                    'status_name' => $pitch->status_name,
                    'category_name' => $pitch->category_name,
                    'final_price' => $final_price,
                    'day' => $day,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'deposit' => $deposit,
                ]);
            } else {
                foreach (session()->get('cart') as $item) {
                    if ($item['id'] == $id) {
                        if (($time_start = $item['time_start'] || $time_end = $item['time_end'])
                        ) {
                            return response()->json([
                                'hasError' => true,
                                'message' => 'Không thể thêm vì khung giờ đã trùng trong giỏ hàng!!!',
                            ]);
                        } else {
                            array_push($cart, [
                                'id' => $id,
                                'pitch_name' => $pitch->pitch_name,
                                'status_name' => $pitch->status_name,
                                'category_name' => $pitch->category_name,
                                'final_price' => $final_price,
                                'day' => $day,
                                'time_start' => $time_start,
                                'time_end' => $time_end,
                                'deposit' => $deposit,
                            ]);
                        }
                    } else {
                        array_push($cart, [
                            'id' => $id,
                            'pitch_name' => $pitch->pitch_name,
                            'status_name' => $pitch->status_name,
                            'category_name' => $pitch->category_name,
                            'final_price' => $final_price,
                            'day' => $day,
                            'time_start' => $time_start,
                            'time_end' => $time_end,
                            'deposit' => $deposit,
                        ]);
                    }
                }
            }
        } else {
            return response()->json([
                'hasError' => true,
                'message' => 'Không thể thêm vào hàng chờ vì khung giờ đã có người đặt hoặc sân đang được bảo trì!!!',
            ]);
        }
        session()->put('cart', $cart);
        return response()->json([
            'hasError' => false,
            'message' => 'Đã thêm sân vào hàng chờ!!!',
        ]);
    }

    public function showCart(Request $request)
    {
        $id_customer = Auth::id();
        if (isset($id_customer)) {
            $customer = User::find($id_customer);
        } else {
            $customer = '';
        }
        $listArea = Area::where('del_flag', 1)->get();
        $introduce = Introduce::where('del_flag', 1)->first();

        $carts = session()->get('cart');
        if (empty($carts)) {
            $carts = [];
        } else {
            $carts = session()->get('cart');
        }
        // dd($carts);

        return view('customer_manager.cart.index', [
            'listArea' => $listArea,
            'customer' => $customer,
            'introduce' => $introduce,
            'carts' => $carts,
        ]);
    }

    public function deleteCartItem($id)
    {
        // dd($id);
        $carts = session()->get('cart');
        // dd($carts);

        unset($carts[$id]);

        if (count($carts) > 0) {
            session()->put('cart', $carts);
        } else {
            session()->forget('cart');
        }
        
        return redirect()->back()->with('success', 'Đã xoá 1 đơn đặt sân!');
    }

    public function pitchFilter(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        $date = $request->date;
        $time_order = $request->time_order_normal;
        $time = explode("-", $time_order);
        $start_in_arr = $time[0];
        $end_in_arr = $time[1];
        //time validate
        
        $pitchModel = new Pitch();
        $checkTime = $pitchModel->checkTime($start_in_arr, $end_in_arr);

        if(!$checkTime) {
            return response()->json([
                'hasError' => true,
                'message' => 'Lỗi khung thời gian!!!',
            ]);
        }

        $time_start = Carbon::parse($start_in_arr . $date);
        $time_end = Carbon::parse($end_in_arr . $date);

        $time_now = Carbon::now();
        //giờ
        $start = $start_in_arr;
        $end = $end_in_arr;

        if ($time_start < $time_now) {
            return response()->json([
                'hasError' => true,
                'message' => 'Khung giờ đã quá muộn so với thời gian hiện tại!!!',
            ]);
        }

        if ($date < $today) {
            return response()->json([
                'hasError' => true,
                'message' => 'Chọn ngày phải lớn hơn hoặc bằng ngày hôm nay!!!',
            ]);
        }

        $maxDate3Day = strtotime('+3 day', strtotime($today));
        $maxDate3Day = date('Y-m-d', $maxDate3Day);

        if ($date > $maxDate3Day) {
            return response()->json([
                'hasError' => true,
                'message' => 'Chỉ được đặt trước tối đa 3 ngày sau ngày hôm nay!!!',
            ]);
        }

        $listPitch = Pitch::where('del_flag', 1)->get();
        $pitchModel2 = new Pitch();
        list($bill_7, $maintenances_7, $bill_11, $maintenances_11) = $pitchModel2->fillPitch($time_start, $time_end);
            
        $array_pitch = [];
        if (count($bill_11) != 0 || count($maintenances_11) != 0) {
            foreach ($listPitch as $data) {
                array_push($array_pitch, $data->id);
            }
        } else {
            foreach ($bill_7 as $data) {
                array_push($array_pitch, $data->pitch_id);
            }
            foreach ($maintenances_7 as $data) {
                array_push($array_pitch, $data->pitch_id);
            }
        }

        return response()->json([
            'hasError' => false,
            'array_pitch' => $array_pitch,
            'date' => $date,
            'listPitch' => $listPitch,
            'message' => 'Success',
            'start' => $start,
            'end' => $end,
        ]);
    }

    public function filterOrderPeriodic(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        $first_date = $request->date_start_periodic;
        $date_end_periodic = $request->date_end_periodic;
        
        // đặt tối thiểu 1 tháng
        $minDate1Month = strtotime('+1 month', strtotime($first_date));
        $minDate1Month = date('Y-m-d', $minDate1Month);

        $time_order = $request->time_order_periodic;
        $time = explode("-", $time_order);
        $start_in_arr = $time[0];
        $end_in_arr = $time[1];
        //time validate
        
        $pitchModel = new Pitch();
        $checkTime = $pitchModel->checkTime($start_in_arr, $end_in_arr);

        $time_now = Carbon::now();

        if(!$checkTime) {
            return response()->json([
                'hasError' => true,
                'message' => 'Lỗi khung thời gian!!!',
            ]);
        }

        $start = $start_in_arr;
        $end = $end_in_arr;
        
        if ($first_date < $today) {
            return response()->json([
                'hasError' => true,
                'message' => 'Chọn ngày phải lớn hơn hoặc bằng ngày hôm nay!!!',
            ]);
        }
        
        if ($first_date > $date_end_periodic) {
            return response()->json([
                'hasError' => true,
                'message' => 'Ngày kết thúc phải lớn hơn ngày bắt đầu!!!',
            ]);
        }

        if ($date_end_periodic < $minDate1Month) {
            return response()->json([
                'hasError' => true,
                'message' => 'Đặt tối thiểu ít nhất 1 tháng từ ngày bắt đầu!!!',
            ]);
        }

        $arr_date_order = [];
        for ($i = $first_date; $i <= $date_end_periodic; $i = date('Y-m-d', strtotime('+1 day', strtotime($i)))) {
            $dayOfTheWeek = Carbon::parse($i)->dayOfWeek;
            if (Carbon::parse($request->weekday)->dayOfWeek == $dayOfTheWeek) {
                array_push($arr_date_order, $i);
            }
        }

        $date_arr = [];
        $array_pitch = [];
        foreach ($arr_date_order as $key) {
            $date = date_format(date_create($key), "Y-m-d");
            array_push($date_arr, $date);

            // var_dump($date);
            $time_start_periodic = Carbon::parse($start_in_arr . $date);
            $time_end_periodic = Carbon::parse($end_in_arr . $date);

            if ($time_start_periodic < $time_now) {
                return response()->json([
                    'hasError' => true,
                    'message' => 'Lỗi!!! Đã qua khung giờ bạn chọn trong ngày ' . $date .' !!!',
                ]);
            }

            $listPitch = Pitch::where('del_flag', 1)->get();
            // dd($time_start->toDateString());
            $pitchModel2 = new Pitch();
            list($bill_7, $maintenances_7, $bill_11, $maintenances_11) = $pitchModel2->fillPitch($time_start_periodic, $time_end_periodic);

            if (count($bill_11) != 0 || count($maintenances_11) != 0) {
                foreach ($listPitch as $data) {
                    array_push($array_pitch, $data->id);
                }
            } else {
                foreach ($bill_7 as $data) {
                    array_push($array_pitch, $data->pitch_id);
                }
                foreach ($maintenances_7 as $data) {
                    array_push($array_pitch, $data->pitch_id);
                }
            }
        }

        return response()->json([
            'hasError' => false,
            'weekday' => $request->weekday,
            'first_date' => $first_date,
            'array_pitch' => $array_pitch,
            'date' => $date_arr,
            'second_date' => $date_end_periodic,
            'listPitch' => $listPitch,
            'message' => 'Success',
            'start' => $start,
            'end' => $end,
        ]);
    }
}
