<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackGoogle(Request $request)
    {
        try {
            $user = Socialite::driver('google')->user();
            $findUser = User::where('google_id', $user->id)->first();

            if ($findUser) {
                Auth::login($findUser);
                return redirect()->route('home')->with('success', 'Đăng nhập thành công!');
            } else {
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'confirm_flag' => 1,
                    'password' => null,
                    'date_birth' => null,
                    'gender' => null,
                    'phone' => null,
                    'del_flag' => 1,
                ]);
                Auth::login($newUser);
                return redirect()->route('home')->with('success', 'Đăng nhập thành công!');
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
