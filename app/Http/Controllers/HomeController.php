<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Post;
use App\Models\User;
use App\Models\Pitch;
use App\Models\Introduce;
use App\Models\BillDetail;
use App\Models\Maintenance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        $search = $request->search;
        if ($search == '') {
            $search = $today;
        }
        // đặt thường tối đa 3 ngày
        $maxDate3Day = strtotime('+3 day', strtotime($today));
        $maxDate3Day = date('Y-m-d', $maxDate3Day);
        // dd($maxDate3Day);

        // đặt định kì tối thiểu 1 tháng
        $minDate1Month = strtotime('+1 month', strtotime($today));
        $minDate1Month = date('Y-m-d', $minDate1Month);

        $start = null;
        $end = null;

        $listPost = Post::where('del_flag', 1)->orderBy('id', 'desc')->paginate(3);

        $id_customer = Auth::id();
        if (isset($id_customer)) {
            // dd($id_customer);
            $customer = User::find($id_customer);
        } else {
            // dd('null');
            $customer = '';
        }
        $introduce = Introduce::where('del_flag', 1)->first();

        // $search = $request->get('search');
        $listArea = Area::where('del_flag', 1)->get();
        $listPitch = Pitch::where('del_flag', 1)->get();
        $array_pitch = [];

        $listPitchTime = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'statuses.status_name', 'statuses.price_change', 'areas.area_name', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->join('areas', 'areas.id', '=', 'pitches.area_id')
            ->where('pitches.del_flag', 1)
            ->where('pitches.area_id', 1)->get();

        $listTime = Pitch::select('pitches.*', 'bill_details.time_start', 'bill_details.time_end')
            ->join('bill_details', 'bill_details.pitch_id', '=', 'pitches.id')
            ->where('pitches.del_flag', 1)
            ->where('bill_details.time_start', 'like', "%$search%")
            ->where('pitches.area_id', 1)
            ->orderBy('bill_details.time_start', 'ASC')
            ->get();
        $pitch = new Pitch();
        $arr_time = $pitch->arrTime();

        return view('home', [
            'listArea' => $listArea,
            'customer' => $customer,
            'listPost' => $listPost,
            'introduce' => $introduce,
            'listPitchTime' => $listPitchTime,
            'listTime' => $listTime,
            'listPitch' => $listPitch,
            'search' => $search,
            'today' => $today,
            'array_pitch' => $array_pitch,
            'start' => $start,
            'end' => $end,
            'maxDate3Day' => $maxDate3Day,
            'minDate1Month' => $minDate1Month,
            'arr_time' => $arr_time
        ]);
    }

    public function postDetail(Request $request, $id)
    {
        try {
            $listArea = Area::all();
            $post = Post::find($id);
            $listPost = Post::inRandomOrder()->limit(3)->where('del_flag', 1)->where('id', '<>', $id)->get();
            $id_customer = Auth::id();
            if (isset($id_customer)) {
                $customer = User::find($id_customer);
            } else {
                $customer = '';
            }
            $introduce = Introduce::where('del_flag', 1)->first();

            return view('customer_manager.post_detail.index', [
                'customer' => $customer,
                'post' => $post,
                'introduce' => $introduce,
                'listPost' => $listPost,
                'listArea' => $listArea,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Bài viết không hợp lệ!');
        }
    }
}
