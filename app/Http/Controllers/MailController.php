<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Admin;
use App\Mail\RegisterVerify;
use App\Mail\ChangePasswordAdmin;
use App\Http\Controllers\Controller;
use App\Mail\ChangePasswordCustomer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public static function sendVerifyMail($name, $email, $verification_code)
    {
        $data = [
            'name' => $name,
            'verification_code' => $verification_code,
        ];
        Mail::to($email)->send(new RegisterVerify($data));
    }

    public static function sendRandomPasswordCustomer($email)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $pass = implode($pass);

        $passHash = Hash::make($pass);
        // update trong bảng khách hàng 
        $customer = User::where('email', $email)->first();
            
        $customer->password = $passHash;
        $customer->update();

        $data = [
            'name' => $customer->name,
            'pass' => $pass,
        ];
        Mail::to($email)->send(new ChangePasswordCustomer($data));
    }

    public static function sendRandomPasswordAdmin($email)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $pass = implode($pass);

        $passHash = Hash::make($pass);
        // update trong bảng khách hàng 
        $customer = Admin::where('email', $email)->first();
            
        $customer->password = $passHash;
        $customer->update();

        $data = [
            'name' => $customer->name,
            'pass' => $pass,
        ];
        Mail::to($email)->send(new ChangePasswordAdmin($data));
    }
}
