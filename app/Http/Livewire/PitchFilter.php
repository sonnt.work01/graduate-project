<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use App\Models\Area;
use App\Models\Post;
use App\Models\Pitch;
use Livewire\Component;
use App\Models\Customer;
use App\Models\Introduce;
use App\Models\BillDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PitchFilter extends Component
{
    public $search = null;
    public $time_start = null;
    public $time_end = null;

    public function render(Request $request)
    {
        $today = date('Y-m-d');
        $search = $request->search;
        if ($search == '') {
            $search = $today;
        }
        $maxDate3Day = strtotime('+3 day', strtotime($today));
        $maxDate3Day = date('Y-m-d', $maxDate3Day);

        $listBlog = Post::where('del_flag', 1)->orderBy('id', 'desc')->paginate(3);

        $id_customer = $request->session()->get('id_customer');
        if (isset($id_customer)) {
            // dd($id_customer);
            $customer = Customer::find($id_customer);
        } else {
            // dd('null');
            $customer = '';
        }
        $introduce = Introduce::all();

        // $search = $request->get('search');
        $listArea = Area::where('del_flag', 1)->get();

        $listPitch = Pitch::where('del_flag', 1)->get();
        $date = $request->search;
        $time_start = Carbon::parse($request->time_start . $date);
        $time_end = Carbon::parse($request->time_end . $date);

        $start = $request->time_start;
        $end = $request->time_end;

        // dd($time_start->toDateString());
        $bill_7 = BillDetail::select('bill_details.*', 'pitches.pitch_name')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
            ->where('pitches.del_flag', 1)
            ->where('pitches.category_id', 1)
            ->where('bills.status', '<>', 3)
            ->where('bill_details.time_start', '<=', $time_start)
            ->where('bill_details.time_end', '>=', $time_start)
            ->orWhere('bill_details.time_start', '<=', $time_end)
            ->where('bill_details.time_end', '>=', $time_end)
            ->where('pitches.del_flag', 1)
            ->where('pitches.category_id', 1)
            ->where('bills.status', '<>', 3)
            ->orWhere('bill_details.time_start', '>=', $time_start)
            ->where('bill_details.time_end', '<=', $time_end)
            ->where('pitches.del_flag', 1)
            ->where('pitches.category_id', 1)
            ->where('bills.status', '<>', 3)
            ->get();
        $bill_11 = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'pitches.category_id')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
            ->where('pitches.category_id', 2)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)
            ->where('bill_details.time_start', '<=', $time_start)
            ->where('bill_details.time_end', '>=', $time_start)
            ->orWhere('bill_details.time_start', '<=', $time_end)
            ->where('bill_details.time_end', '>=', $time_end)
            ->where('pitches.category_id', 2)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)
            ->orWhere('bill_details.time_start', '>=', $time_start)
            ->where('bill_details.time_end', '<=', $time_end)
            ->where('pitches.category_id', 2)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)
            ->get();
        // dd($bill_11);
        $array_pitch = [];
        if (count($bill_11) != 0) {
            foreach ($listPitch as $data) {
                array_push($array_pitch, $data->id);
            }
        } else {
            foreach ($bill_7 as $data) {
                array_push($array_pitch, $data->pitch_id);
            }
        }
        $listPitchTime = Pitch::select('pitches.*', 'categories.category_name', 'categories.price', 'statuses.status_name', 'statuses.price_change', 'areas.area_name', DB::raw("(categories.price + categories.price*statuses.price_change/100) as `final_price`"))
            ->join('categories', 'categories.id', '=', 'pitches.category_id')
            ->join('statuses', 'statuses.id', '=', 'pitches.status_id')
            ->join('areas', 'areas.id', '=', 'pitches.area_id')
            ->where('pitches.del_flag', 1)
            ->where('pitches.area_id', 1)->get();

        $listTime = Pitch::select('pitches.*', 'bill_details.time_start', 'bill_details.time_end')
            ->join('bill_details', 'bill_details.pitch_id', '=', 'pitches.id')
            ->where('pitches.del_flag', 1)
            ->where('bill_details.time_start', 'like', "%$search%")
            ->where('pitches.area_id', 1)
            ->orderBy('bill_details.time_start', 'ASC')
            ->get();

        return view('livewire.pitch-filter', [
            'listArea' => $listArea,
            'customer' => $customer,
            'listBlog' => $listBlog,
            'introduce' => $introduce,
            'search' => $search,
            'listPitch' => $listPitch,
            'today' => $today,
            'array_pitch' => $array_pitch,
            'start' => $start,
            'end' => $end,
            'listPitchTime' => $listPitchTime,
            'listTime' => $listTime,
            'maxDate3Day' => $maxDate3Day,
        ]);
    }
}
