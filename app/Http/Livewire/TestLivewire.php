<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Post;
use App\Models\Pitch;
use Livewire\Component;
use App\Models\Customer;
use App\Models\Introduce;

class TestLivewire extends Component
{
    public $count = 0;

    public function increment()
    {
        $this->count++;
    }

    public function decrement()
    {
        $this->count--;
    }

    public function render()
    {
        $listBlog = Post::where('del_flag', 1)->orderBy('id', 'desc')->paginate(3);

        // // $id_customer = $request->session()->get('id_customer');
        if (isset($id_customer)) {
            // dd($id_customer);
            $customer = Customer::find($id_customer);
        } else {
            // dd('null');
            $customer = '';
        }
        $introduce = Introduce::all();

        // $search = $request->get('search');
        $listArea = Area::where('del_flag', 1)->get();
        $listPitch = Pitch::where('del_flag', 1)->get();

        return view('livewire.test-livewire', [
            'count' => $this->count,
            'listBlog' => $listBlog,
            'customer' => $customer,
            'introduce' => $introduce,
            'listArea' => $listArea,
            'listPitch' => $listPitch,
        ]);
    }
}
