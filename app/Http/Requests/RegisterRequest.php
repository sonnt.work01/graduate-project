<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'email' => 'required|regex:/(.*)@gmail\.com/i|unique:users',
            'password' => 'required|min:8',
            'date_birth' => 'required',
            'gender' => 'required',
            'phone' => 'required|digits:10|regex:/(0)[0-9]{9}/',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'email.required' => "Email không được để trống!",
            'email.regex' => "Email không hợp lệ!",
            'email.unique' => "Email đã tồn tại!",
            'phone.required' => "Số điện thoại không được để trống!",
            'phone.digits' => "Số điện thoại không hợp lệ, chỉ có 10 chữ số!",
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'password.required' => "Mật khẩu không được để trống!",
            'password.min' => "Mật khẩu tối thiểu 8 ký tự!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'gender.required' => "Giới tính không được để trống!",
        ];
    }
}
