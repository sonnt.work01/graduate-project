<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'email' => 'required|unique:admins',
            'password' => 'required|min:8|',
            'gender' => 'required|',
            'phone' => 'required|digits:10|regex:/(0)[0-9]{9}/',
            'date_birth' => 'required',
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'email.required' => "Email không được để trống!",
            'email.unique' => "Email đã tồn tại!",
            'password.required' => "Mật khẩu không được để trống!",
            'password.min' => "Mật khẩu tối đa 8 ký tự!",
            'gender.required' => "Giới tính không được để trống!",
            'phone.required' => "Số điện thoại không được để trống!",
            'phone.digits' => "Số điện thoại không hợp lệ, chỉ có 10 chữ số!",
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
        ];
    }
}
