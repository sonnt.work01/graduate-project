<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'password',
        'gender',
        'date_birth',
        'address',
        'phone',
        'role',
        'del_flag',
    ];

    public function area()
    {
        return $this->belongsToMany(Area::class);
    }

    public function post()
    {
        return $this->hasMany(Post::class);
    }

    public function getGenderNameAttribute()
    {
        if ($this->gender == 0) {
            return 'Nữ';
        } else {
            return 'Nam';
        }
    }
}
