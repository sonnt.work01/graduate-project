<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminArea extends Model
{
    use HasFactory;

    protected $fillable = [
        'admin_id',
        'area_id',
        'del_flag'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }
}
