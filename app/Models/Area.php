<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;

    protected $fillable = [
        'area_name',
        'area_address',
        'image_path',
        'diagram',
        'del_flag'
    ];

    public function admin()
    {
        return $this->belongsToMany(Admin::class);
    }

    public function pitch()
    {
        return $this->hasMany(Pitch::class);
    }
}
