<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'bill_id',
        'pitch_id',
        'time_start',
        'time_end',
        'pitch_price',
        'price_change',
    ];

    public function pitch()
    {
        return $this->hasMany(Pitch::class);
    }

    public function bill()
    {
        return $this->hasMany(Bill::class);
    }

}
