<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BillPeriodic extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'admin_id',
        'deposit',
        'status',
        'cancellation_reason'
    ];

    public function getStatusNameAttribute()
    {
        if ($this->status == 1) {
            return 'Chờ duyệt';
        } elseif ($this->status == 2) {
            return 'Đã duyệt';
        } elseif ($this->status == 3) {
            return 'Đã huỷ';
        }
        elseif ($this->status == 4) {
            return 'Đã thanh toán';
        }
    }

    public function getTimeAgoAttribute()
    {
        Carbon::setLocale('vi');
        $timeAgo = Carbon::create($this->created_at)->diffForHumans(Carbon::now());
        return $timeAgo;
    }

    public function getPaymentTypeNameAttribute()
    {
        if ($this->payment_type == 0) {
            return 'Trả tiền mặt tại sân';
        } elseif ($this->payment_type == 1) {
            return 'Thanh toán trực tuyến';
        }
    }
}
