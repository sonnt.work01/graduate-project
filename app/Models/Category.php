<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'pitch_id',
        'price',
        'del_flag',
    ];

    public function pitch()
    {
        return $this->hasMany(Pitch::class);
    }
}
