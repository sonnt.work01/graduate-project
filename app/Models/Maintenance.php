<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    use HasFactory;

    protected $fillable = [
        'pitch_id',
        'description',
        'time_start',
        'time_end',
        'del_flag',
    ];

    public function pitch()
    {
        return $this->belongsTo(Pitch::class);
    }
}
