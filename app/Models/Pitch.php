<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pitch extends Model
{
    use HasFactory;

    protected $fillable = [
        'area_id',
        'status_id',
        'category_id',
        'pitch_name',
        'image_path',
        'location',
        'description',
        'del_flag',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function billDetail()
    {
        return $this->hasMany(BillDetail::class);
    }

    public function maintenance()
    {
        return $this->hasMany(Maintenance::class);
    }

    public function checkTime($time_start, $time_end)
    {
        $arr_time = $this->arrTime();
        $check = false;
        foreach ($arr_time as $time) {
            if($time['time_start'] == $time_start
            && $time['time_end'] == $time_end
            ) {
                $check = true;
            }
        }
        return $check;
    }
    
    public function arrTime()
    {
        return [
            0 => [
                'time_start' => '05:30:00',
                'time_end' => '07:00:00',
            ],
            1 => [
                'time_start' => '07:00:00',
                'time_end' => '08:30:00',
            ],
            2 => [
                'time_start' => '08:30:00',
                'time_end' => '10:00:00',
            ],
            3 => [
                'time_start' => '10:00:00',
                'time_end' => '11:30:00',
            ],
            4 => [
                'time_start' => '11:30:00',
                'time_end' => '13:00:00',
            ],
            5 => [
                'time_start' => '13:00:00',
                'time_end' => '14:30:00',
            ],
            6 => [
                'time_start' => '14:30:00',
                'time_end' => '16:00:00',
            ],
            7 => [
                'time_start' => '16:00:00',
                'time_end' => '17:30:00',
            ],
            8 => [
                'time_start' => '17:30:00',
                'time_end' => '19:00:00',
            ],
            9 => [
                'time_start' => '19:00:00',
                'time_end' => '20:30:00',
            ],
            10 => [
                'time_start' => '20:30:00',
                'time_end' => '22:00:00',
            ],
            11 => [
                'time_start' => '22:00:00',
                'time_end' => '23:30:00',
            ],
        ];
    }

    public function fillPitch($time_start, $time_end)
    {
        $bill_7 = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'pitches.id')
        ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
        ->join('bills', 'bills.id', '=', 'bill_details.bill_id')

        ->where('pitches.del_flag', 1)
        ->where('pitches.category_id', 1)
        ->where('bills.status', '<>', 3)
        ->where('bill_details.time_start', '=', $time_start)
        ->where('bill_details.time_end', '=', $time_end)
        ->get();
        // dd($bill_7);
        $maintenances_7 = Maintenance::Where('maintenances.time_start', '<', $time_start)
            ->where('maintenances.time_end', '>', $time_start)
            ->where('maintenances.del_flag', 1)

            ->orWhere('maintenances.time_start', '<', $time_end)
            ->where('maintenances.time_end', '>', $time_end)
            ->where('maintenances.del_flag', 1)

            ->orWhere('maintenances.time_start', '>', $time_start)
            ->where('maintenances.time_end', '<', $time_end)
            ->where('maintenances.del_flag', 1)
            ->get();
        $bill_11 = BillDetail::select('bill_details.*', 'pitches.pitch_name', 'pitches.category_id', 'pitches.id')
            ->join('pitches', 'pitches.id', '=', 'bill_details.pitch_id')
            ->join('bills', 'bills.id', '=', 'bill_details.bill_id')
            ->where('pitches.category_id', 2)
            ->where('pitches.del_flag', 1)
            ->where('bills.status', '<>', 3)
            ->where('bill_details.time_start', '=', $time_start)
            ->where('bill_details.time_end', '=', $time_end)
            ->get();

        $maintenances_11 = Maintenance::select('maintenances.*')
            ->join('pitches', 'pitches.id', '=', 'maintenances.pitch_id')
            ->Where('pitches.category_id', 2)
            ->Where('maintenances.time_start', '<', $time_start)
            ->where('maintenances.time_end', '>', $time_start)
            ->where('maintenances.del_flag', 1)

            ->orWhere('maintenances.time_start', '<', $time_end)
            ->where('maintenances.time_end', '>', $time_end)
            ->Where('pitches.category_id', 2)
            ->where('maintenances.del_flag', 1)

            ->orWhere('maintenances.time_start', '>', $time_start)
            ->where('maintenances.time_end', '<', $time_end)
            ->Where('pitches.category_id', 2)
            ->where('maintenances.del_flag', 1)
            ->get();
            return [
                $bill_7,
                $maintenances_7,
                $bill_11,
                $maintenances_11
            ];
    }
}
