<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price_change',
        'del_flag',
    ];

    public function pitch()
    {
        return $this->hasMany(Pitch::class);
    }
}
