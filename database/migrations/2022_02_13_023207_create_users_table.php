<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->string('password')->nullable();
            $table->string('google_id')->nullable();
            $table->string('verification_code')->nullable();
            $table->boolean('confirm_flag')->default(0);
            $table->date('date_birth')->nullable();
            $table->boolean('gender')->nullable();
            $table->string('phone')->nullable();
            $table->integer('points')->nullable();
            $table->boolean('del_flag')->default(1);
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
