<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('admin_id')->nullable();
            $table->unsignedInteger('bill_periodic_id')->nullable();
            $table->float('deposit');
            $table->float('total_price');
            $table->integer('status');
            $table->string('cancellation_reason', 100)->nullable();
            $table->string('customer_name', 100)->nullable();
            $table->string('customer_phone', 11)->nullable();
            $table->integer('point')->nullable();
            $table->boolean('payment_type');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->foreign('bill_periodic_id')->references('id')->on('bill_periodics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
