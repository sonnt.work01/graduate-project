@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/admin_area" class="btn btn-primary">Quay lại</a>
    <h1>Thêm Quản lý khu vực</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('admin_area.store') }}" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Người quản lý</th>
                    <td>
                        <div class="col-md-8">
                            <select name="admin_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @if ($listAdmin->count() == 0)
                                    <option disabled>Không còn nhân viên phù hợp</option>
                                @else
                                    @foreach ($listAdmin as $data)
                                        <option value="{{ $data->id }}">
                                            {{ $data->name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Khu vực</th>
                    <td>
                        <div class="col-md-8">
                            <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @if ($listArea->count() == 0)
                                    <option disabled>Không còn khu vực phù hợp</option>
                                @else
                                    @foreach ($listArea as $data)
                                        <option value="{{ $data->id }}"">
                                                {{ $data->area_name }}
                                            </option>
                                         @endforeach
                                    @endif
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Thêm</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
