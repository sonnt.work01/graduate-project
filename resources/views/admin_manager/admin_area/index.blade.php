@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Quản lý khu vực</h1>
    <form action="" method="get">
        <div class="col-md-3 col-sm-2">
            <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
            </select>
        </div>
        <div class="col-md-5 col-sm-2">
            <button class="btn btn-info">Chọn</button>
        </div>
    </form>
    <a href="{{ route('admin_area.create') }}" class="btn btn-primary"><i class="material-icons">person_add</i>
        Chọn người quản lý khu vực
    </a>
    <div class="material-datatables">
        @if ($listAdmin->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Họ Tên Admin</th>
                            <th>Khu vực quản lý</th>
                            <th>Trạng thái</th>
                            {{-- <th>Sửa thông tin</th> --}}
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Họ Tên Admin</th>
                            <th>Khu vực quản lý</th>
                            <th>Trạng thái</th>
                            {{-- <th>Sửa thông tin</th> --}}
                        </tr>
                    </tfoot>
                    <?php $i = 0; ?>
                    @foreach ($listAdmin as $data)
                        <?php $i++; ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <th>{{ $data->name }}</th>
                            <td>{{ $data->area_name }}</td>
                            <td>
                                {{ $data->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                            </td>
                            {{-- <td><a href="{{ route('admin_area.edit', $data->admin_id) }}" class="btn btn-success">Sửa</a> --}}
                            {{-- </td> --}}
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    </div>
@endsection
