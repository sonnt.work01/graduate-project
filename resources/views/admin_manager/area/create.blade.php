@extends('layouts.admin')

@section('content')
    <a href="/area" class="btn btn-primary">Quay lại</a>
    <h1>Thêm khu vực</h1>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('area.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tên khu vực
                    </th>
                    <td>
                        <input type="text" name="area_name" class="form-control" required>
                    </td>
                </tr>
                {{-- <tr>
                    <th>
                        Chọn người quản lý
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="admin_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @if ($listAdmin->count() == 0)
                                    <option disabled>Không còn nhân viên phù hợp</option>
                                @else
                                    @foreach ($listAdmin as $data)
                                        <option value="{{ $data->id }}">
                                            {{ $data->name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </td>
                </tr> --}}
                <tr>
                    <th>
                        Chọn ảnh
                    </th>
                    <td>
                        <input type="file" name="image" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Chọn sơ đồ
                    </th>
                    <td>
                        <input type="file" name="diagram" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Địa chỉ
                    </th>
                    <td>
                        <input type="text" name="area_address" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Thêm khu vực</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
