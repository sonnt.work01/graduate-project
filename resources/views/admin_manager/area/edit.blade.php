@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/area" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thông tin khu vực</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('area.update', $area->id) }}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tên khu vực
                    </th>
                    <td>
                        <input type="text" name="area_name" value="{{ $area->area_name }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Chọn ảnh
                    </th>
                    <td>
                        <img src="{{ asset('images/' . $area->image_path) }}" style="width: 350px; height:350px">
                        <input type="file" name="image">
                        <input type="hidden" name="hidden_image" value="{{ $area->image_path }}">
                    </td>
                </tr>
                <tr>
                    <th>
                        Chọn sơ đồ
                    </th>
                    <td>
                        <img src="{{ asset('images/' . $area->diagram) }}" style="width: 350px; height:350px">
                        <input type="file" name="diagram">
                        <input type="hidden" name="hidden_diagram" value="{{ $area->diagram }}">
                    </td>
                </tr>
                <tr>
                    <th>
                        Địa chỉ
                    </th>
                    <td>
                        <input type="text" name="area_address" value="{{ $area->area_address }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Trạng thái hoạt động
                    </th>
                    <td>
                        <input type="radio" name="del_flag" id="1" value="1" @if ($area->del_flag == 1) checked @endif><label for="1"
                            style="color: black"><b>Hoạt động</b></label>
                        <input type="radio" name="del_flag" id="0" value="0" @if ($area->del_flag == 0) checked @endif><label for="0"
                            style="color: black"><b>Dừng hoạt động</b></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
