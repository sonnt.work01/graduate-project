@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Danh sách khu vực</h1>
    <form action="" method="get">
        <div class="col-md-3 col-sm-2">
            <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
            </select>
        </div>
        <div class="col-md-5 col-sm-2">
            <button class="btn btn-info">Chọn</button>
        </div>
    </form>
    @if ($listArea->count() == 0)
        <a href="{{ route('area.create') }}" class="btn btn-primary"><i class="material-icons">add_circle</i>
            Thêm khu vực</a>
    @else
        <a disabled style="pointer-events: none;" class="btn btn-primary"><i class="material-icons">add_circle</i>
            Thêm khu vực</a>
    @endif
    <div class="material-datatables">
        @if ($listArea->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên khu vực</th>
                            {{-- <th>Người quản lý</th> --}}
                            <th>Ảnh</th>
                            <th>Sơ đồ</th>
                            <th>Địa chỉ</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Tên khu vực</th>
                            {{-- <th>Người quản lý</th> --}}
                            <th>Ảnh</th>
                            <th>Sơ đồ</th>
                            <th>Địa chỉ</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listArea as $area)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <th>
                                    {{ $area->area_name }}
                                </th>
                                {{-- <td>
                                    {{ $area->name }}
                                </td> --}}
                                <td>
                                    <img src="{{ asset('images/' . $area->image_path) }}"
                                        style="width:250px; height:250px;">
                                </td>
                                <td>
                                    <img src="{{ asset('images/' . $area->diagram) }}" style="width:250px; height:250px;">
                                </td>

                                <td>
                                    {{ $area->area_address }}
                                </td>
                                <td>
                                    {{ $area->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                </td>
                                <td>
                                    <a href="{{ route('area.edit', $area->id) }}">
                                        <button type="button" class="btn btn-success">
                                            <i class="material-icons">edit</i>
                                            Sửa
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
