@extends('layouts.admin')

@section('content')
    <div>
        <a href="{{ route('bill-periodic') }}" class="btn btn-warning">Hoá đơn đặt định kì</a>
    </div>
    <h2>Hoá đơn</h2>
    <div class="row">
        <div class="col-md-12">
            <h4 id="success" style="color:rgb(49, 156, 0); font-weight: bold"></h4>
            <h4 id="error" style="color:red; font-weight: bold"></h4>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <form action="" method="get">
        {{-- @if ($admin->role == 1)
        <div class="col-md-4 col-sm-4">
            <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                @foreach ($area as $data)
                    <option value="{{ $data->id }}" @if ($data->id == $area_id) ? selected @endif>{{ $data->area_name }}</option>
                @endforeach
            </select>
        </div> --}}
        <div class="col-md-4 col-sm-4">
            <select name="bill_active" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($bill_active == 1) ? selected @endif>Chưa xử lý (Đang chờ duyệt)
                    {{ $bill->count() }} đơn</option>
                <option value="2" @if ($bill_active == 2) ? selected @endif>Đã duyệt</option>
                <option value="3" @if ($bill_active == 3) ? selected @endif>Đã huỷ</option>
                <option value="4" @if ($bill_active == 4) ? selected @endif>Đã thanh toán</option>
            </select>
        </div>
        <div class="col-md-4 col-sm-4">
            <select name="isAccount" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="account" @if ($isAccount == 'account') ? selected @endif>Có tài khoản</option>
                <option value="no_account" @if ($isAccount == 'no_account') ? selected @endif>Vãng lai</option>
            </select>
        </div>
        <div class="col-md-4 col-sm-4">
            <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
            <button class="btn btn-info">Tìm kiếm</button>
        </div>
        {{-- @else
        <div class="col-md-6 col-sm-6">
            <select name="bill_active" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($bill_active == 1) ? selected @endif>Chưa xử lý</option>
                <option value="2" @if ($bill_active == 2) ? selected @endif>Đã đặt</option>
                <option value="3" @if ($bill_active == 3) ? selected @endif>Đã huỷ</option>
                <option value="4" @if ($bill_active == 4) ? selected @endif>Đã thanh toán</option>
            </select>
        </div>
        <div class="col-md-6 col-sm-6">
            <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
            <button class="btn btn-info">Tìm kiếm</button>
        </div>
        @endif --}}
    </form>
    @if ($listBill->count() == 0)
        {{ 'Không có bản ghi' }}
    @endif
    @if ($bill_active == 1)
        <div class="material-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%"
                style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th width="100px">
                            Trạng thái
                        </th>
                        <th>
                            Đã đặt từ
                        </th>
                        <th>
                            Hình thức thanh toán đặt cọc
                        </th>
                        <th>
                            Hành động
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th width="100px">
                            Trạng thái
                        </th>
                        <th>
                            Đã đặt từ
                        </th>
                        <th>
                            Hình thức thanh toán đặt cọc
                        </th>
                        <th>
                            Hành động
                        </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php $i = 0; ?>
                    @foreach ($listBill as $bill)
                        <?php $i++; ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>
                                @if ($bill->bill_periodic_id)
                                    <b style=" color:rgb(255, 0, 0);">
                                        {{ 'Đặt định kì' }}
                                    </b>
                                @else
                                    <b style="color:rgb(38, 0, 255);">
                                        {{ 'Đặt thường' }}
                                    </b>
                                @endif
                            </td>
                            <td>
                                @if ($bill->bill_periodic_id)
                                    <b>BKSĐK{{ $bill->id }}</b>
                                @else
                                    <b>BKS{{ $bill->id }}</b>
                                @endif
                            </td>
                            <th>
                                @if ($bill->user_id == null)
                                    <b>{{ $bill->customer_name }} - {{ $bill->customer_phone }}</b>
                                @else
                                    <b>{{ $bill->user_name }} - {{ $bill->phone }}</b>
                                @endif
                            </th>
                            <td>
                                {{ $bill->created_at }}
                            </td>
                            <td>
                                {{ number_format($bill->deposit, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                {{ number_format($bill->total_price, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                {{ number_format($bill->missing, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                <b style="background-color: blue; color:white; padding:5px; border-radius: 4px">
                                    {{ $bill->StatusName }}
                                </b>
                            </td>
                            <?php
                            date_default_timezone_set('Asia/Ho_Chi_Minh');
                            $today = date('Y-m-d H:m:s');
                            $num = strtotime($bill->created_at);
                            $now = strtotime($today);
                            $time = $now - $num;
                            ?>
                            <td style="color: red; font-weight:bold">
                                {{ $bill->TimeAgo }}
                            </td>
                            <td style="color: red; font-weight:bold">
                                {{ $bill->PaymentTypeName }}
                            </td>
                            @if (!$bill->bill_periodic_id)
                                <td>
                                    <a href="{{ route('bill-detail', $bill->id) }}" class="btn">Chi tiết</a>
                                    <form action="{{ route('bill.update', $bill->id) }}" method="post">
                                        @csrf
                                        @method('PATCH')
                                        <button type="submit" onclick="return confirm('Bạn có chắc chắn duyệt đơn này?')"
                                            class="btn btn-success">Duyệt</button>
                                    </form>
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#destroyModal">
                                        Huỷ đơn
                                    </button>
                                    {{-- start modal destroy --}}
                                    <div class="modal fade" id="destroyModal" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <form action="{{ route('bill.destroy', $bill->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">Lý do huỷ đơn
                                                        </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="col-md-12">
                                                            <textarea type="text" name="cancellation_reason" style="width:100%;" class="form-control"
                                                                placeholder="Nhập lí do huỷ đơn..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Huỷ</button>
                                                        <button type="submit"
                                                            onclick="return confirm('Bạn có chắc chắn huỷ đơn này?')"
                                                            class="btn btn-danger">
                                                            Huỷ đơn
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    {{-- end modal destroy --}}
                                </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @elseif ($bill_active == 2)
        <div class="material-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th width="100px">
                            Trạng thái
                        </th>
                        <th>
                            Hành động
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th width="100px">
                            Trạng thái
                        </th>
                        <th>
                            Hành động
                        </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php $i = 0; ?>
                    @foreach ($listBill as $bill)
                        <?php $i++; ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>
                                @if ($bill->bill_periodic_id)
                                    <b style=" color:rgb(255, 0, 0);">
                                        {{ 'Đặt định kì' }}
                                    </b>
                                @else
                                    <b style="color:rgb(38, 0, 255);">
                                        {{ 'Đặt thường' }}
                                    </b>
                                @endif
                            </td>
                            <th>
                                @if ($bill->bill_periodic_id)
                                    <b>BKSĐK{{ $bill->id }}</b>
                                @else
                                    <b>BKS{{ $bill->id }}</b>
                                @endif
                            </th>
                            <th>
                                @if ($bill->user_id == null)
                                    <b>{{ $bill->customer_name }} - {{ $bill->customer_phone }}</b>
                                @else
                                    <b>{{ $bill->user_name }} - {{ $bill->phone }}</b>
                                @endif
                            </th>
                            <td>
                                {{ $bill->created_at }}
                            </td>
                            <td>
                                {{ number_format($bill->deposit, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                {{ number_format($bill->total_price, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                {{ number_format($bill->missing, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                <b style="background-color: green; color:white; padding:6px; border-radius:5px">
                                    {{ $bill->StatusName }}
                                </b>
                            </td>
                            <td>
                                <a href="{{ route('bill-detail', $bill->id) }}" class="btn">Chi tiết</a>
                                @if (!$bill->bill_periodic_id)
                                <form action="{{ route('bill.update', $bill->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <button class="btn btn-success">Thanh toán</button>
                                </form>
                                @else
                                    <button class="btn btn-success" data-toggle="modal" data-target="#paymentTypeModal">
                                        Thanh toán
                                    </button>
                                    <!-- Modal payment -->
                                    <div class="modal fade" id="paymentTypeModal" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <form action="{{ route('bill.update', $bill->id) }}" method="post">
                                                @csrf
                                                @method('PATCH')
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">Hình thức thanh
                                                            toán khách hàng yêu cầu
                                                        </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <select name="payment_type" class="form-control" required>
                                                            <option value="" selected disabled>Chọn hình thức thanh
                                                                toán
                                                            </option>
                                                            <option value="0">Trả tiền mặt</option>
                                                            <option value="1">Thanh toán trực tuyến (Momo, Ngân hàng)
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Huỷ</button>
                                                        <button type="submit"
                                                            onclick="return confirm('Bạn có chắc chắn hoàn thành thanh toán đơn này?')"
                                                            class="btn btn-success">Thanh toán</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- end modal -->
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#destroyModal">
                                        Huỷ đơn
                                    </button>
                                    {{-- start modal destroy --}}
                                    <div class="modal fade" id="destroyModal" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <form action="{{ route('bill-periodic-destroy-single', $bill->id) }}"
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">Lý do huỷ đơn
                                                        </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="col-md-12">
                                                            <textarea type="text" name="cancellation_reason" style="width:100%;" class="form-control"
                                                                placeholder="Nhập lí do huỷ đơn..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Huỷ</button>
                                                        <button type="submit"
                                                            onclick="return confirm('Bạn có chắc chắn huỷ đơn này?')"
                                                            class="btn btn-danger">
                                                            Huỷ đơn
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    {{-- end modal destroy --}}
                                @endif
                                @if ($bill->user_id == null)
                                    <a class="btn" href="{{ route('export-bill-pdf-no-account', $bill->id) }}">Xuất
                                        file
                                        PDF</a>
                                @else
                                    <a class="btn btn-warning" href="{{ route('export-bill-pdf', $bill->id) }}">Xuất file
                                        PDF</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @elseif($bill_active == 3)
        <div class="material-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        <th>
                            Lí do huỷ
                        </th>
                        <th>
                            Chi tiết
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        <th>
                            Lí do huỷ
                        </th>
                        <th>
                            Chi tiết
                        </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php $i = 0; ?>
                    @foreach ($listBill as $bill)
                        <?php $i++; ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>
                                @if ($bill->bill_periodic_id)
                                    <b style=" color:rgb(255, 0, 0);">
                                        {{ 'Đặt định kì' }}
                                    </b>
                                @else
                                    <b style="color:rgb(38, 0, 255);">
                                        {{ 'Đặt thường' }}
                                    </b>
                                @endif
                            </td>
                            <th>
                                @if ($bill->bill_periodic_id)
                                    <b>BKSĐK{{ $bill->id }}</b>
                                @else
                                    <b>BKS{{ $bill->id }}</b>
                                @endif
                            </th>
                            <th>
                                @if ($bill->user_id == null)
                                    <b>{{ $bill->customer_name }} - {{ $bill->customer_phone }}</b>
                                @else
                                    <b>{{ $bill->user_name }} - {{ $bill->phone }}</b>
                                @endif
                            </th>
                            <td>
                                {{ $bill->created_at }}
                            </td>
                            <td>
                                {{ number_format($bill->deposit, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                {{ number_format($bill->total_price, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                {{ number_format($bill->missing, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                <b style="background-color: red; color:white;padding:6px; border-radius:5px">
                                    {{ $bill->StatusName }}
                                </b>
                            </td>
                            <td>
                                <b style="color: red">
                                    {{ $bill->cancellation_reason }}
                                </b>
                            </td>
                            <td>
                                <a href="{{ route('bill-detail', $bill->id) }}" class="btn">Chi tiết</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="material-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        <th>
                            Xuất hoá đơn PDF
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>STT</th>
                        <th>
                            Thể loại đơn
                        </th>
                        <th>
                            Mã hoá đơn
                        </th>
                        <th>
                            Khách hàng
                        </th>
                        <th>
                            Thời gian đặt
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tổng tiền
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        <th>
                            Xuất hoá đơn PDF
                        </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php $i = 0; ?>
                    @foreach ($listBill as $bill)
                        <?php $i++; ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>
                                @if ($bill->bill_periodic_id)
                                    <b style=" color:rgb(255, 0, 0);">
                                        {{ 'Đặt định kì' }}
                                    </b>
                                @else
                                    <b style="color:rgb(38, 0, 255);">
                                        {{ 'Đặt thường' }}
                                    </b>
                                @endif
                            </td>
                            <th>
                                @if ($bill->bill_periodic_id)
                                    <b>BKSĐK{{ $bill->id }}</b>
                                @else
                                    <b>BKS{{ $bill->id }}</b>
                                @endif
                            </th>
                            <th>
                                @if ($bill->user_id == null)
                                    <b>{{ $bill->customer_name }} - {{ $bill->customer_phone }}</b>
                                @else
                                    <b>{{ $bill->user_name }} - {{ $bill->phone }}</b>
                                @endif
                            </th>
                            <td>
                                {{ $bill->created_at }}
                            </td>
                            <td>
                                {{ number_format($bill->deposit, 0, '', ',') . 'đ' }}
                            </td>
                            <td>
                                {{ number_format($bill->total_price, 0, '', ',') . 'đ' }}
                            </td>
                            {{-- <td>
                                {{ number_format($bill->missing, 0, '', ',') . 'đ' }}
                            </td> --}}
                            <td>
                                <b style="background-color: rgb(255, 145, 0); color:white;padding:6px; border-radius:5px">
                                    {{ $bill->StatusName }}
                                </b>
                            </td>
                            <td>
                                <a href="{{ route('bill-detail', $bill->id) }}" class="btn">chi tiết</a>
                                <br>
                                @if ($bill->user_id == null)
                                    <a class="btn btn-warning"
                                        href="{{ route('export-bill-pdf-no-account', $bill->id) }}">Xuất file PDF</a>
                                @else
                                    <a class="btn btn-warning" href="{{ route('export-bill-pdf', $bill->id) }}">Xuất file
                                        PDF</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection
