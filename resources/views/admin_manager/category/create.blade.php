@extends('layouts.admin')

@section('content')
    <a href="/category" class="btn btn-primary">Quay lại</a>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('category.store') }}" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>
                        <b>Tên thể loại</b>
                        <input type="text" name="category_name" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Giá thể loại</b>
                        <input type="text" name="price" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Phần trăm tiền cần đặt cọc khi đặt sân (%)</b>
                        <input type="text" name="deposit" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Thêm</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
