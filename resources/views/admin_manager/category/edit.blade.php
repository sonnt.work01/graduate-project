@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/category" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thể loại</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('category.update', $category->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>
                        <b>Tên thể loại</b>
                        <input type="text" name="category_name" class="form-control"
                            value="{{ $category->category_name }}" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Giá thể loại (VND)</b>
                        <input type="text" name="price" class="form-control" value="{{ $category->price }}" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Phần trăm tiền cần đặt cọc khi đặt sân (%)</b>
                        <input type="text" name="deposit" class="form-control" value="{{ $category->deposit }}"
                            required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Trạng thái </b>
                        <input type="radio" name="del_flag" id="1" value="1"
                            @if ($category->del_flag == 1) checked @endif><label for="1" style="color: black"><b>Hoạt
                                động</b></label>
                        <input type="radio" name="del_flag" id="0" value="0"
                            @if ($category->del_flag == 0) checked @endif><label for="0" style="color: black"><b>Ngừng
                                hoạt động</b></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
