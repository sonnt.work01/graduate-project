@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Thể loại</h1>
    <form action="" method="get">
        <div class="row">
            <div class="col-md-3 col-sm-2">
                <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                    <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                    <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
                </select>
            </div>
            <div class="col-md-5 col-sm-2">
                <button class="btn btn-info">Chọn</button>
            </div>
            <div class="col-md-4 col-sm-2">
                @if ($listCategory->count() == 0)
                <a href="{{ route('category.create') }}" class="btn btn-primary">
                    <i class="material-icons">add_circle</i>Thêm thể loại
                </a>
                @else
                <a href="{{ route('category.create') }}"  class="btn btn-primary">
                    <i class="material-icons">add_circle</i>Thêm thể loại
                </a>
                @endif
            </div>
        </div>
    </form>
    <div class="material-datatables">
        @if ($listCategory->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên thể loại</th>
                            <th>Giá thể loại</th>
                            <th>Phần trăm tiền cần đặt cọc khi đặt sân</th>
                            <th>Trạng thái</th>
                            <th>Sửa</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Tên thể loại</th>
                            <th>Giá thể loại</th>
                            <th>Phần trăm tiền cần đặt cọc khi đặt sân</th>
                            <th>Trạng thái</th>
                            <th>Sửa</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listCategory as $data)
                            <?php $i++; ?>
                            <tr>
                                <th>{{ $i }}</th>
                                <td>{{ $data->category_name }}</td>
                                <td>{{ number_format($data->price, 0, '', ',') . 'đ' }}</td>
                                <td>{{ $data->deposit }} (%)</td>
                                <td>
                                    {{ $data->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                </td>
                                <td>
                                    <a href="{{ route('category.edit', $data->id) }}" class="btn btn-success">Sửa</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
