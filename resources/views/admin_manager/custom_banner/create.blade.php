@extends('layouts.admin')

@section('content')
    <a href="/custom_banner" class="btn btn-primary">Quay lại</a>
    <h1>Thêm khu vực</h1>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('custom_banner.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Chọn ảnh banner
                    </th>
                    <td>
                        <input type="file" name="image" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Chọn vị trí hiện thị
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="location" data-style="btn btn-primary btn-round">
                                <option disabled selected>Chọn vị trí hiển thị</option>
                                <option value="1">Banner trên đầu trang chủ</option>
                                <option value="2">Banner dưới trang chủ</option>
                                <option value="3">Banner trang giới thiệu</option>
                                <option value="4">Banner trang đặt sân</option>
                                <option value="5">Banner trang blog</option>
                                <option value="6">Banner trang liên hệ</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Thêm banner</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
