@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Danh sách banner</h1>
    <a href="{{ route('custom_banner.create') }}" class="btn btn-primary"><i class="material-icons">add_circle</i>Thêm
        banner</a>
    <div class="material-datatables">
        @if ($listBanner->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Ảnh banner</th>
                            <th>Vị trí banner</th>
                            <th>Đổi ảnh banner</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Ảnh banner</th>
                            <th>Vị trí banner</th>
                            <th>Đổi ảnh banner</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listBanner as $banner)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    <img src="{{ asset('images/' . $banner->image_path) }}"
                                        style="width:250px; height:250px;">
                                </td>
                                <th>
                                    @if ($banner->location == 1)
                                        {{ 'Banner trên đầu trang chủ' }}
                                    @elseif($banner->location == 2)
                                        {{ 'Banner dưới trang chủ' }}
                                    @elseif($banner->location == 3)
                                        {{ 'Banner trang giới thiệu' }}
                                    @elseif($banner->location == 4)
                                        {{ 'Banner trang đặt sân' }}
                                    @elseif($banner->location == 5)
                                        {{ 'Banner trang blog' }}
                                    @elseif($banner->location == 6)
                                        {{ 'Banner trang liên hệ' }}
                                    @endif
                                </th>
                                <td>
                                    <a href="{{ route('custom_banner.edit', $banner->id) }}">
                                        <button type="button" class="btn btn-success">
                                            <i class="material-icons">edit</i>
                                            Đổi ảnh
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
