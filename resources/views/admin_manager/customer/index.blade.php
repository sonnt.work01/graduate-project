@extends('layouts.admin')

@section('content')
    <h1>Danh sách khách hàng</h1>
    <form action="" method="get">
        <div class="col-md-3 col-sm-2">
        <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
            <option value="1" @if ($del_flag==1) selected @endif>Hoạt động</option>
            <option value="0" @if ($del_flag==0) selected @endif>Không hoạt động</option>
        </select>
        </div>
        <div class="col-md-5 col-sm-2">
            <button class="btn btn-info">Chọn</button>
        </div>
</form>
<div class="material-datatables">
        @if ($listCustomer->count() == 0)
            {{ "Không có bản ghi" }}
        @else
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
                <tr>
                <th>STT</th>
                <th>Họ tên</th>
                <th>Email</th>
                <th>Ngày sinh</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
            </tr>
            </thead>
            <tfoot>
                <tr>
                <th>STT</th>
                <th>Họ tên</th>
                <th>Email</th>
                <th>Ngày sinh</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
            </tr>
            </tfoot>
            <tbody>
            <?php $i = 0 ?>
            @foreach ($listCustomer as $customer)
                        <?php $i++ ?>
            <tr>
                <td>{{ $i }}</td>
                <th>{{ $customer->name }}</th>
                <td>{{ $customer->email }}</td>
                <td>{{ $customer->date_birth }}</td>
                <td>{{ $customer->GenderName }}</td>
                <td>{{ $customer->phone }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>
@endsection
