@extends('layouts.admin')

@section('content')
    <style>
        .fc-unthemed .fc-today {
            background: #ffdbdb;
        }

        /* .fc-today-button {
                                        display: none;
                                    } */

    </style>
    <div class="container" style="width:100%;">
        <div class="row">
            <div class="col">
                <h3>Tổng hợp lịch đá của ngày hôm nay {{ date('d/m/Y', strtotime($today)) }}</h3>
                <p style="font-size: 18px">Số sân đặt: {{ count($order) }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background: #2e6da4; color:white">
                        <p style="text-align:center; font-size:30px; text-transform: uppercase;">
                            Thời gian biểu sân bóng
                        </p>
                    </div>
                </div>
                <div class="panel-body">
                    {!! $calendar->calendar() !!}
                    {!! $calendar->script() !!}
                </div>
            </div>

        </div>
    </div>
    <div>
        <h1>Doanh thu</h1>
    </div>
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h3 id="error" style="color:red; font-weight: bold; font-size: 18px"></h3>
        </div>
    </div>
    <form id="form_filter" data-url={{ route('chart-filter') }} method="post">
        <div class="col-md-12 col-sm-12">
            <b>Từ </b><input id="date_start" type="date" value="{{ $time_start }}" style="padding: 5px">
            <b>Đến </b><input id="date_end" type="date" value="{{ $time_end }}" min="" style="padding: 5px">
            <button type="submit" class="btn btn-info">Tìm kiếm</button>
        </div>
    </form>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div style="background-color: rgb(255, 217, 0)" class="card bg-warning">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <span style="font-size: 40px" class="material-icons">
                                    insights
                                </span>
                            </div>
                            <div class="col-xs-7">
                                <div class="pitch_name">
                                    <h4>Tổng doanh thu</h4>
                                    <span id="tong_doanh_thu" style="color: rgb(13, 153, 0); font-size:25px">
                                        {{ number_format($total_doanh_thu) . 'đ' }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="pitch_col" style="width:100%; height:400px;"></div>
    <div id="pitch_pie"></div>

    
    {{-- <h2>Top sân được đặt nhiều nhất trong tháng</h2>

    <h2>Top khung giờ đặt nhiều nhất trong tháng</h2> --}}
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    
    <script type="text/javascript">
        $(document).ready(function() {
            //update
            $('#form_filter').submit(function(event) {
                event.preventDefault();
                let url = $(this).attr('data-url');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "post",
                    data: {
                        date_start: $('#date_start').val(),
                        date_end: $('#date_end').val(),
                    },
                    success: function(response) {
                        console.log(response.message);
                        let arr_doanh_thu_san = response.arr_doanh_thu_san;
                        const arr_phan_tram_doanh_thu_san_theo_thang = [];
                        const arr_doanh_thu_san_theo_thang = [];
                        var arr_pitch = response.arr_pitch;
                        var arr_name_pitch = response.arr_name_pitch;
                        var total_doanh_thu = response.total_format;
                        var tong_doanh_thu_san_theo_thang = 0;

                        if(response.hasError){
                            $('#error').append(response.message);
                            setTimeout(function() {
                                $('#error').empty();
                            }, 4000);
                        } else if(!response.hasError) {
                            
                            $('#tong_doanh_thu').empty();
                            $('#tong_doanh_thu').append(total_doanh_thu);
    
                            for (var i = 0; i < arr_pitch.length; i++) {
                                tong_doanh_thu_san_theo_thang += arr_doanh_thu_san[i];
                            }
                            for (var i = 0; i < arr_pitch.length; i++) {
                                arr_phan_tram_doanh_thu_san_theo_thang.push({
                                    name: arr_name_pitch[i],
                                    y: (arr_doanh_thu_san[i] /
                                        tong_doanh_thu_san_theo_thang) * 100
                                });
                            }
                            for (var i = 0; i < arr_pitch.length; i++) {
                                arr_doanh_thu_san_theo_thang.push([arr_name_pitch[i],
                                    arr_doanh_thu_san[i]
                                ]);
                            }
                            Highcharts.chart('pitch_col', {
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: 'Doanh thu các sân'
                                },
                                xAxis: {
                                    type: 'category',
                                    labels: {
                                        style: {
                                            fontSize: '13px',
                                            fontFamily: 'Verdana, sans-serif'
                                        }
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Doanh thu'
                                    }
                                },
                                legend: {
                                    enabled: false
                                },
                                // tooltip: {
                                //     pointFormat: 'Doanh thu: <b>{point.y:.1f} </b>'
                                // },
                                series: [{
                                    name: 'Doanh thu',
                                    data: arr_doanh_thu_san_theo_thang,
                                    // dataLabels: {
                                    //     enabled: true,
                                    //     rotation: -90,
                                    //     color: '#FFFFFF',
                                    //     align: 'right',
                                    //     format: '{point.y:.1f}', // one decimal
                                    //     y: 10, // 10 pixels down from the top
                                    //     style: {
                                    //         fontSize: '12px',
                                    //         fontFamily: 'Verdana, sans-serif'
                                    //     }
                                    // }
                                }]
                            });
    
                            Highcharts.chart('pitch_pie', {
                                chart: {
                                    type: 'pie'
                                },
                                title: {
                                    text: 'Phần trăm doanh thu của các sân'
                                },
                                accessibility: {
                                    announceNewData: {
                                        enabled: true
                                    },
                                    point: {
                                        valueSuffix: '%'
                                    }
                                },
    
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            format: '{point.name}: {point.y:.1f}%'
                                        }
                                    }
                                },
    
                                tooltip: {
                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                                },
    
                                series: [{
                                    name: "Khu vực",
                                    colorByPoint: true,
                                    data: arr_phan_tram_doanh_thu_san_theo_thang,
                                }],
                            });
                        }
                        

                    },
                });
            });
        });
    </script>

    <script type="text/javascript">
        @php
        $js_arr_pitch = json_encode($arr_pitch);
        $js_arr_name_pitch = json_encode($arr_name_pitch);
        $js_doanh_thu_san = json_encode($arr_doanh_thu_san);
        @endphp
        const arr_phan_tram_doanh_thu_san_theo_thang = [];
        const arr_doanh_thu_san_theo_thang = [];
        let tong_doanh_thu_khu_vuc_theo_thang = 0;
        var arr_pitch = {!! $js_arr_pitch !!};
        var arr_name_pitch = {!! $js_arr_name_pitch !!};
        var arr_doanh_thu_san = {!! $js_doanh_thu_san !!};
        let tong_doanh_thu_san_theo_thang = 0;

        for (var i = 0; i < arr_pitch.length; i++) {
            tong_doanh_thu_san_theo_thang += arr_doanh_thu_san[i];
        }
        for (var i = 0; i < arr_pitch.length; i++) {
            arr_phan_tram_doanh_thu_san_theo_thang.push({
                name: arr_name_pitch[i],
                y: (arr_doanh_thu_san[i] / tong_doanh_thu_san_theo_thang) * 100
            });
        }
        for (var i = 0; i < arr_pitch.length; i++) {
            arr_doanh_thu_san_theo_thang.push([arr_name_pitch[i], arr_doanh_thu_san[i]]);
        }

        Highcharts.chart('pitch_col', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Doanh thu các sân'
            },
            xAxis: {
                type: 'category',
                labels: {
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Doanh thu'
                }
            },
            legend: {
                enabled: false
            },
            // tooltip: {
            //     pointFormat: 'Doanh thu: <b>{point.y:.1f} </b>'
            // },
            series: [{
                name: 'Doanh thu',
                data: arr_doanh_thu_san_theo_thang,
                // dataLabels: {
                //     enabled: true,
                //     rotation: -90,
                //     color: '#FFFFFF',
                //     align: 'right',
                //     format: '{point.y:.1f}', // one decimal
                //     y: 10, // 10 pixels down from the top
                //     style: {
                //         fontSize: '12px',
                //         fontFamily: 'Verdana, sans-serif'
                //     }
                // }
            }]
        });

        Highcharts.chart('pitch_pie', {
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Phần trăm doanh thu của các sân'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                name: "Khu vực",
                colorByPoint: true,
                data: arr_phan_tram_doanh_thu_san_theo_thang,
            }],
        });
    </script>
@endsection
