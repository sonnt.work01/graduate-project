@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/introduce" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thông tin giới thiệu</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h3 id="error" style="color:red; font-weight: bold"></h3>
            <h3 id="success" style="color:rgb(49, 156, 0); font-weight: bold"></h3>
        </div>
    </div>
    <form action="{{ route('introduce.update', $introduce->id) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tên công ty
                    </th>
                    <td>
                        <input type="text" name="company" value="{{ $introduce->company }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Ảnh đại diện
                    </th>
                    <td width="50%">
                        <img src="{{ asset('images/' . $introduce->avatar) }}" style="width: 350px; height:350px">
                        <input type="file" name="avatar">
                        <input type="hidden" name="hidden_avatar" value="{{ $introduce->avatar }}">
                    </td>
                </tr>
                <tr>
                    <th>
                        Thông tin giới thiệu
                    </th>
                    <td>
                        <textarea name="content" value="{{ $introduce->content }}" class="form-control" rows="5"
                            required>{{ $introduce->content }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        Địa chỉ
                    </th>
                    <td>
                        <input type="text" name="address" value="{{ $introduce->address }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Ngày đăng
                    </th>
                    <td><input type="date" name="date_submitted" value="{{ $introduce->date_submitted }}"
                            class="form-control" required></td>
                </tr>
                <tr>
                    <th>
                        Trạng thái hoạt động
                    </th>
                    <td>
                        <input type="radio" name="del_flag" id="1" value="1" @if ($introduce->del_flag == 1) checked @endif><label for="1"
                            style="color: black"><b>Hiện</b></label>
                        <input type="radio" name="del_flag" id="0" value="0" @if ($introduce->del_flag == 0) checked @endif><label for="0"
                            style="color: black"><b>Ẩn</b></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
