@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Thông tin giới thiệu</h1>
    @if ($listIntroduce->count() == 0)
        <a href="{{ route('introduce.create') }}" class="btn btn-primary">
            <i class="material-icons">person_add</i>
            Thêm thông tin</a>
    @else
        <a class="btn btn-primary" disabled style="pointer-events: none;">
            <i class="material-icons">person_add</i>
            Thêm thông tin</a>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h3 id="error" style="color:red; font-weight: bold"></h3>
            <h3 id="success" style="color:rgb(49, 156, 0); font-weight: bold"></h3>
        </div>
    </div>
    @if ($listIntroduce->count() == 0)
        {{ 'Không có bản ghi' }}
    @else
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>STT</th>
                    <th>Tên công ty</th>
                    <th>Ảnh đại diện</th>
                    <th>Thông tin giới thiệu</th>
                    <th>Địa chỉ công ty</th>
                    <th>Ngày đăng</th>
                    <th>Trạng thái</th>
                    <th>Sửa thông tin</th>
                </tr>
                <?php $i = 0; ?>
                @foreach ($listIntroduce as $introduce)
                    <?php $i++; ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <th>{{ $introduce->company }}</th>
                        <td>
                            <img src="{{ asset('images/' . $introduce->avatar) }}" style="width:200px; height:200px">
                        </td>
                        <td>{{ $introduce->content }}</td>
                        <td>{{ $introduce->address }}</td>
                        <td>{{ $introduce->date_submitted }}</td>
                        <td>
                            {{ $introduce->del_flag == 1 ? 'Hiển thị' : 'Ẩn' }}
                        </td>
                        <td>
                            <a href="{{ route('introduce.edit', $introduce->id) }}" class="btn btn-success">Sửa</a>
                            {{-- <button data-url="{{ route('introduce.edit', $introduce->id) }}"
                                class="btn btn-warning btnEdit" type="button" data-toggle="modal"
                                data-target="#modalUpdateIntroduce">
                                Sửa
                            </button> --}}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endif

    {{-- <div class="modal fade" id="modalUpdateIntroduce" tabindex="-1" aria-labelledby="EditIntroduceModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" style="overflow-y: initial !important">
            <div class="modal-content" style="height: 80vh;
                        overflow-y: auto;">
                <form action="" data-url="{{ route('introduce.update', $introduce->id) }}" id="form_edit_intro" method="post">
                    @csrf
                    <div class="modal-header">
                        <h2>Sửa thông tin</h2>
                    </div>
                    <div class="modal-body">
                        <table>
                            <tr>
                                <td>
                                    <label for="company_edit" class="col-form-control">
                                        <b>Tên công ty:</b>
                                        <input type="text" size="100%" class="form-control" id="company_edit" required>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="col-form-control">
                                        <b>Ảnh đại diện:</b>
                                        <img src="{{ asset('images/' . $introduce->avatar) }}"
                                            style="width: 350px; height:350px">
                                        <input type="file" id="avatar">
                                        <input type="hidden" id="hidden_avatar">
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="content" class="col-form-control">
                                        <b>Thông tin giới thiệu:</b>
                                        <textarea id="content" class="form-control" size="100%" rows="5" required></textarea>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="address" class="col-form-control">
                                        <b>Địa chỉ:</b>
                                        <input type="text" size="100%" id="address" class="form-control" required>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="date_submitted" class="col-form-control">
                                        <b>Địa chỉ:</b>
                                        <input type="date" size="100%" id="date_submitted" class="form-control" required>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="col-form-control">
                                        <b>Trạng thái hoạt động:</b>

                                        <input type="radio" name="del_flag" id="del_flag_1" value="1">
                                        <label for="del_flag_1" style="color: black"><b>Hiện</b></label>
                                        <input type="radio" name="del_flag" id="del_flag_0" value="0">
                                        <label for="del_flag_0" style="color: black"><b>Ẩn</b></label>
                                    </label>
                                </td>
                            </tr>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                        <div>
                            <button type="submit" class="btn btn-primary" id="submit">Cập nhật</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> --}}

    {{-- <script type="text/javascript">
        $(document).ready(function() {
            //edit
            $('.btnEdit').click(function() {
                let url = $(this).attr('data-url');
                $.ajax({
                    url: url,
                    method: "GET",
                    success: function(response) {
                        console.log(response.introduce)

                        $('#company_edit').val(response.introduce.company)
                        $('#hidden_avatar').val(response.introduce.avatar)
                        $('#content').val(response.introduce.content)
                        $('#address').val(response.introduce.address)
                        $('#date_submitted').val(response.introduce.date_submitted)

                        if (response.introduce.del_flag == $('#del_flag_1').val()) {
                            $('#del_flag_1').prop('checked',true);
                        } else {
                            $('#del_flag_0').prop('checked',true);
                        }
                    }
                });
            });
            //update
            $('#form_edit_intro').submit(function(e) {
                e.preventDefault();
                let url = $(this).attr('data-url');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "PUT",
                    data: {
                        company: $('#company_edit').val(),
                        avatar: $('#avatar').val(),
                        hidden_avatar: $('#hidden_avatar').val(),
                        content: $('#content').val(),
                        address: $('#address').val(),
                        date_submitted: $('#date_submitted').val(),
                        del_flag: $("[name=del_flag]:checked").val(),
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.code == 200) {
                            alert(response.message);
                        }
                    }
                });
            });
        });
    </script> --}}
@endsection
