@extends('layouts.admin')

@section('content')
    <style>
        h3 {
            padding: 0;
            margin: 0;
        }

        .alert.alert-danger {
            height: 111px;
        }

    </style>
    <a href="{{ route('maintenances.index') }}" class="btn btn-primary">Quay lại</a>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('maintenances.store') }}" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <h2>Thêm bảo trì</h2>
                <b style="color:red"><span>*</span> Lưu ý: Lên lịch bảo trì phải đảm bảo không có sân nào được đặt </b>
                <tr>
                    <td>
                        <div class="col-md-5">
                            <select name="pitch_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($listPitch as $data)
                                    <option value="{{ $data->id }}">
                                        {{ $data->pitch_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Nội dung bảo trì</b>
                        <textarea name="description" class="form-control" rows="5" required></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Thời gian bắt đầu</b>
                        <input type="datetime-local" min="{{ date('Y-m-d\TH:i', strtotime($today)) }}"
                            name="time_start" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Dự kiến hoàn thành</b>
                        <input type="datetime-local" min="{{ date('Y-m-d\TH:i', strtotime($today)) }}"
                            name="time_end" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Thêm</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
