@extends('layouts.admin')

@section('content')
    <style>
        h3 {
            padding: 0;
            margin: 0;
        }

        .alert.alert-danger {
            height: 70px;
        }

    </style>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="{{ route('maintenances.index') }}" class="btn btn-primary">Quay lại</a>
    <h2>Sửa bảo trì</h2>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('maintenances.update', $maintenances->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>
                        <div class="col-md-5">
                            <select name="pitch_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($listPitch as $data)
                                    <option value="{{ $data->id }}" @if ($data->id == $maintenances->pitch_id) selected @endif>
                                        {{ $data->pitch_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Nội dung bảo trì</b>
                        <textarea name="description" value={{ $maintenances->description }} class="form-control"
                            rows="5" required>{{ $maintenances->description }}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Thời gian bắt đầu</b>
                        <input type="datetime-local" min="{{ date('Y-m-d\TH:i', strtotime($today)) }}"
                            name="time_start" class="form-control"
                            value="{{ date('Y-m-d\TH:i', strtotime($maintenances->time_start)) }}" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Dự kiến hoàn thành</b>
                        <input type="datetime-local" min="{{ date('Y-m-d\TH:i', strtotime($today)) }}"
                            name="time_end" class="form-control"
                            value="{{ date('Y-m-d\TH:i', strtotime($maintenances->time_end)) }}" required>
                    </td>
                </tr>
                {{-- <tr>
                    <td>
                        <b>Trạng thái </b>
                        <input type="radio" name="del_flag" id="1" value="1"
                            @if ($maintenances->del_flag == 1) checked @endif><label for="1" style="color: black"><b>Hoạt
                                động</b></label>
                        <input type="radio" name="del_flag" id="0" value="0"
                            @if ($maintenances->del_flag == 0) checked @endif><label for="0" style="color: black"><b>Ngừng
                                hoạt động</b></label>
                    </td>
                </tr> --}}
                <tr>
                    <td>
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
