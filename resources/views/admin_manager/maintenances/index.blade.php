@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Danh sách thời gian</h1>
    <form action="" method="get">
        <div class="col-md-3 col-sm-2">
            <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($del_flag == 1) selected @endif>Đang bảo trì</option>
                <option value="0" @if ($del_flag == 0) selected @endif>Đã bảo trì</option>
            </select>
        </div>
        <div class="col-md-5 col-sm-2">
            <button class="btn btn-info">Chọn</button>
        </div>
    </form>
    <a href="{{ route('maintenances.create') }}" class="btn btn-primary"><i class="material-icons">more_time</i>
        Bảo trì sân
    </a>
    <div class="material-datatables">
        @if ($listMaintenances->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Sân bảo trì</th>
                            <th>Nội dung bảo trì</th>
                            <th>Thời gian bắt đầu</th>
                            <th>Dự kiến hoàn thành</th>
                            <th>Trạng thái</th>
                            @foreach ($listMaintenances as $data)
                                @if ($data->del_flag == 1)
                                    <th>Sửa</th>
                                    <th>Đã hoàn thành</th>
                                @endif
                            @endforeach
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Sân bảo trì</th>
                            <th>Nội dung bảo trì</th>
                            <th>Thời gian bắt đầu</th>
                            <th>Dự kiến hoàn thành</th>
                            <th>Trạng thái</th>
                            @foreach ($listMaintenances as $data)
                                @if ($data->del_flag == 1)
                                    <th>Sửa</th>
                                    <th>Đã hoàn thành</th>
                                @endif
                            @endforeach
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listMaintenances as $data)
                            <?php $i++; ?>
                            <tr>
                                <th>{{ $i }}</th>
                                <td>{{ $data->pitch_name }}</td>
                                <td>{{ $data->description }}</td>
                                <td>{{ $data->time_start }}</td>
                                <td>{{ $data->time_end }}</td>
                                <td>
                                    {{ $data->del_flag == 1 ? 'Đang bảo trì' : 'Đã bảo trì' }}
                                </td>
                                @if ($data->del_flag == 1)
                                    <td><a href="{{ route('maintenances.edit', $data->id) }}"
                                            class="btn btn-success">Sửa</a>
                                    </td>
                                    <td>
                                        <form action="{{ route('maintenances.destroy', $data->id) }}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-warning">
                                                Đã hoàn thành
                                            </button>
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
