@extends('layouts.admin')

@section('content')
    <style>
        p {
            font-size: 20px;
        }
    </style>
    <div class="container">
        <div class="material-datatables">
            @if ($message = Session::get('success'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-success">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            <div>
                <h2 style="text-align:left">Thanh Toán Hoá Đơn</h2>
            </div>
            <div class="row">
                <div class="col-md-7">
                    {{-- vãng lai --}}
                    @if ($bill_data->user_id == '')
                        {{-- đơn đặt thường --}}
                        @if ($bill_data->bill_periodic_id == '')
                            <p>Mã hoá đơn: <b>BKS{{ $bill_data->id }}</b></p>
                        @else
                            {{-- đơn định kì --}}
                            <p>Mã hoá đơn: <b>BKSĐK{{ $bill_periodic->id }}</b></p>
                        @endif
                        <p>Tên: <b>{{ $customer_name }}</b></p>
                        <p>Số điện thoại: <b>{{ $customer_phone }}</b></p>
                    @else
                        {{-- có tài khoản --}}
                        {{-- đơn đặt thường --}}
                        @if ($bill_data->bill_periodic_id == '')
                            <p>Mã hoá đơn: <b>BKS{{ $bill_data->id }}</b></p>
                        @else
                            {{-- đơn định kì --}}
                            <p>Mã hoá đơn: <b>BKSĐK{{ $bill_periodic->id }}</b></p>
                        @endif
                        <p>Tên: <b>{{ $customer->name }}</b></p>
                    @endif
                    <p>Ngày đặt: {{ $booking_date }}</p>
                    {{-- đặt thường --}}
                    @if ($bill_data->bill_periodic_id == '')
                        <p>Tổng tiền: <b>{{ number_format($total_price) }} VNĐ</b></p>
                        <p>Số tiền cần thanh toán trước: <b>{{ number_format($deposit) }} VNĐ</b></p>
                        <p>
                            Số tiền còn lại cần thanh toán sau khi hoàn thành trận đấu:
                            <b>{{ number_format($bill_data->total_price - $bill_data->deposit) }} VNĐ</b>
                        </p>
                        {{-- tra tiền mặt --}}
                        @if ($bill_data->payment_type == 0) 
                            <h4>
                                Khách hàng chọn hình thức thanh toán: <br> <b>{{ $bill_data->PaymentTypeName }}</b>
                                <br>
                                Vì vậy, cần thông báo cho khách hàng phải đến khu vực sân để thanh toán tiền đặt cọc trước
                                <b style="color: red">{{ $maxTimePayment }}</b>
                                để hoá đơn được xử lý thành công
                                <br>
                                <b style="color:red">* Lưu ý: Sau thời gian trên, khách hàng chưa thanh toán số tiền cần đặt
                                    cọc thì hoá đơn đặt sân sẽ bị huỷ</b>
                            </h4>
                        @else
                        {{-- tiền online --}}
                            <h4>
                                Khách hàng chọn hình thức thanh toán: <b>{{ $bill_data->PaymentTypeName }}</b>
                                <br>
                                Vì vậy, quý khách hãy lựa chọn thanh toán bằng tài khoản Momo hoặc Ngân hàng theo thông tin
                                <b>phía dưới</b> để thanh toán tiền đặt cọc trước <b
                                    style="color: red">{{ $maxTimePayment }}</b>
                                để hoá đơn của quý khách được xử lý thành công
                                <br>
                                <b style="color:red">* Lưu ý: Sau thời gian trên, quý khách chưa thanh toán số tiền cần đặt
                                    cọc thì hoá đơn đặt sân sẽ bị huỷ</b>
                            </h4>
                        @endif
                    @else
                    {{-- đặt định kì --}}
                        <p>Số tiền cần thanh toán trước: <b>{{ number_format($total_price) }} VNĐ</b></p>
                        <h4>Số tiền cần trả với mỗi ngày đặt sân: <b>{{ number_format($bill_data->total_price) }} VNĐ</b>
                        </h4>
                        <h4>Tổng tiền cần thanh toán cho thời hạn đặt định kỳ <b style="color:red">{{$count_date_periodic}} ngày</b>: <b>{{ number_format($total_price_count) }} VNĐ</b></h4>
                        <h4>
                            <span style="color:red">*Lưu ý: khách hàng vi phạm quy định khi không thanh toán đủ, đúng hẹn,
                                chúng tôi sẽ tịch thu số tiền đặt cọc <b>{{ number_format($total_price) }} VNĐ</b> </span>
                        </h4>
                        {{-- trả tiền mặt --}}
                        @if ($bill_periodic->payment_type == 0)
                            <h4>
                                Khách hàng chọn hình thức thanh toán: <br> <b>{{ $bill_periodic->PaymentTypeName }}</b>
                                <br>
                                Vì vậy, cần thông báo cho khách hàng phải đến khu vực sân để thanh toán tiền đặt cọc trước
                                <b style="color: red">{{ $maxTimePayment }}</b>
                                để hoá đơn được xử lý thành công
                                <br>
                                <b style="color:red">* Lưu ý: Sau thời gian trên, khách hàng chưa thanh toán số tiền cần đặt
                                    cọc thì hoá đơn đặt sân sẽ bị huỷ</b>
                            </h4>
                        @else
                        {{-- tiền online --}}
                            <h4>
                                Khách hàng chọn hình thức thanh toán: <b>{{ $bill_periodic->PaymentTypeName }}</b>
                                <br>
                                Vì vậy, quý khách hãy lựa chọn thanh toán bằng tài khoản Momo hoặc Ngân hàng theo thông tin
                                <b>phía dưới</b> để thanh toán tiền đặt cọc trước <b
                                    style="color: red">{{ $maxTimePayment }}</b>
                                để hoá đơn của quý khách được xử lý thành công
                                <br>
                                <b style="color:red">* Lưu ý: Sau thời gian trên, quý khách chưa thanh toán số tiền cần đặt
                                    cọc thì hoá đơn đặt sân sẽ bị huỷ</b>
                            </h4>
                        @endif
                    @endif
                </div>
                <div class="col-md-5">
                    <a class="btn" href="{{ route('bill.index') }}">Chuyển đến hoá đơn để duyệt</a>
                </div>
            </div>
            @if ($bill_data->bill_periodic_id == '')
                <div class="row">
                    <div class="col-md-10">
                        <h3 style="text-align:center">Hình thức thanh toán</h3>
                        <div class="row">
                            <div class="col-md-8">
                                <b style="font-size: 18px">1. Thanh toán qua Ví điện tử Momo</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản nhận: <b>0862229084</b> hoặc quét mã QR và thanh
                                    toán số tiền<br>kèm theo lời nhắn
                                    <b>{{ $bill_data->id }}</b>
                                </p>
                                <p style="font-size: 18px; color:red"><span style="color:red">*</span>Lưu ý: khi thanh toán
                                    <b>cần nhập đúng chữ số của lời nhắn</b> để hoá đơn của bạn được xử lý chính xác và
                                    nhanh chóng
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('/images/qrmomo.jpg') }}" width="200px" height="200px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b style="font-size: 18px">2. Thanh toán qua tài khoản ngân hàng</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản: <b>06922267101</b></p>
                                <p style="font-size: 18px">Ngân hàng: <b>TpBank</b></p>
                                <p style="font-size: 18px"><span style="color:red">*</span>Quý khách chuyển khoản kèm lời
                                    nhắn <b>{{ $bill_data->id }}</b></p>
                                <p style="font-size: 18px; color:red"><span style="color:red">*</span>Lưu ý: khi thanh toán
                                    <b>cần nhập đúng chữ số của lời nhắn</b> để hoá đơn của bạn được xử lý chính xác và
                                    nhanh chóng
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-10">
                        <h3 style="text-align:center">Hình thức thanh toán</h3>
                        <div class="row">
                            <div class="col-md-8">
                                <b style="font-size: 18px">1. Thanh toán qua Ví điện tử Momo</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản nhận: <b>0862229084</b> hoặc quét mã QR và thanh
                                    toán số tiền<br>kèm theo lời nhắn
                                    <b>{{ $bill_periodic->id }}</b>
                                </p>
                                <p style="font-size: 18px; color:red"><span style="color:red">*</span>Lưu ý: khi thanh toán
                                    <b>cần nhập đúng chữ số của lời nhắn</b> để hoá đơn của bạn được xử lý chính xác và
                                    nhanh chóng
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('/images/qrmomo.jpg') }}" width="200px" height="200px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b style="font-size: 18px">2. Thanh toán qua tài khoản ngân hàng</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản: <b>06922267101</b></p>
                                <p style="font-size: 18px">Ngân hàng: <b>TpBank</b></p>
                                <p style="font-size: 18px"><span style="color:red">*</span>Quý khách chuyển khoản kèm lời
                                    nhắn <b>{{ $bill_periodic->id }}</b></p>
                                <p style="font-size: 18px; color:red"><span style="color:red">*</span>Lưu ý: khi thanh toán
                                    <b>cần nhập đúng chữ số của lời nhắn</b> để hoá đơn của bạn được xử lý chính xác và
                                    nhanh chóng
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    </div>
@endsection
