@extends('layouts.admin')

@section('content')
    <style>
        .bootstrap-select>.dropdown-toggle {
            background: #0400f7 !important;
            color: white !important;
        }
    </style>
    <div class="section">
        <div class="container-fluid">
            <a href="{{ route('order-admin') }}" class="btn btn-primary">Quay lại</a>
            <div class="row">
                <div class="col">
                    <h2 class="section-title" align="center">Xin mời bạn cung cấp đầy đủ thông tin</h2>
                    {{-- validate --}}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if ($message = Session::get('error'))
                        <div class="section cd-section section-notifications" id="notifications">
                            <div class="alert alert-danger">
                                <div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                    </button>
                                    <h3>{{ $message }}</h3>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card">
                        <form method="post" action="{{ route('order-admin-process') }}" class="form-horizontal">
                            @csrf
                            <div class="card-content">
                                <div class="row" id="email">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Email </b><b
                                            style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="form-group label-floating is-empty">
                                            <select name="customer_id" class="form-control">
                                                @foreach ($customer as $data)
                                                    <option value="{{ $data->id }}" id="email_val">
                                                        {{ $data->email }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="checkbox">
                                        <label>
                                            <input id="checkbox_no_account" type="checkbox">
                                            Khách hàng chưa có tài khoản
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="no_account" class="col-sm-12">
                                        <div class="row">
                                            <input type="hidden" id="check" name="check_account">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">Tên </b><b
                                                    style="color: red">*</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="customer_name"
                                                    placeholder="Nhập tên khách hàng...">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">Số điện thoại
                                                </b><b style="color: red">*</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="customer_phone"
                                                    placeholder="Nhập số điện thoại khách hàng...">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Khu vực</b></label>
                                    <div class="col-sm-9">
                                        <div class="form-group label-floating is-empty">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <select name="area_id" class="form-control"
                                                    data-style="btn btn-primary btn-round">
                                                    <option value="{{ $area->id }}">
                                                        {{ $area->area_name }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">Sân</b></label>
                                        <div class="col-sm-9">
                                            <div class="form-group label-floating is-empty">
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <select name="pitch_id" class="form-control"
                                                        data-style="btn btn-primary btn-round">
                                                        <option value="{{ $pitch->id }}">
                                                            {{ $pitch->pitch_name }}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">Ngày</b><b
                                                style="color: red">*</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    @if ($weekday == null)
                                                        <input id="day" type="date" name="day"
                                                            value="{{ $date }}" min="{{ $date }}"
                                                            class="form-control datepicke" readonly>
                                                    @else
                                                        <input type="hidden" value="{{ $weekday }}"
                                                            name="weekday_periodic">
                                                        <input type="hidden" value="{{ $first_date }}"
                                                            name="first_date_periodic">
                                                        <input type="hidden" value="{{ $second_date }}"
                                                            name="second_date_periodic">
                                                        <textarea
                                                            style="color: red; 
                                                            font-weight:bold; 
                                                            overflow-y: scroll;
                                                            height: 100px;
                                                            resize: none;"
                                                            class="form-control rounded-0" rows="5" readonly>
                                                            {{ $date }}
                                                        </textarea>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">Thời gian bắt đầu
                                            </b><b style="color: red">*</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input id="time_start" type="time" name="time_start" readonly
                                                        value="{{ $time_start }}" class="form-control datepicke">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">Thời gian kết
                                                thúc</b><b style="color: red">*</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input id="time_end" type="time" name="time_end" readonly
                                                        value="{{ $time_end }}" class="form-control datepicke">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Thời lượng (phút)</b><b style="color: red">*</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input id="time_end" type="text" value="{{ $minute }}"
                                                        class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if ($weekday)
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Số tiền cần trả từng ngày đặt (VNĐ)</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input
                                                        value="{{ number_format($show_final_price, 0, '', ',') . 'đ' }}"
                                                        style="font-size:20px; color:red" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Số tiền bạn cần đặt cọc khi đặt định kì (VNĐ)</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input type="text"
                                                        value="{{ number_format(1000000, 0, '', ',') . 'đ' }}"
                                                        name="show_final_price_periodic" style="font-size:20px; color:red"
                                                        class="form-control" readonly required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Tổng tiền cần thanh toán cho thời hạn đặt định kỳ <span
                                                    style="color:red">{{ $count_date_periodic }} ngày</span>
                                                (VNĐ)</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input type="text"
                                                        value="{{ number_format($total_price, 0, '', ',') . 'đ' }}"
                                                        name="show_final_price_periodic" style="font-size:20px; color:red"
                                                        class="form-control" readonly required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if (!$weekday)
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Thành tiền</b>
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input type="text"
                                                        value="{{ number_format($show_final_price, 0, '', ',') . 'đ' }}"
                                                        style="font-size:20px; color:red" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">
                                            <span style="color:red">*</span>Hình thức thanh toán tiền đặt cọc</b></label>
                                    <div class="col-sm-5">
                                        <div class="form-group label-floating is-empty">
                                            <select name="payment_type" class="form-control" required>
                                                <option value="" selected disabled>Chọn hình thức thanh toán</option>
                                                <option value="0">Trả tiền mặt</option>
                                                <option value="1">Thanh toán trực tuyến (Momo, Ngân hàng)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" onclick="return confirm('Bạn có chắc chắn đặt đơn này?')"
                                            class="btn btn-primary">Đặt</button>
                                    </div>
                                    <div class="col-md-2">

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
