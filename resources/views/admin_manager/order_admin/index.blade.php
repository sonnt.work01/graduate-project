@extends('layouts.admin')

@section('content')
    <style>
        .orderCSS {
            width: 100%;
            height: 135px;
            background-image: url('{{ asset('images/order.png') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .emptyCSS {
            width: 100%;
            height: 135px;
            background-image: url('{{ asset('images/empty.jpg') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .init {
            width: 100%;
            height: 135px;
            background-image: url('{{ asset('images/empty.jpg') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .card .tab-content {
            margin-top: 0px !important;
        }

        #error {
            font-size: 18px;
            background-color: #D8000C;
            color: white;
            padding: 10px;
            font-weight: bold;
        }

        #loader {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 999;
            /* or higher if necessary */
            top: 0;
            left: 0;
            text-indent: 100%;
            font-size: 0;
            background: url('{{ asset('loader/image_processing20210909-1419-19deel7.gif') }}') center no-repeat;
        }

        .bootstrap-select>.dropdown-toggle {
            width: 100% !important;
        }

        .bootstrap-select>.dropdown-toggle {
            background: #ff7600 !important;
            color: white !important;
            font-size: 15px;
            padding: 10px;
        }

        .col-sm-4 {
            padding-right: 5px !important;
            padding-left: 5px !important;
        }
    </style>
    <div id="loader" style="background-color: white;"></div>

    <div class="section">
        <div class="content">
            <div class="container-fluid">
                {{-- validate --}}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><span style="font-size: 19px">{{ $error }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        @if ($message = Session::get('success'))
                            <div class="section cd-section section-notifications" id="notifications">
                                <div class="alert alert-success">
                                    <div>
                                        <div class="alert-icon">
                                            <i class="material-icons">check</i>
                                        </div>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                        </button>
                                        <h3>{{ $message }}</h3>
                                    </div>
                                </div>
                            </div>
                        @elseif ($message = Session::get('error'))
                            <div class="section cd-section section-notifications" id="notifications">
                                <div class="alert alert-danger">
                                    <div>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                        </button>
                                        <h3>{{ $message }}</h3>
                                    </div>
                                </div>
                            </div>
                        @endif
                        {{-- <div class="row"> --}}
                        <h2 class="section-title">Khung giờ đã đặt</h2>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>
                                        Sân
                                    </th>
                                    <th>
                                        Khung giờ đã đặt
                                    </th>
                                </tr>
                                @foreach ($listPitchTime as $pitch)
                                    <tr>
                                        <th>
                                            {{ $pitch->pitch_name }}
                                        </th>
                                        @foreach ($listTime as $time)
                                            @if ($pitch->id == $time->id)
                                                <td>
                                                    {{ date('H:i:s', strtotime($time->time_start)) . ' - ' . date('H:i:s', strtotime($time->time_end)) }}
                                                </td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{-- div pitch --}}
                        <div class="row">
                            <div class="col-12">
                                <b style="font-size: 18px; padding-left:32px; margin: 0">
                                    <span style="color:red">*</span> Xin mời quý khách lựa chọn khung giờ để tìm cho mình
                                    sân bóng phù hợp
                                </b>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <h2 class="section-title" style="margin: 0">Sơ đồ sân Bách Khoa</h2>
                            <div style="width:100%; height:50px; background-color: #c2c2c2; border-radius:5px">
                                <div style="text-align:center">
                                    <h5><b>Khán đài B</b></h5>
                                </div>
                            </div>
                            <br>
                            <fieldset>
                                @foreach ($listPitch as $pitch)
                                    @if ($pitch->category_id == 2)
                                        <legend>Sân 11
                                            <div id="form_order11" class="form_order">
                                                <form action="/order_admin_form/{{ $pitch->area_id }}/{{ $pitch->id }}"
                                                    method="post">
                                                    @csrf
                                                    <input class="date_hidden" type="hidden" value="{{ $search }}"
                                                        name="date">
                                                    <input class="time_start_hidden" type="hidden"
                                                        value="{{ $start }}" name="time_start">
                                                    <input class="time_end_hidden" type="hidden"
                                                        value="{{ $end }}" name="time_end">
                                                    <input class="date_periodic_end" type="hidden"
                                                        name="date_periodic_end">
                                                    <input class="weekday_periodic" type="hidden" name="weekday_periodic">
                                                    <input class="date_periodic_start" type="hidden"
                                                        name="date_periodic_start">
                                                    <button class="btn"
                                                        style="background-color: #fffb07; color:black; font-weight:bold">
                                                        Đặt sân 11
                                                    </button>
                                                </form>
                                            </div>
                                        </legend>
                                    @endif
                                    @if ($pitch->category_id == 1)
                                        <div class="col-sm-6 cart-pitch">
                                            <div class="card card_pitch init" id="{{ $pitch->id }}">
                                                <div class="card-body">
                                                    <h5 class="card-title"
                                                        style="color: rgb(255, 217, 0); text-align: center; margin-top: 0">
                                                        {{ $pitch->pitch_name }}
                                                    </h5>
                                                    <div id="form_order{{ $pitch->id }}" class="form_order">
                                                        <form
                                                            action="/order_admin_form/{{ $pitch->area_id }}/{{ $pitch->id }}"method="post">
                                                            @csrf
                                                            <input class="date_hidden" type="hidden" name="date">
                                                            <input class="time_start_hidden" type="hidden"
                                                                name="time_start">
                                                            <input class="time_end_hidden" type="hidden" name="time_end">
                                                            <input class="date_periodic_end" type="hidden"
                                                                name="date_periodic_end">
                                                            <input class="weekday_periodic" type="hidden"
                                                                name="weekday_periodic">
                                                            <input class="date_periodic_start" type="hidden"
                                                                name="date_periodic_start">
                                                            <button class="btn"
                                                                style="background-color: #fffb07; color:black; font-weight:bold">
                                                                Đặt sân
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </fieldset>
                            {{-- </div> --}}
                            <div class="row">
                                <div class="col-sm-3">
                                    <h5 style="text-align:center"><i>Cổng vào</i> <i
                                            class="fa-solid fa-circle-arrow-up-right"></i>
                                    </h5>
                                </div>
                                <div class="col-sm-6">
                                    <div style="width:100%; height:50px; background-color: #c2c2c2; border-radius:5px">
                                        <div style="text-align:center">
                                            <h5><b>Khán đài A</b></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <h5 style="text-align:center"><i class="fa-solid fa-circle-up-left"></i> <i>Cổng
                                            vào</i>
                                    </h5>
                                </div>
                            </div>
                            <div class="row"
                                style="border-top: 2px solid black; border-bottom: 2px solid black; margin-top: 5px;">
                                <h5><b>Đường Lê Thanh Nghị</b></h5>
                            </div>
                        </div>
                        {{-- div time --}}
                        <br>
                        <br>
                        <div class="col-sm-4" style="border-style: solid">
                            <h2 class="section-title">Chọn khung giờ</h2>
                            <div class="row">
                                <p style="margin: 0 10px 0 16px; font-weight: bold">
                                    Giờ hoạt động: <span style="color:red;"> 5h30 sáng </span> đến <span
                                        style="color:red;"> 23h30 </span> hàng ngày
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 id="error"></h5>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <ul class="nav nav-pills nav-pills-warning">
                                    <li class="active">
                                        <a href="#pill1" data-toggle="tab">Đặt thường</a>
                                    </li>
                                    <li><a href="#pill2" data-toggle="tab">Đặt định kì</a></li>
                                </ul>
                                <div class="tab-content tab-space">
                                    <div class="tab-pane active" id="pill1">
                                        <form id="form_filter" data-url="{{ route('pitch-filter-admin') }}"
                                            method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <b style="color: black">Chọn ngày</b><b style="color: red">*</b>
                                                    <div class="form-group label-floating is-empty">
                                                        <input id="date" type="date" name="search"
                                                            value="{{ $search }}" min="{{ $today }}"
                                                            style="padding: 5px">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <b style="color: black">Chọn khung giờ</b><b style="color: red">*</b>
                                                    <div class="form-group label-floating is-empty">
                                                        <select id="time_order_normal" class="selectpicker">
                                                            @foreach ($arr_time as $item)
                                                                <option
                                                                    value="{{ $item['time_start'] }}-{{ $item['time_end'] }}">
                                                                    {{ $item['time_start'] }} - {{ $item['time_end'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-info btn_filter">Chọn</button>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="pill2">
                                        <p style="color:red; font-size:18px"><span>* Lưu ý: chọn ngày kết thúc phải tối
                                                thiểu đủ 1 tháng tính từ ngày bắt đầu</span></p>
                                        <form id="form_filter_periodic"
                                            data-url="{{ route('pitch-filter-periodic-admin') }}" method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <b style="color: black">Chọn thứ trong tuần </b>
                                                    <select name="" id="weekday" class="selectpicker">
                                                        <option value="Monday">Thứ 2</option>
                                                        <option value="Tuesday">Thứ 3</option>
                                                        <option value="Wednesday">Thứ 4</option>
                                                        <option value="Thursday">Thứ 5</option>
                                                        <option value="Friday">Thứ 6</option>
                                                        <option value="Saturday">Thứ 7</option>
                                                        <option value="Sunday">Chủ nhật</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <b class="col-sm-6 label-on-left" style="color: black">Ngày bắt đầu<b
                                                        style="color: red">*</b></b>
                                                <div class="col-sm-6">
                                                    <input id="date_start_periodic" type="date"
                                                        min="{{ $today }}" name="date_start_periodic"
                                                        value="{{ $today }}" class="form-control datepicke"
                                                        required>
                                                </div>
                                                <b class="col-sm-6 label-on-left" style="color: black">Ngày kết thúc</b>
                                                <div class="col-sm-6">
                                                    <input id="date_end_periodic" type="date" name="date_end_periodic"
                                                        min="{{ $minDate1Month }}" value="{{ $minDate1Month }}"
                                                        class="form-control datepicke" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <b style="color: black">Chọn khung giờ</b><b style="color: red">*</b>
                                                    <div class="form-group label-floating is-empty">
                                                        <select id="time_order_periodic" class="selectpicker">
                                                            @foreach ($arr_time as $item)
                                                                <option
                                                                    value="{{ $item['time_start'] }}-{{ $item['time_end'] }}">
                                                                    {{ $item['time_start'] }} - {{ $item['time_end'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-info btn_filter">Chọn</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h6><span style="color: red">*</span><i>Chú thích:</i></h6>
                            <div class="col-sm-6">
                                <div class="card"
                                    style="width: 100%; height: 80px;
                                                        color: white;
                                                        background-image: url('{{ asset('images/empty.jpg') }}');
                                                        background-position: center;
                                                        background-repeat: no-repeat;
                                                        background-size: cover;">
                                    <div class="card-body">
                                        <p style="text-align:center; padding-top:19px"><b>Còn trống</b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card"
                                    style="width: 100%; height: 80px;
                                                        color: white;
                                                        background-image: url('{{ asset('images/order.png') }}');
                                                        background-position: center;
                                                        background-repeat: no-repeat;
                                                        background-size: cover;">
                                    <div class="card-body">
                                        <p style="text-align:center; padding-top:19px"><b>Đã có người đặt</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- đặt thường --}}
        <script type="text/javascript">
            $(document).ready(function() {
                $('#error').hide();
                $('.form_order').hide();
                $('#loader').hide();

                $('#form_filter').submit(function(event) {
                    event.preventDefault();
                    let url = $(this).attr('data-url');
                    if ($('#time_start').val() == null && $('#time_end').val()) {
                        $('.form_order').hide().attr('disabled', true);
                    }

                    $('#loader').fadeIn();
                    setTimeout(function() {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: url,
                            method: "post",
                            data: {
                                date: $('#date').val(),
                                time_order_normal: $('#time_order_normal').val(),
                            },
                            success: function(response) {
                                $('#loader').fadeOut();
                                //min 60 minute
                                if (response.hasError) {
                                    $('#error').show();
                                    $('#error').append(response.message);
                                    $('.form_order').hide();
                                    setTimeout(function() {
                                        $('#error').empty();
                                        $('#error').hide();
                                    }, 4000);
                                }

                                if (!response.hasError) {
                                    $('.date_periodic_end').val("");
                                    $('.weekday_periodic').val("");
                                    $('.date_periodic_start').val("");

                                    $('.date_hidden').val(response.date);
                                    $('.time_start_hidden').val(response.start);
                                    $('.time_end_hidden').val(response.end);

                                    let arr_pitch = response.listPitch;
                                    let arr_order = response.array_pitch;
                                    let count = 0;
                                    let arr = [];
                                    console.log(arr_pitch);
                                    console.log(arr_order);
                                    for (let i = 0; i < arr_pitch.length; i++) {
                                        for (let j = 0; j < arr_order.length; j++) {
                                            if (arr_pitch[i].id === arr_order[j]) {
                                                arr.push(arr_pitch[i].id);
                                            }
                                        }
                                    }
                                    console.log(arr);
                                    if (arr.length != 0) {
                                        $('#form_order11').hide();
                                    } else {
                                        $('#form_order11').show();
                                    }
                                    for (let i = 0; i < arr_pitch.length; i++) {
                                        if (jQuery.inArray(arr_pitch[i].id, arr) === -1) {
                                            $('#' + arr_pitch[i].id).removeClass('init');
                                            $('#' + arr_pitch[i].id).removeClass(
                                                'orderCSS');
                                            $('#' + arr_pitch[i].id).addClass('emptyCSS');
                                            $('#form_order' + arr_pitch[i].id).show();
                                        } else {
                                            $('#' + arr_pitch[i].id).removeClass('init');
                                            $('#' + arr_pitch[i].id).removeClass(
                                                'emptyCSS');
                                            $('#' + arr_pitch[i].id).addClass('orderCSS');
                                            $('#form_order' + arr_pitch[i].id).hide();
                                        }
                                    }
                                }
                            },
                        });
                    }, 1000);
                });
            });
        </script>

        {{-- Đặt định kỳ --}}
        <script type="text/javascript">
            $(document).ready(function() {
                $('#error').hide();
                $('.form_order').hide();
                $('#loader').hide();
                $('#form_filter_periodic').submit(function(event) {
                    event.preventDefault();
                    let url = $(this).attr('data-url');
                    if ($('#time_start_periodic').val() == null && $('#time_end_periodic').val()) {
                        $('.form_filter_periodic').hide().attr('disabled', true);
                    }
                    $('#loader').fadeIn();
                    setTimeout(function() {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: url,
                            method: "post",
                            data: {
                                weekday: $('#weekday').val(),
                                date_start_periodic: $('#date_start_periodic').val(),
                                date_end_periodic: $('#date_end_periodic').val(),
                                time_order_periodic: $('#time_order_periodic').val(),
                            },
                            success: function(response) {
                                $('#loader').fadeOut();
                                console.log(response);

                                if (response.hasError) {
                                    $('#error').show();
                                    $('#error').append(response.message);
                                    $('.form_order').hide();
                                    setTimeout(function() {
                                        $('#error').empty();
                                        $('#error').hide();
                                    }, 4000);
                                }

                                if (!response.hasError) {
                                    $('.date_hidden').val("");
                                    $('.time_start_hidden').val(response.start);
                                    $('.time_end_hidden').val(response.end);
                                    $('.date_periodic_end').val(response.second_date);
                                    $('.weekday_periodic').val(response.weekday);
                                    $('.date_periodic_start').val(response.first_date);

                                    let arr_pitch = response.listPitch;
                                    let arr_order = response.array_pitch;
                                    let count = 0;
                                    let arr = [];
                                    for (let i = 0; i < arr_pitch.length; i++) {
                                        for (let j = 0; j < arr_order.length; j++) {
                                            if (arr_pitch[i].id === arr_order[j]) {
                                                arr.push(arr_pitch[i].id);
                                            }
                                        }
                                    }
                                    if (arr.length != 0) {
                                        $('#form_order11').hide();
                                    } else {
                                        $('#form_order11').show();
                                    }
                                    for (let i = 0; i < arr_pitch.length; i++) {
                                        if (jQuery.inArray(arr_pitch[i].id, arr) === -1) {
                                            $('#' + arr_pitch[i].id).removeClass('init');
                                            $('#' + arr_pitch[i].id).removeClass(
                                                'orderCSS');
                                            $('#' + arr_pitch[i].id).addClass('emptyCSS');
                                            $('#form_order' + arr_pitch[i].id).show();
                                        } else {
                                            $('#' + arr_pitch[i].id).removeClass('init');
                                            $('#' + arr_pitch[i].id).removeClass(
                                                'emptyCSS');
                                            $('#' + arr_pitch[i].id).addClass('orderCSS');
                                            $('#form_order' + arr_pitch[i].id).hide();
                                        }
                                    }
                                }
                            },
                        });
                    }, 1000);
                });
            });
        </script>
    @endsection
