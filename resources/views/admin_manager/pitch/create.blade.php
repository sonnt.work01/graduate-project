@extends('layouts.admin')

@section('content')
    <a href="/pitch" class="btn btn-primary">Quay lại</a>
    <h1>Thêm thông tin sân</h1>
    <div class="section cd-section section-notifications">
        <p style="color: rgb(83, 83, 83); font-size:16px">
            <em>
                <b>Lưu ý:
                    <br>+ Khi chọn sân 7 là sân ghép, thì 4 sân 7 sẽ tạo thành sân 11 (sân ghép)
                    <br>+ Khi chọn sân 7 là sân đơn, thì sân này hoàn toàn độc lập với sân khác
                </b>
            </em>
        </p>
    </div>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <form action="{{ route('pitch.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tên sân
                    </th>
                    <td>
                        <input type="text" name="pitch_name" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Khu vực
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($listArea as $area)
                                    <option value="{{ $area->id }}">
                                        {{ $area->area_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        Loại sân
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="category_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($category as $data)
                                    <option value="{{ $data->id }}">
                                        {{ $data->category_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        Chọn vị trí
                    </th>
                    <td>
                        <input type="radio" name="location" value="1" id="2" checked><label for="2"><span
                                style="color: black">Sân ghép</span></label>
                        <input type="radio" name="location" value="0" id="3"><label for="3"><span style="color: black">Sân
                                đơn</span></label>
                    </td>
                </tr>
                <tr>
                    <th>
                        Chọn ảnh
                    </th>
                    <td>
                        <input type="file" name="image" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Mô tả
                    </th>
                    <td>
                        <textarea name="description" class="form-control" rows="5"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        Tình trạng
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="status_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($status as $data)
                                    <option value="{{ $data->id }}">
                                        {{ $data->status_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Thêm sân</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
