@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/pitch" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thông tin sân</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('pitch.update', $pitch->id) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tên sân
                    </th>
                    <td>
                        <input type="text" name="pitch_name" value="{{ $pitch->pitch_name }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Ảnh
                    </th>
                    <td>
                        <img src="{{ asset('images/' . $pitch->image_path) }}" class="img-thumbnail"
                            style="width: 350px; height:350px">
                        <input type="file" name="image">
                        <input type="hidden" name="hidden_image" value="{{ $pitch->image_path }}">
                    </td>
                </tr>
                <tr>
                    <th>
                        Mô tả
                    </th>
                    <td>
                        <textarea name="description" value="{{ $pitch->description }}" class="form-control" rows="5">{{ $pitch->description }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        Khu vực
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($listArea as $area)
                                    <option value="{{ $area->id }}" @if ($pitch->area_id == $area->id) selected @endif>
                                        {{ $area->area_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        Vị trí
                    </th>
                    <td>
                        <input type="radio" name="location" value="1" id="1" @if ($pitch->location == 1) checked @endif><label for="1"
                            style="color: black"><b>Sân ghép</b></label>
                        <input type="radio" name="location" value="0" id="0" @if ($pitch->location == 0) checked @endif><label for="0"
                            style="color: black"><b>Sân đơn</b></label>
                    </td>
                </tr>
                <tr>
                    <th>
                        Loại sân
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="category_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($category as $data)
                                    <option value="{{ $data->id }}" @if ($pitch->category_id == $data->id) selected @endif>
                                        {{ $data->category_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        Tình trạng
                    </th>
                    <td>
                        <div class="col-md-5">
                            <select name="status_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @foreach ($status as $data)
                                    <option value="{{ $data->id }}" @if ($pitch->status_id == $data->id) selected @endif>
                                        {{ $data->status_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        Trạng thái hoạt động
                    </th>
                    <td>
                        <input type="radio" name="del_flag" value="1" id="4" @if ($pitch->del_flag == 1) checked @endif><label for="4"
                            style="color: black"><b>Hoạt động</b></label>
                        <input type="radio" name="del_flag" value="0" id="5" @if ($pitch->del_flag == 0) checked @endif><label for="5"
                            style="color: black"><b>Ngừng hoạt động</b></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
