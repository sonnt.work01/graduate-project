@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <form action="" method="get">
            @if ($admin->role == 1)
                {{-- <div class="col-md-3 col-sm-3">
                    <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                        @foreach ($listArea as $data)
                            <option value="{{ $data->id }}" @if ($data->id == $area_id) ? selected @endif>
                                {{ $data->area_name }}</option>
                        @endforeach
                    </select>
                </div> --}}
                <div class="col-md-3">
                    <span>Trạng thái</span>
                    <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                        <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                        <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
                    </select>
                </div>
                <div class="col-md-3">
                    {{-- <input type="date" name="search" value="{{ $search }}" style="padding: 5px"> --}}
                    <button class="btn btn-info">Chọn</button>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('pitch.create') }}" class="btn btn-primary" style="align:right">
                        <i class="material-icons">add_circle</i> Thêm sân
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('status') }}" class="btn btn-primary" style="align:right">
                        <i class="material-icons">add_circle</i> Các tình trạng sân
                    </a>
                </div>
            @else
                <div class="col-md-3 col-sm-3">
                    <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                        <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                        <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-3">
                    {{-- <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
                    <button class="btn btn-info">Chọn sân</button> --}}
                    <a href="{{ route('pitch.create') }}" class="btn btn-primary">
                        <i class="material-icons">add_circle</i> Thêm sân
                    </a>
                </div>
            @endif
        </form>
    </div>
    <h1>Danh sách các sân</h1>
    <div class="material-datatables">
        @if ($listPitch->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên sân</th>
                            <th>Khu vực</th>
                            <th>Vị trí</th>
                            <th>Loại sân</th>
                            <th>Ảnh</th>
                            <th>Giá gốc</th>
                            <th>Tình trạng</th>
                            <th>Giá sân theo giờ</th>
                            <th>Trạng thái</th>
                            <th>Sửa thông tin sân</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Tên sân</th>
                            <th>Khu vực</th>
                            <th>Vị trí</th>
                            <th>Loại sân</th>
                            <th>Ảnh</th>
                            <th>Giá gốc</th>
                            <th>Tình trạng</th>
                            <th>Giá sân theo giờ</th>
                            <th>Trạng thái</th>
                            <th>Sửa thông tin sân</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listPitch as $pitch)
                            <?php $i++; ?>
                            @if ($pitch->del_flag == 1)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <th>{{ $pitch->pitch_name }}</th>
                                    <td>{{ $pitch->area_name }}</td>
                                    <td>{{ $pitch->location == 0 ? 'Sân đơn' : 'Sân ghép' }}</td>
                                    <td>{{ $pitch->category_name }}</td>
                                    <td>
                                        <img src="{{ asset('images/' . $pitch->image_path) }}"
                                            style="width:250px; height:250px;">
                                    </td>
                                    <td>{{ number_format($pitch->price, 0, '', ',') . 'đ' }}</td>
                                    <td>{{ $pitch->status_name . ' (Tăng giá ' . $pitch->price_change . '%)' }}</td>
                                    <td>{{ number_format($pitch->final_price, 0, '', ',') . 'đ' }}</td>
                                    <td>
                                        {{ $pitch->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                    </td>
                                    <td><a href="{{ route('pitch.edit', $pitch->id) }}" class="btn btn-success">Sửa</a>
                                    </td>
                                </tr>
                            @elseif($pitch->del_flag == 0)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <th>{{ $pitch->pitch_name }}</th>
                                    <td>{{ $pitch->area_name }}</td>
                                    <td>{{ $pitch->location == 0 ? 'Sân đơn' : 'Sân ghép' }}</td>
                                    <td>{{ $pitch->category_name }}</td>
                                    <td>
                                        <img src="{{ asset('images/' . $pitch->image_path) }}"
                                            style="width:250px; height:250px;">
                                    </td>
                                    <td>{{ $pitch->price }}</td>
                                    <td>{{ $pitch->status_name . ' (Tăng giá ' . $pitch->price_change . '%)' }}</td>
                                    <td>{{ $pitch->final_price }}</td>
                                    <td>
                                        {{ $pitch->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                    </td>
                                    <td><a href="{{ route('pitch.edit', $pitch->id) }}" class="btn btn-success">Sửa</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
