@extends('layouts.admin')

@section('content')
    <a href="{{ route('status') }}" class="btn btn-primary">Quay lại</a>
    <h1>Thêm tình trạng</h1>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <form action="{{ route('status_store') }}" method="post" >
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tình trạng
                    </th>
                    <td>
                        <input type="text" name="status_name" placeholder="Nhập tên tình trạng..." class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Thay đổi giá (%)
                    </th>
                    <td>
                        <input type="text" name="price_change" placeholder="Nhập % thay đổi giá..." class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Thêm</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
