@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="{{ route('status') }}" class="btn btn-primary">Quay lại</a>
    <h3>Tình trạng</h3>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('status_update', $status->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tình trạng
                    </th>
                    <td>
                        <input type="text" name="status_name" value="{{ $status->status_name }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Thay đổi giá
                    </th>
                    <td>
                        <input type="text" name="price_change" value="{{ $status->price_change }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Trạng thái hoạt động
                    </th>
                    <td>
                        <input type="radio" name="del_flag" value="1" id="4" @if ($status->del_flag == 1) checked @endif><label for="4"
                            style="color: black"><b>Hoạt động</b></label>
                        <input type="radio" name="del_flag" value="0" id="5" @if ($status->del_flag == 0) checked @endif><label for="5"
                            style="color: black"><b>Ngừng hoạt động</b></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
