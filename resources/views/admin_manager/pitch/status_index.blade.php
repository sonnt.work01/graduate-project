@extends('layouts.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Các tình trạng</h1>
    <form action="" method="get">
        @if ($admin->role == 1)
            <div class="row">
                <div class="col-md-3">
                    <span>Trạng thái</span>
                    <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                        <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                        <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('status_create') }}" class="btn btn-info" style="align:right">
                        <i class="material-icons">add_circle</i> Thêm tình trạng
                    </a>
                </div>
            </div>
        @elseif ($admin->role == 0)
            <div class="row">
                <div class="col-md-3">
                    <span>Trạng thái</span>
                    <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                        <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                        <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
                    </select>
                </div>
                <div class="col-md-3">
                    {{-- <input type="date" name="search" value="{{ $search }}" style="padding: 5px"> --}}
                    <button class="btn btn-info">Chọn</button>
                </div>
            </div>
        @endif
    </form>
    <div class="material-datatables">
        @if ($status->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tình trạng</th>
                            <th>Giá thay đổi</th>
                            <th>Trạng thái</th>
                            <th>Sửa</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Tình trạng</th>
                            <th>Giá thay đổi</th>
                            <th>Trạng thái</th>
                            <th>Sửa</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($status as $data)
                            <?php $i++; ?>
                            @if ($data->del_flag == 1)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <th>{{ $data->status_name }}</th>
                                    <td>{{ $data->price_change }}</td>
                                    <td>
                                        {{ $data->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                    </td>
                                    <td>
                                        <a href="{{ route('status_edit', $data->id) }}" class="btn btn-success">
                                            Sửa
                                        </a>
                                    </td>
                                </tr>
                            @elseif($data->del_flag == 0)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <th>{{ $data->status_name }}</th>
                                    <td>{{ $data->price_change }}</td>
                                    <td>
                                        {{ $data->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                    </td>
                                    <td>
                                        <a href="{{ route('status_edit', $data->id) }}" class="btn btn-success">
                                            Sửa
                                        </a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
