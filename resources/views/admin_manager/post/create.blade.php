@extends('layouts.admin')

@section('content')
    <a href="/post" class="btn btn-primary">Quay lại</a>
    <h1>Thêm Bài viết</h1>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tiêu đề
                    </th>
                    <td>
                        <input type="text" name="title" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Người đăng
                    </th>
                    <td>
                        <select name="admin_id" class="form-control">
                            <option value="{{ $admin->id }}">
                                {{ $admin->name }}
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        Chọn ảnh
                    </th>
                    <td>
                        <input type="file" name="image" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Nội dung
                    </th>
                    <td>
                        <textarea name="content" class="form-control" rows="10"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        Ngày đăng
                    </th>
                    <td>
                        <input type="date" name="date_submitted" class="form-control" value="{{ $today }}"
                            min="{{ $today }}" required>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Thêm bài viết</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
