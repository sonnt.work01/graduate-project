@extends('layouts.admin')

@section('content')
@if ($message = Session::get('success'))
<div class="section cd-section section-notifications" id="notifications">
    <div class="alert alert-success">
        <div>
            <div class="alert-icon">
                <i class="material-icons">check</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            <h3>{{ $message }}</h3>
        </div>
    </div>
</div>
@endif
    <h1>Danh sách các bài viết</h1>
    <form action="" method="get">
        <div class="col-md-3 col-sm-2">
        <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
            <option value="1" @if ($del_flag==1) selected @endif>Hoạt động</option>
            <option value="0" @if ($del_flag==0) selected @endif>Không hoạt động</option>
        </select>
        </div>
        <div class="col-md-5 col-sm-2">
            <button class="btn btn-info">Chọn</button>
        </div>
</form>
    <a href="{{ route('post.create') }}" class="btn btn-primary"><i class="material-icons">person_add</i>Thêm post</a>
    <div class="material-datatables">
        @if ($listPost->count() == 0)
            {{ "Không có bản ghi" }}
        @else
    <div class="table-responsive">
        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th>STT</th>
                <th>Tiêu đề</th>
                <th>Người đăng</th>
                <th>Ảnh đại diện</th>
                <th>Nội dung</th>
                <th>Ngày đăng</th>
                <th>Trạng thái</th>
                <th>Sửa thông tin</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>STT</th>
                <th>Tiêu đề</th>
                <th>Người đăng</th>
                <th>Ảnh đại diện</th>
                <th>Nội dung</th>
                <th>Ngày đăng</th>
                <th>Trạng thái</th>
                <th>Sửa thông tin</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $i = 0 ?>
            @foreach ($listPost as $post)
                        <?php $i++ ?>
                <tr>
                    <td>{{ $i }}</td>
                    <th>{{ $post->title }}</th>
                    <td>{{ $post->name }}</td>
                    <td>
                        <img src="{{ asset('images/' . $post->image_path) }}" style="width:200px; height:200px">
                    </td>
                    <td>
                        <p style="overflow: hidden;text-overflow: ellipsis;-webkit-line-clamp: 5; -webkit-box-orient: vertical; display: -webkit-box;">{{ $post->content }}</p>
                    </td>
                    <td>{{ $post->date_submitted }}</td>
                    <td>
                        {{ $post->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                    </td>
                    <td><a href="{{ route('post.edit', $post->id) }}" class="btn btn-success">Sửa</a></td>
                </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    @endif
    </div>
@endsection
