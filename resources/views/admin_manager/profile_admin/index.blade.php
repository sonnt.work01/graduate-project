@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        @if ($message = Session::get('success'))
            <div class="section cd-section section-notifications" id="notifications">
                <div class="alert alert-success">
                    <div>
                        <div class="alert-icon">
                            <i class="material-icons">check</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>
                        <h3>{{ $message }}</h3>
                    </div>
                </div>
            </div>
        @endif
        <div class="material-datatables">
            <div class="table-responsive">
                <div class="card-content">
                    <h3 class="card-title">Thông tin cá nhân</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 id="error" style="color:red; font-weight: bold"></h4>
                            <h4 id="success" style="color:rgb(49, 156, 0); font-weight: bold"></h4>
                        </div>
                    </div>
                    <form id="form_edit_profile" data-url="{{ route('edit-admin', $admin->id) }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Họ tên</label>
                                    <input type="text" id="name" value="{{ $profile_admin->name }}"
                                        class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label">Ngày sinh</label>
                                    <input type="date" id="date_birth" value="{{ $profile_admin->date_birth }}"
                                        class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Giới tính</label>
                                    <label for="1">
                                        <input type="radio" name="gender" id="1" value="1"
                                            @if ($admin->gender == 1) checked @endif> Nam
                                    </label>
                                    <label for="0">
                                        <input type="radio" name="gender" id="0" value="0"
                                            @if ($admin->gender == 0) checked @endif> Nữ
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Số điện thoại</label>
                                    <input type="text" id="phone" value="{{ $profile_admin->phone }}"
                                        class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Email</label>
                                    <input type="text" name="email" value="{{ $profile_admin->email }}"
                                        class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Địa chỉ</label>
                                    <input type="text" id="address" value="{{ $profile_admin->address }}"
                                        class="form-control" required>
                                </div>
                            </div>
                        </div>
                </div>
                <button type="submit" class="btn btn-rose pull-right">Cập nhật thông tin</button>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            //update
            $('#form_edit_profile').submit(function(e) {
                e.preventDefault();
                let url = $(this).attr('data-url');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "PUT",
                    data: {
                        name: $('#name').val(),
                        date_birth: $('#date_birth').val(),
                        phone: $('#phone').val(),
                        address: $('#address').val(),
                        gender: $("[name=gender]:checked").val(),
                    },
                    success: function(response) {
                        console.log(response);
                        if (!response.hasError) {
                            $('#success').append(response.message);
                            setTimeout(function() {
                                $('#success').empty();
                            }, 3000);
                        }
                    },
                    error: function(response) {
                        $.each(response.responseJSON.errors, function(key, value) {
                            console.log(key + ": " + value);
                            $('#error').append(value + '<br>');
                            setTimeout(function() {
                                $('#error').empty();
                            }, 4000);
                        });
                    }
                });
            });
        });
    </script>
@endsection
