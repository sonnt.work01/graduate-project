@extends('layouts.contact_us')

@section('content')
    <div class="profile-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="profile">
                        <div class="avatar">
                            <img src="{{ asset('images/' . $introduce->avatar) }}"
                                style="width:150px; height:150px; border-radius:50%">
                        </div>
                        <div class="name">
                            <h3 class="title">Công ty {{ $introduce->company }}</h3>
                            <h6>Company</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="description text-center">
                <p style="color: black; font-size: 20px;">
                    {{ $introduce->content }}
                </p>
                <p style="color: black; font-size: 20px;">
                    <b>Địa chỉ: </b>{{ $introduce->address }}
                </p>
            </div>
        </div>
    </div>
@endsection
