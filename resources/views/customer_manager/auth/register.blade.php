<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/football-icon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>BKSport Đăng ký</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="{{ asset('assets') }}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('assets') }}/css/material-kit.css?v=1.2.1" rel="stylesheet" />
</head>

<body class="signup-page">
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Bách Khoa Sport</a>
            </div>
        </div>
    </nav>


    <div class="page-header header-filter"
        style="background-image: url('{{ asset('assets') }}/img/signup.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="card card-signup">
                        <h2 class="card-title text-center">Đăng ký tài khoản</h2>
                        <div class="row">
                            <div class="col-md-9 col-md-offset-2">
                                {{-- form --}}
                                <form class="form" method="post" action="{{ route('register') }}">
                                    @csrf
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="card-content">
                                        <div class="form-group">
                                            <i class="material-icons">face</i>
                                            <label class="label-control">Tên đầy đủ</label>
                                            <input type="text" name="name" class="form-control"
                                                placeholder="Nhập họ tên..." required>
                                        </div>
                                        <div class="form-group">
                                            <i class="material-icons">face</i>
                                            <label class="label-control">Email</label>
                                            <input type="text" name="email" class="form-control"
                                                placeholder="Nhập emai..." required>
                                        </div>
                                        <div class="form-group">
                                            <i class="material-icons">lock</i>
                                            <label class="label-control">Mật khẩu</label>
                                            <input type="password" name="password" placeholder="Nhập mật khẩu..."
                                                class="form-control" id="passIn" required />
                                            <span class="material-icons" onclick="show_hide_pass()"
                                                style="cursor: pointer">
                                                visibility
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <i class="material-icons">date_range</i>
                                            <label class="label-control">Ngày sinh</label>

                                            <input type="date" name="date_birth" class="form-control datepicker"
                                                required />
                                        </div>
                                        <div class="form-group">
                                            <i class="material-icons">face</i>
                                            <label class="label-control">Giới tính</label>
                                            </span>
                                            <input type="radio" name="gender" value="1" id="1" checked><label
                                                for="1">Nam</label>
                                            <input type="radio" name="gender" value="0" id="0"><label for="0">Nữ</label>
                                        </div>
                                        <div class="form-group">
                                            <i class="material-icons">phone</i>
                                            <label class="label-control">Số điện thoại</label>
                                            <input type="text" name="phone" class="form-control" required />
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <button class="btn btn-primary btn-round">Đăng ký</button>
                                    </div>
                                    <div class="footer text-center">
                                        <p>Bạn đã có tài khoản?<a style="color: blue" href="/login-customer ">Đăng nhập</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    function show_hide_pass(params) {
        var x = document.getElementById("passIn");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
<!--   Core JS Files   -->
<script src="{{ asset('assets') }}/js/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/material.min.js"></script>

<!--    Plugin for Date Time Picker and Full Calendar Plugin   -->
<script src="{{ asset('assets') }}/js/moment.min.js"></script>

<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
<script src="{{ asset('assets') }}/js/nouislider.min.js" type="text/javascript"></script>

<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker   -->
<script src="{{ asset('assets') }}/js/bootstrap-datetimepicker.js" type="text/javascript"></script>

<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select   -->
<script src="{{ asset('assets') }}/js/bootstrap-selectpicker.js" type="text/javascript"></script>

<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
<script src="{{ asset('assets') }}/js/bootstrap-tagsinput.js"></script>

<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput   -->
<script src="{{ asset('assets') }}/js/jasny-bootstrap.min.js"></script>

<!--    Plugin For Google Maps   -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
<script src="{{ asset('assets') }}/js/material-kit.js?v=1.2.1" type="text/javascript"></script>

</html>
