<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        a {
            border: none;
            background-color: rgb(255, 208, 0);
            font-size: 20px;
            padding: 6px;
            border-radius: 5px 5px;
            cursor: pointer;
        }

    </style>
</head>

<body>
    <h2>Yêu cầu xác thực tài khoản</h2>
    <div>
        <p style="font-size: 15px">Xin chào
            <b style="font-size: 20px">{{ $mail_data['name'] }}</b>
        </p>
        <h2>
            <b>Click vào nút dưới đây để xác thực tài khoản</b>
        </h2>
        <a href="http://127.0.0.1:8000/verify-register?code={{ $mail_data['verification_code'] }}">
            Xác thực tài khoản
        </a>
        <br>
    </div>
</body>

</html>
