@extends('layouts.customer')

@section('content')
    <div class="container">
        <div class="material-datatables">
            <a href="{{ route('bill-periodic-customer') }}" class="btn btn-primary">Quay lại</a>
            <div class="material-datatables">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%"
                    style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>
                                Tên sân
                            </th>
                            <th>
                                Thời gian bắt đầu đá
                            </th>
                            <th>
                                Thời gian kết thúc
                            </th>
                            <th>
                                Giá sân
                            </th>
                            <th>
                                Thay đổi giá
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>
                                Tên sân
                            </th>
                            <th>
                                Thời gian bắt đầu đá
                            </th>
                            <th>
                                Thời gian kết thúc
                            </th>
                            <th>
                                Giá sân
                            </th>
                            <th>
                                Thay đổi giá
                            </th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listBill as $bill)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <th>
                                    <b>{{ $bill->pitch_name }}</b>
                                </th>
                                <td>
                                    {{ $bill->time_start }}
                                </td>
                                <td>
                                    {{ $bill->time_end }}
                                </td>
                                <td>
                                    {{ $bill->pitch_price }}
                                </td>
                                <td>
                                    Tăng {{ $bill->price_change }} %
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
