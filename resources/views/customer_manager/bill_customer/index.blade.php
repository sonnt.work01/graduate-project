@extends('layouts.customer')

@section('content')
    <div class="container">
        <div class="material-datatables">
            <div>
                <h1 style="text-align:center">Danh sách hóa đơn</h1>
            </div>
            @if ($message = Session::get('success'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-success">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-danger">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-8">
                    <form action="" method="get">
                        <div class="col-md-6">
                            @csrf
                            <select name="bill_active" class="selectpicker" data-style="btn btn-primary btn-round">
                                <option value="1" @if ($bill_active == 1) ? selected @endif>Chưa xử lý (đang
                                    chờ
                                    duyệt)
                                </option>
                                <option value="2" @if ($bill_active == 2) ? selected @endif>Đã duyệt</option>
                                <option value="3" @if ($bill_active == 3) ? selected @endif>Đã huỷ</option>
                                <option value="4" @if ($bill_active == 4) ? selected @endif>Đã thanh toán
                                </option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
                            <button class="btn btn-info">Tìm kiếm</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-4" style="text-align: right">
                    <a href="{{ route('bill-periodic-customer') }}" class="btn btn-warning">Hoá đơn đặt định kì</a>
                </div>
            </div>
            @if ($listBill->count() == 0)
                <h4 style="text-align:center; color:red"><b>{{ 'Chưa có hoá đơn' }}</b></h4>
            @else
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>
                                Thể loại đơn
                            </th>
                            <th>
                                Mã hoá đơn
                            </th>
                            <th>
                                Tên khách hàng
                            </th>
                            <th>
                                Ngày đặt
                            </th>
                            <th>
                                Tổng tiền
                            </th>
                            @if ($bill_active != 4)
                                <th>
                                    Tiền đặt cọc
                                </th>
                                <th>
                                    Tiền cần thanh toán sau khi kết thúc
                                </th>
                            @endif
                            <th>
                                Trạng thái hoá đơn
                            </th>
                            <th>
                                Hình thức thanh toán đặt cọc
                            </th>
                            @if ($bill_active == 2)
                                <th>
                                    Điểm
                                </th>
                            @elseif ($bill_active == 3)
                                <th>
                                    Lý do huỷ
                                </th>
                            @elseif ($bill_active == 4)
                                <th>
                                    Điểm
                                </th>
                            @endif
                            @if ($bill_active == 1)
                                <th>
                                    Hành động
                                </th>
                            @endif
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>
                                Thể loại đơn
                            </th>
                            <th>
                                Mã hoá đơn
                            </th>
                            <th>
                                Tên khách hàng
                            </th>
                            <th>
                                Ngày đặt
                            </th>
                            <th>
                                Tổng tiền
                            </th>
                            @if ($bill_active != 4)
                                <th>
                                    Tiền đặt cọc
                                </th>
                                <th>
                                    Tiền cần thanh toán sau khi kết thúc
                                </th>
                            @endif
                            <th>
                                Trạng thái hoá đơn
                            </th>
                            <th>
                                Hình thức thanh toán đặt cọc
                            </th>
                            @if ($bill_active == 2)
                                <th>
                                    Điểm
                                </th>
                            @elseif ($bill_active == 3)
                                <th>
                                    Lý do huỷ
                                </th>
                            @elseif ($bill_active == 4)
                                <th>
                                    Điểm
                                </th>
                            @endif
                            @if ($bill_active == 1)
                                <th>
                                    Hành động
                                </th>
                            @endif
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listBill as $bill)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    @if ($bill->bill_periodic_id)
                                        <b style=" color:rgb(255, 0, 0);">
                                            {{ 'Đặt định kì' }}
                                        </b>
                                    @else
                                        <b style="color:rgb(38, 0, 255);">
                                            {{ 'Đặt thường' }}
                                        </b>
                                    @endif
                                </td>
                                <th>
                                    @if ($bill->bill_periodic_id)
                                        <b>BKSĐK{{ $bill->id }}</b>
                                    @else
                                        <b>BKS{{ $bill->id }}</b>
                                    @endif
                                </th>
                                <th>
                                    <b>{{ $bill->name }}</b>
                                </th>
                                <td>
                                    {{ $bill->created_at }}
                                </td>
                                <td>
                                    {{ number_format($bill->total_price, 0, '', ',') . 'đ' }}
                                </td>
                                @if ($bill_active != 4)
                                    <td>
                                        {{ number_format($bill->deposit, 0, '', ',') . 'đ' }}
                                    </td>
                                    <td>
                                        {{ number_format($bill->missing, 0, '', ',') . 'đ' }}
                                    </td>
                                @endif
                                <td>
                                    {{-- đơn duyệt --}}
                                    @if ($bill->status == 1)
                                        <b style="background-color: blue; color:white; padding:5px; border-radius: 4px">
                                            {{ $bill->StatusName }}
                                        </b>
                                        {{-- đơn duyệt --}}
                                    @elseif($bill->status == 2)
                                        <b
                                            style="background-color: rgb(42, 202, 2); color:white; padding:5px; border-radius: 4px">
                                            {{ $bill->StatusName }}</b>
                                        <br>
                                        <b style="color: red">Hoá đơn đã được gửi về email</b>
                                        {{-- đơn Huỷ --}}
                                    @elseif($bill->status == 3)
                                        <b style="background-color: red; color:white; padding:5px; border-radius: 4px">
                                            {{ $bill->StatusName }}</b>
                                        {{-- đơn đã thanh toán --}}
                                    @elseif($bill->status == 4)
                                        <b
                                            style="background-color: rgb(255, 103, 2); color:white; padding:5px; border-radius: 4px">
                                            {{ $bill->StatusName }}</b>
                                        <br>
                                        <b style="color: red;">Hoá đơn đã được gửi về email</b>
                                    @endif
                                </td>
                                <td style="color: rgb(255, 102, 0); font-weight:bold">
                                    {{ $bill->PaymentTypeName }}
                                </td>
                                {{-- số điểm trừ mỗi đơn --}}
                                @if ($bill_active == 2)
                                    @if ($bill->point != null)
                                        <td>
                                            <b style="color: red;">- {{ $bill->point }}</b>
                                        </td>
                                    @else
                                        <td>
                                            <b style="color: red;">- 0</b>
                                        </td>
                                    @endif

                                    {{-- lý do huỷ --}}
                                @elseif ($bill->status == 3)
                                    <td>
                                        <b style="color: red;">{{ $bill->cancellation_reason }}</b>
                                    </td>

                                    {{-- số điểm được cộng khi thanh toán --}}
                                @elseif ($bill->status == 4)
                                    <td>
                                        <b style="color: red;">+ {{ round($bill->total_price / 100) }}</b>
                                    </td>
                                @endif
                                <td>
                                    <a href="{{ route('bill-detail-customer', $bill->id) }}" class="btn">Xem chi
                                        tiết</a>
                                    @if ($bill->status == 1)
                                        <button class="btn btn-danger destroy-button" data-toggle="modal"
                                            id="{{ $bill->id }}">
                                            Huỷ đơn
                                        </button>
                                        {{-- start modal destroy --}}
                                        <div class="modal" id="destroyModalCustom{{ $bill->id }}" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true" style="display:none;">
                                            <div class="modal-dialog" role="document">
                                                <form action="{{ route('bill_destroy', $bill->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3 class="modal-title" id="exampleModalLabel">Lý do huỷ đơn
                                                            </h3>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12">
                                                                <textarea type="text" name="cancellation_reason" style="width:100%;" class="form-control"
                                                                    placeholder="Nhập lí do huỷ đơn..."></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-bind="{{ $bill->id }}"
                                                                class="btn btn-secondary cancel"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <button type="submit"
                                                                onclick="return confirm('Bạn có chắc chắn huỷ đơn này?')"
                                                                class="btn btn-danger">
                                                                Huỷ đơn
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- end modal destroy --}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <script>
        $(".destroy-button").click(function() {
            $("#destroyModalCustom" + $(this).attr('id')).show();
        })
        $(".cancel").click(function() {
            $("#destroyModalCustom" + $(this).attr('data-bind')).hide();
        })
    </script>
@endsection
