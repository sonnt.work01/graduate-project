@extends('layouts.customer')

@section('content')
    <style>
        #footer {
            position: fixed;
            /* left: 100px;
                                        top: 150px; */
            bottom: 0;
            background: #ffd000;
            line-height: 2;
            text-align: center;
            color: #042E64;
            font-size: 30px;
            font-family: sans-serif;
            font-weight: bold;
        }

        .bootstrap-select>.dropdown-toggle {
            background: #0025ff !important;
            color: white !important;
        }
    </style>
    <div class="container">
        <div class="material-datatables">
            @if ($message = Session::get('success'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-success">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-danger">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            <div>
                <h2 style="text-align:center">Hàng chờ</h2>
            </div>
            @if (!$carts)
                <h4 style="text-align:center; color:red"><b>Hàng chờ trống. Quay lại <a href="{{ route('home') }}">trang chủ</a> để đặt sân</b></h4>
            @else
            <div class="row">
                    <div class="col-md-8">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                            width="100%">
                            <thead>
                                <tr>
                                    <th>
                                        Tên sân
                                    </th>
                                    <th>
                                        Tình trạng
                                    </th>
                                    <th>
                                        Loại sân
                                    </th>
                                    <th>
                                        Ngày đá
                                    </th>
                                    <th>
                                        Thời gian bắt đầu
                                    </th>
                                    <th>
                                        Thời gian kết thúc
                                    </th>
                                    <th>
                                        Giá
                                    </th>
                                    <th>
                                        Hành động
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>
                                        Tên sân
                                    </th>
                                    <th>
                                        Tình trạng
                                    </th>
                                    <th>
                                        Loại sân
                                    </th>
                                    <th>
                                        Ngày đá
                                    </th>
                                    <th>
                                        Thời gian bắt đầu
                                    </th>
                                    <th>
                                        Thời gian kết thúc
                                    </th>
                                    <th>
                                        Giá
                                    </th>
                                    <th>
                                        Hành động
                                    </th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @php
                                    $total = 0;
                                    $deposit = 0;
                                @endphp
                                @foreach ($carts as $id => $data)
                                    @php
                                        $total += $data['final_price'];
                                        $deposit = $total / 2;
                                    @endphp
                                    <tr>
                                        <td>
                                            {{ $data['pitch_name'] }}
                                        </td>
                                        <td>
                                            {{ $data['status_name'] }}
                                        </td>
                                        <td>
                                            {{ $data['category_name'] }}
                                        </td>
                                        <td>
                                            {{ $data['day'] }}
                                        </td>
                                        <td>
                                            {{ $data['time_start'] }}
                                        </td>
                                        <td>
                                            {{ $data['time_end'] }}
                                        </td>
                                        <td>
                                            {{ number_format($data['final_price']) . 'đ' }}
                                        </td>
                                        <td>
                                            <form action="{{ route('delete-cart-item', $id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Xoá</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="card" style="padding: 10px">
                            <form id="form_point" data-url="{{ route('point-cart') }}" method="post">
                                <input id="total_cart" type="hidden" value="{{ $total }}">
                                <div class="row">
                                    <label class="col-sm-6 label-on-left">
                                        <b style="color: black">
                                            Số điểm bạn có:
                                        </b>
                                    </label>
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating is-empty">
                                            <input type="number" value="{{ $customer->points }}"
                                                style="font-size:20px; color:red" class="form-control" readonly required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-6 label-on-left">
                                        <b style="color: black">
                                            Đổi điểm thành tiền<br>
                                            Tỉ lệ:
                                            <br>
                                            <span style="color: red">1000 điểm = 1.000đ</span>
                                        </b>
                                    </label>
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating is-empty">
                                            <input id="point" type="number" style="font-size:20px; color:red"
                                                class="form-control"required>
                                        </div>
                                        <button type="submit" class="btn btn-warning">Đổi điểm</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <p id="error_point" style="color:red; font-weight: bold; font-size: 15px"></p>
                                    </div>
                                </div>
                            </form>
                            @if (count($carts) != 0)
                                <form action="{{ route('order-cart-process') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="point_form" id="point_form_1">
                                    <div class="col-md">
                                        <h4 style="font-weight: bold; text-align:right">
                                            Tiền cần đặt cọc (50% tổng tiền):
                                            {{-- show --}}
                                            <input type="text" id="deposit_cart_1"
                                                value="{{ number_format($deposit) . 'đ' }}">
                                            {{-- input --}}
                                            <input type="hidden" name="deposit_cart_input" id="deposit_cart"
                                                value="{{ $deposit }}">
                                        </h4>
                                        <h4 style="font-weight: bold; text-align:right">
                                            Thành tiền:
                                            {{-- show --}}
                                            <input type="text" id="show_final_cart"
                                                value="{{ number_format($total) . 'đ' }}">

                                            {{-- input --}}
                                            <input type="hidden" id="final_cart_1" name="final_cart_input"
                                                value="{{ $total }}">
                                            <br>
                                        </h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <b><span style="color:red">*</span>Hình thức thanh toán</label></b>
                                                <select name="payment_type" class="selectpicker" required>
                                                    <option value="" selected disabled>Chọn hình thức thanh toán
                                                    </option>
                                                    <option value="0">Trả tiền mặt</option>
                                                    <option value="1">Thanh toán trực tuyến (Momo, Ngân hàng)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row" style="text-align:right">
                                            <div class="col-md-12">
                                                <button onclick="return confirm('Bạn có chắc chắn đặt đơn này?')"
                                                    class="btn btn-danger" style="text-align:right">Đặt sân</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                    
                </div>
        </div>
        <div id="footer">
            <span>
                <p>Thời gian đặt còn lại:</p>
                <p id="coutdown"></p>
            </span>
        </div>
        @endif

        {{-- reload page after 5 minutes --}}
        <script type="text/javascript">
            $(document).ready(function() {
                setInterval(function() {
                    location.reload();
                }, 301000);
            });
            </script>

        {{-- countdown --}}
        <script type="text/javascript">
            $(document).ready(function() {
                function startTimer(duration, display) {
                    var timer = duration,
                        minutes, seconds;
                    setInterval(function() {
                        minutes = parseInt(timer / 60, 10);
                        seconds = parseInt(timer % 60, 10);

                        minutes = minutes < 10 ? "0" + minutes : minutes;
                        seconds = seconds < 10 ? "0" + seconds : seconds;

                        display.text(minutes + ":" + seconds);

                        if (--timer < 0) {
                            timer = duration;
                        }
                    }, 1000);
                }

                jQuery(function($) {
                    var fiveMinutes = 300,
                        display = $('#coutdown');
                    startTimer(fiveMinutes, display);
                });
            });
        </script>
    </div>

    {{-- point --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_point').submit(function(event) {
                event.preventDefault();
                let url = $(this).attr('data-url');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        point_cart: $('#point').val(),
                        total_cart: $('#total_cart').val(),
                    },
                    success: function(response) {
                        console.log(response);

                        if (!response.hasError) {
                            var output = parseInt(response.final).toLocaleString();
                            $('#show_final_cart').val(output + 'đ');

                            $('#final_cart_1').val(response.final);
                            $('#point_form_1').val(response.point);
                            $('#deposit_cart').val(response.deposit_cart);

                            var output_deposit = parseInt(response.deposit_cart)
                                .toLocaleString();
                            $('#deposit_cart_1').val(output_deposit + 'đ');
                        }

                        if (response.hasError) {
                            $('#error_point').append(response.message);
                            setTimeout(function() {
                                $('#error_point').empty();
                            }, 4000);
                        }
                    },
                });
            });
        });
    </script>
@endsection
