@extends('layouts.customer')

@section('content')
    <div class="section">
        <div class="container">
            <div class="material-datatables">
                @if ($message = Session::get('success'))
                    <div class="section cd-section section-notifications" id="notifications">
                        <div class="alert alert-success">
                            <div>
                                <div class="alert-icon">
                                    <i class="material-icons">check</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>
                                <h3>{{ $message }}</h3>
                            </div>
                        </div>
                    </div>
                @endif
                <div>
                    <h2 style="text-align:center">Hoá đơn được đặt thành công</h2>
                    <b style="font-size: 18px; text-align:center"><span style="color:red">*</span>
                        Lưu ý: Xin mời quý khách thanh toán số tiền đặt cọc đã quy định, sau khi thanh toán, hoá đơn của quý
                        khách mới được duyệt và hoá đơn sẽ được gửi về email <span
                            style="color: red">{{ $customer->email }}</span> của bạn
                    </b>
                </div>
                <div class="container">
                    <div class="col-md-5" style="border-right:1px solid black">
                        <h3 style="text-align:center">Thông tin hoá đơn</h3>
                        <h4>Mã hoá đơn: BKS{{ $bill->id }}</h4>
                        <h4>Tên khách hàng: {{ $customer->name }}</h4>
                        <h4>Ngày đặt: {{ $bill->created_at }}</h4>
                        <h4>Tổng tiền: <b>{{ number_format($bill->total_price) }} VNĐ</b> </h4>
                        <h4>Số tiền cần thanh toán trước: <b>{{ number_format($bill->deposit) }} VNĐ</b></h4>
                        <h4>
                            Số tiền còn lại cần thanh toán sau khi hoàn thành trận đấu:
                            <b>{{ number_format($bill->total_price - $bill->deposit) }} VNĐ</b>
                        </h4>
                        @if ($bill->payment_type == 0)
                            <h4>
                                Quý khách chọn hình thức thanh toán: <br> <b>{{ $bill->PaymentTypeName }}</b>
                                <br>
                                Vì vậy, quý khách cần phải đến khu vực sân để thanh toán tiền đặt cọc trước <b
                                    style="color: red">{{ $maxTimePayment }}</b>
                                để hoá đơn của quý khách được xử lý thành công
                                <br>
                                <b style="color:red">* Lưu ý: Sau thời gian trên, quý khách chưa thanh toán số tiền cần đặt
                                    cọc thì hoá đơn đặt sân sẽ bị huỷ</b>
                            </h4>
                        @else
                            <h4>
                                Quý khách chọn hình thức thanh toán: <b>{{ $bill->PaymentTypeName }}</b>
                                <br>
                                Vì vậy, quý khách hãy lựa chọn thanh toán bằng tài khoản Momo hoặc Ngân hàng theo thông tin phía bên phải để thanh toán tiền đặt cọc trước <b
                                    style="color: red">{{ $maxTimePayment }}</b>
                                để hoá đơn của quý khách được xử lý thành công
                                <br>
                                <b style="color:red">* Lưu ý: Sau thời gian trên, quý khách chưa thanh toán số tiền cần đặt
                                    cọc thì hoá đơn đặt sân sẽ bị huỷ</b>
                            </h4>
                        @endif
                    </div>
                    <div class="col-md-7">
                        <h3 style="text-align:center">Hình thức thanh toán</h3>
                        <div class="row">
                            <div class="col-md-8">
                                <b style="font-size: 18px">1. Thanh toán qua Ví điện tử Momo</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản nhận: <b>0862229084</b> hoặc quét mã QR và thanh toán số tiền
                                    <b>{{ number_format($bill->deposit) }} VNĐ</b> <br>kèm theo lời nhắn
                                    <b>{{ $bill->id }}</b></p>
                                <p style="font-size: 18px; color:red"><span style="color:red">*</span>Lưu ý: khi thanh toán
                                    <b>cần nhập đúng chữ số của lời nhắn</b> để hoá đơn của bạn được xử lý chính xác và
                                    nhanh chóng</p>
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('/images/qrmomo.jpg') }}" width="200px" height="200px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b style="font-size: 18px">2. Thanh toán qua tài khoản ngân hàng</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản: <b>06922267101</b></p>
                                <p style="font-size: 18px">Ngân hàng: <b>TpBank</b></p>
                                <p style="font-size: 18px"><span style="color:red">*</span>Quý khách chuyển khoản kèm lời
                                    nhắn <b>{{ $bill->id }}</b></p>
                                <p style="font-size: 18px; color:red"><span style="color:red">*</span>Lưu ý: khi thanh toán
                                    <b>cần nhập đúng chữ số của lời nhắn</b> để hoá đơn của bạn được xử lý chính xác và
                                    nhanh chóng</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
