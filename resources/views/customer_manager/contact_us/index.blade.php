@extends('layouts.contact_us')

@section('content')
    <div class="profile-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="profile">
                        <div class="avatar">
                            <img src="{{ asset('images/' . $introduce->avatar) }}"
                                style="width:150px; height:150px; border-radius:50%">
                        </div>
                        <div class="name">
                            <h3 class="title">Công ty {{ $introduce->company }}</h3>
                            <h6>Company</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="row">
                    <div class="col-md-7">
                        @foreach ($area as $data)
                            <div class="col-md-6">
                                <div>
                                    <b>Địa chỉ:</b>
                                    <p style="font-size: 18px">{{ $data->area_address }}</p>
                                </div>
                                <b>Hotline:</b>
                                <br>
                                @foreach ($contact as $data)
                                    <span>{{ $data->name }}:</span>
                                    <p><b>{{ $data->phone }}</b></p>
                                @endforeach
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.748052772711!2d105.84537481429739!3d21.002734194043736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac7411ccafb1%3A0x8b64e3a9974068e2!2zU8OibiBW4bqtbiDEkOG7mW5nIELDoWNoIEtob2EgLSBMw6ogVGhhbmggTmdo4buL!5e0!3m2!1svi!2s!4v1648273006417!5m2!1svi!2s"
                                    width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy"
                                    referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-5">
                        <h3 style="text-align:center">Hình thức thanh toán</h3>
                        <div class="row">
                            <div class="col-md-8">
                                <b style="font-size: 18px">1. Thanh toán qua Ví điện tử Momo</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản nhận: <b>0862229084</b> hoặc quét mã QR
                            </div>
                            <div class="col-md-4">
                                <img src="{{ asset('/images/qrmomo.jpg') }}" width="200px" height="200px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b style="font-size: 18px">2. Thanh toán qua tài khoản ngân hàng</b>
                                <p style="font-size: 18px">Chủ tài khoản: <b>Nguyễn Thái Sơn</b></p>
                                <p style="font-size: 18px">Số tài khoản: <b>06922267101</b></p>
                                <p style="font-size: 18px">Ngân hàng: <b>TpBank</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
