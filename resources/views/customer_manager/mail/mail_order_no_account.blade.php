<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bill</title>
    <style>
        .invoice-box {
            max-width: 600px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: DejaVu Sans, sans-serif;
            color: rgb(0, 0, 0);
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
            border-collapse: collapse;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(n + 2) {
            text-align: left;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 10px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.item input {
            padding-left: 5px;
        }

        .invoice-box table tr.item td:first-child input {
            margin-left: -5px;
            width: 100%;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        .invoice-box input[type="number"] {
            width: 60px;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial,
                sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0" border="1">
            <tr class="top" style="border-bottom: 1px solid black">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <h2>Công ty Bách Khoa Sports</h2>
                            </td>

                            <td style="text-align: right">
                                Mã hoá đơn: BKS{{ $bill->id }}
                                <br>
                                Ngày tạo: {{ $bill->updated_at }}
                                <br>
                                Nhân viên: {{ $bill->admin_name }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information" style="border-bottom: 1px solid black">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                Khách hàng: <b>{{ $bill->customer_name }}</b>
                                <br>
                                Số điện thoại: <b>{{ $bill->customer_phone }}</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <h3>Thông tin hoá đơn</h3>
                </td>
            </tr>
            <tr colspan="4" style="border-bottom: 1px solid black">
                <td>
                    Ngày đặt: <b>{{ $bill->created_at }}</b>
                    <br>
                    Tổng tiền hoá đơn: <b>{{ number_format($bill->total_price) }} VNĐ</b>
                    <br>
                    Tiền đặt cọc: <b>{{ number_format($bill->deposit) }} VNĐ</b>
                </td>
                <td style="text-align: right" colspan="1">
                    Tiền còn thiếu: <br>
                    <b style="font-size: 25px; color: red">{{ number_format($bill->missing) }} VNĐ</b>
                </td>
                <td style="text-align: right" colspan="2">
                    <b style="font-size: 25px; color: red">Đã được duyệt</b>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <h3>Chi tiết hoá đơn</h3>
                </td>
            </tr>
            <tr class="heading">
                <td>Tên sân</td>
                <td>Thời gian bắt đầu</td>
                <td>Thời gian kết thúc</td>
            </tr>
            @foreach ($billDetail as $data)
                <tr class="item">
                    <td>{{ $data->pitch_name }}</td>
                    <td>{{ $data->time_start }}</td>
                    <td>{{ $data->time_end }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</body>

</html>
