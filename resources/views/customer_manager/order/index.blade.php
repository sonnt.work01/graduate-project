@extends('layouts.order')

@section('content')
    <style>
        #footer {
            position: fixed;
            /* left: 100px;
                                        top: 150px; */
            bottom: 0;
            background: #ffd000;
            line-height: 2;
            text-align: center;
            color: #042E64;
            font-size: 30px;
            font-family: sans-serif;
            font-weight: bold;
        }
        .bootstrap-select>.dropdown-toggle {
            background: #0400f7 !important;
            color: white !important;
        }
    </style>

    <div class="section">
        <div class="container-fluid">
            <a href="{{ route('home') }}" class="btn btn-primary">Quay lại</a>
            <div class="row">
                <div class="col">
                    {{-- validate --}}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <br>
                    <p style="font-size: 25px; font-weight:bold">Sân Bóng Đại Học Bách Khoa Hà Nội</p>
                    <br>
                    <img src="{{ asset('images/' . $pitch->image_path) }}" style="width:550px; height:550px;">
                    <br>
                    <br>
                    <p style="font-size: 20px;font-weight:bold">Giới thiệu {{ $pitch->pitch_name }}</p>
                    <p style="font-size: 18px; line-height: 25px;">{{ $pitch->description }}</p>
                </div>
                <div class="col-md-7">
                    @if ($message = Session::get('error'))
                        <div class="section cd-section section-notifications" id="notifications">
                            <div class="alert alert-danger">
                                <div>
                                    <div class="alert-icon">
                                        <i class="material-icons">check</i>
                                    </div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                    </button>
                                    <h3>{{ $message }}</h3>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card">
                        <h3 class="section-title" align="center" style="text-transform: uppercase; font-weight: bold;">
                            Xin mời bạn cung cấp đầy đủ thông tin
                        </h3>
                        <div class="card-content">
                            <form method="post" action="{{ route('order-process') }}" class="form-horizontal">
                                @csrf
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Tên khách hàng </b><b
                                            style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="form-group label-floating is-empty">
                                            <select name="customer_id" class="form-control">
                                                <option value="{{ Auth::id() }}">
                                                    {{ Auth::user()->name }}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Email </b><b
                                            style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="form-group label-floating is-empty">
                                            <select name="customer_id" class="form-control">
                                                <option value="{{ Auth::id() }}">
                                                    {{ Auth::user()->email }}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Khu vực</b></label>
                                    <div class="col-sm-9">
                                        <div class="form-group label-floating is-empty">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <select name="area_id" class="form-control"
                                                    data-style="btn btn-primary btn-round">
                                                    <option value="{{ $area->id }}">
                                                        {{ $area->area_name }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Sân</b></label>
                                    <div class="col-sm-9">
                                        <div class="form-group label-floating is-empty">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <select id="pitch123" name="pitch_id" class="form-control"
                                                    data-style="btn btn-primary btn-round">
                                                    <option value="{{ $pitch->id }}">
                                                        {{ $pitch->pitch_name }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Ngày</b><b
                                            style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="col-lg-5 col-md-6 col-sm-3">
                                            <div class="form-group label-floating is-empty">
                                                @if ($weekday == null)
                                                    <input id="day" type="date" name="day"
                                                        value="{{ $date }}" min="{{ $date }}"
                                                        class="form-control datepicke" readonly>
                                                @else
                                                    <input type="hidden" value="{{ $weekday }}"
                                                        name="weekday_periodic">
                                                    <input type="hidden" value="{{ $first_date }}"
                                                        name="first_date_periodic">
                                                    <input type="hidden" value="{{ $second_date }}"
                                                        name="second_date_periodic">
                                                    <textarea style="color: red; font-weight:bold" class="form-control rounded-0" rows="5" readonly>
                                                        {{ $date }}
                                                    </textarea>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Thời gian bắt đầu
                                        </b><b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="col-lg-5 col-md-6 col-sm-3">
                                            <div class="form-group label-floating is-empty">
                                                <input id="time_start" type="time" name="time_start" readonly
                                                    value="{{ $time_start }}" class="form-control datepicke">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">Thời gian kết
                                            thúc</b><b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="col-lg-5 col-md-6 col-sm-3">
                                            <div class="form-group label-floating is-empty">
                                                <input id="time_end" type="time" name="time_end" readonly
                                                    value="{{ $time_end }}" class="form-control datepicke">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">
                                            Thời lượng (phút)</b><b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="col-lg-5 col-md-6 col-sm-3">
                                            <div class="form-group label-floating is-empty">
                                                <input type="text" value="{{ $minute }}" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 label-on-left"><b style="color: black">
                                            <span style="color:red">*</span>Hình thức thanh toán tiền đặt cọc</b></label>
                                    <div class="col-sm-5">
                                            <div class="form-group label-floating is-empty">
                                                <select name="payment_type" class="selectpicker" required>
                                                    <option value="" selected disabled>Chọn hình thức thanh toán</option>
                                                    <option value="0">Trả tiền mặt</option>
                                                    <option value="1">Thanh toán trực tuyến (Momo, Ngân hàng)</option>
                                                </select>
                                            </div>
                                    </div>
                                </div>
                                <div>
                                    {{-- điểm khi đổi --}}
                                    <input id="point_form" type="hidden" name="point_input">
                                    {{-- tiền cuối cùng khi đổi điểm --}}
                                    <input id="final_price" type="hidden" name="final_price" class="form-control">
                                </div>
                                @if ($weekday)
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Số tiền cần trả mỗi ngày đá (VNĐ)</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input
                                                        value="{{ number_format($show_final_price, 0, '', ',') . 'đ' }}"
                                                        style="font-size:20px; color:red" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Số tiền bạn cần đặt cọc khi đặt định kì (VNĐ)</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input type="text"
                                                        value="{{ number_format(1000000, 0, '', ',') . 'đ' }}"
                                                        name="show_final_price_periodic" style="font-size:20px; color:red"
                                                        class="form-control" readonly required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">
                                                Tổng tiền cần thanh toán cho thời hạn đặt định kỳ <span style="color:red">{{$count_date_periodic}} ngày</span> (VNĐ)</b></label>
                                        <div class="col-sm-9">
                                            <div class="col-lg-5 col-md-6 col-sm-3">
                                                <div class="form-group label-floating is-empty">
                                                    <input type="text"
                                                        value="{{ number_format($total_price, 0, '', ',') . 'đ' }}"
                                                        name="show_final_price_periodic" style="font-size:20px; color:red"
                                                        class="form-control" readonly required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12" style="text-align:right">
                                        <button type="submit" onclick="return confirm('Bạn có chắc chắn đặt đơn này?')"
                                            class="btn btn-primary">Đặt</button>
                                    </div>
                                </div>
                            </form>
                            @if (!$weekday)
                                <div class="row">
                                    <div class="col-md-12" style="text-align:right">
                                        <a href="#" data-url="{{ route('add-to-cart', $pitch->id) }}"
                                            class="btn btn-warning add_to_cart">Thêm vào hàng chờ</a>
                                        <p id="error" style="color:red; font-weight: bold; font-size: 15px"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <form id="form_point" data-url="{{ route('point') }}" method="post">
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left">
                                                <b style="color: black">
                                                    Số điểm bạn có:
                                                </b>
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <div class="form-group label-floating is-empty">
                                                        <input type="number" value="{{ $customer->points }}"
                                                            style="font-size:20px; color:red" class="form-control"
                                                            readonly required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left">
                                                <b style="color: black">
                                                    Đổi điểm thành tiền <br>
                                                    Tỉ lệ:
                                                    <br>
                                                    <span style="color: red">1000 điểm = 1.000đ</span>
                                                </b>
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <div class="form-group label-floating is-empty">
                                                        <input id="point" type="number"
                                                            style="font-size:20px; color:red"
                                                            class="form-control"required>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-success">Đổi điểm</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <p id="error_point" style="color:red; font-weight: bold; font-size: 15px">
                                                </p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">
                                                    Thành tiền</b>
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <div class="form-group label-floating is-empty">
                                                        <input id="show_final_price_2" type="text"
                                                            value="{{ number_format($show_final_price, 0, '', ',') . 'đ' }}"
                                                            style="font-size:20px; color:red" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <span>
            <p>Thời gian đặt còn lại:</p>
            <p id="coutdown"></p>
        </span>
    </div>

    {{-- reload page after 5 minutes --}}
    <script type="text/javascript">
        $(document).ready(function() {
            setInterval(function() {
                location.reload();
            }, 301000);
        });
    </script>

    {{-- countdown --}}
    <script type="text/javascript">
        $(document).ready(function() {
            function startTimer(duration, display) {
                var timer = duration,
                    minutes, seconds;
                setInterval(function() {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    display.text(minutes + ":" + seconds);

                    if (--timer < 0) {
                        timer = duration;
                    }
                }, 1000);
            }

            jQuery(function($) {
                var fiveMinutes = 300,
                    display = $('#coutdown');
                startTimer(fiveMinutes, display);
            });
        });
    </script>

    {{-- point --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_point').submit(function(event) {
                event.preventDefault();
                let url = $(this).attr('data-url');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        point: $('#point').val(),
                        pitch_id: $('#pitch123').val(),
                        day: $('#day').val(),
                        time_start: $('#time_start').val(),
                        time_end: $('#time_end').val(),
                    },
                    success: function(response) {
                        console.log(response);
                        if (!response.hasError) {
                            $('#final_price').val(response.final);
                            var output = parseInt(response.final).toLocaleString();
                            $('#show_final_price_2').val(output + 'đ');
                            $('#point_form').val(response.point);
                        }

                        if (response.hasError) {
                            $('#error_point').append(response.message);
                            setTimeout(function() {
                                $('#error_point').empty();
                            }, 4000);
                        }
                    },
                });
            });
        });
    </script>

    {{-- add to cart --}}
    <script>
        function addToCart(event) {
            event.preventDefault();
            let url = $(this).data('url');

            $.ajax({
                type: "GET",
                url: url,
                dataType: "json",
                data: {
                    day: $('#day').val(),
                    time_start: $('#time_start').val(),
                    time_end: $('#time_end').val(),
                    deposit: $('#deposit').val(),
                },
                success: function(data) {
                    console.log(data);
                    if (data.hasError == false) {
                        alert(data.message);
                    }

                    if (data.hasError == true) {
                        $('#error').append(data.message);
                        setTimeout(function() {
                            $('#error').empty();
                        }, 5000);
                    }
                },
                error: function(data) {}
            });
        }
        $(function() {
            $('.add_to_cart').on('click', addToCart);
        });
    </script>
@endsection
