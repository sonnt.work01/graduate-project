@extends('layouts.order')

@section('content')
    <div class="section">
        <div class="container">
            <a href="{{ url()->previous() }}" class="btn btn-primary">Quay lại</a>
            <div class="row">
                <div class="col">
                    {{-- validate --}}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($message = Session::get('error'))
                        <div class="section cd-section section-notifications" id="notifications">
                            <div class="alert alert-danger">
                                <div>
                                    <div class="alert-icon">
                                        <i class="material-icons">check</i>
                                    </div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                    </button>
                                    <h3>{{ $message }}</h3>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p style="font-size: 25px; font-weight:bold">Sân Bóng Đại Học Bách Khoa Hà Nội</p>
                    <br>
                    <img src="{{ asset('images/' . $pitch->image_path) }}" style="width:550px; height:550px;">
                </div>
                <div class="col-md-6">
                    <p style="font-size: 20px;font-weight:bold">Giới thiệu {{ $pitch->pitch_name }}</p>
                    <p style="font-size: 18px; line-height: 25px;">{{ $pitch->description }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
