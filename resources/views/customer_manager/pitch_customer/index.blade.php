@extends('layouts.customer')

@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="section cd-section section-notifications" id="notifications">
                <div class="alert alert-danger">
                    <div>
                        <div class="alert-icon">
                            <i class="material-icons">check</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>
                        <h3>{{ $message }}</h3>
                    </div>
                </div>
            </div>
        @elseif ($message = Session::get('error'))
            <div class="section cd-section section-notifications" id="notifications">
                <div class="alert alert-danger">
                    <div>
                        <div class="alert-icon">
                            <i class="material-icons">check</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>
                        <h3>{{ $message }}</h3>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <h1>Đặt sân</h1>
            <form action="" method="get">
                <input type="date" name="search" value="{{ $search }}" min="{{ $today }}" style="padding: 5px">
                <button class="btn btn-info">Chọn ngày</button>
            </form>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>
                            Sân
                        </th>
                        <th>
                            Khung giờ đã đặt
                        </th>
                    </tr>
                    @foreach ($listPitch as $pitch)
                        <tr>
                            <th>
                                {{$pitch->pitch_name}}
                            </th>
                            @foreach ($listTime as $time)
                            @if ($pitch->id == $time->id)
                                <td>
                                    {{ date("H:i:s",strtotime($time->time_start)) . ' - ' . date("H:i:s",strtotime($time->time_end))}}
                                </td>
                                @endif
                            @endforeach
                            {{-- @if ($time->location == 1)
                                <th>
                                    {{ $time->pitch_name . '(G) ' . $time->time_start . ' - ' . $time->time_end}}
                                </th>
                            @else
                                <th>
                                    {{ $time->pitch_name . '(Đ)' }}
                                </th>
                            @endif --}}
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <h2 class="section-title" align="center">Các sân của khu vực {{ $area->area_name }}</h2>

            <ul class="nav nav-pills nav-pills-rose">
                <li class="active"><a href="#san7" data-toggle="tab" aria-expanded="true">Sân 7</a></li>
                <li class="___class_+?14___"><a href="#san11" data-toggle="tab" aria-expanded="false">Sân 11</a></li>
            </ul>
            <div class="tab-content tab-space">
                <div class="tab-pane active" id="san7">
                    <div class="row">
                        @foreach ($listPitch as $pitch)
                            @if ($pitch->pitch_type == 0)
                                <div class="col-md-4">
                                    <div class="card card-blog">
                                        <div class="card-image">
                                            <a href="#pablo">
                                                <img src="{{ asset('images/' . $pitch->image_path) }}"
                                                    style="width: 360px; height:360px">
                                            </a>
                                            <div class="colored-shadow"
                                                style="background-image: url(&quot;assets/img/examples/color1.jpg&quot;); opacity: 1;">
                                            </div>
                                            <div class="ripple-container"></div>
                                        </div>
                                        <div class="card-content">
                                            <h6 class="category text-rose">Trends</h6>
                                            <h4 class="card-title">
                                                <a href="#pablo">
                                                    <td>{{ $pitch->pitch_name }}</td>
                                                </a>
                                            </h4>
                                            <p class="card-description" style="color:rgb(28, 3, 116); font-size:20px">
                                                {{ $pitch->area_name }}
                                            </p>
                                            <p class="card-description">
                                                <b style="color: red">
                                                    <h3>Giá: {{ number_format($pitch->final_price) }}<sup><u>đ</u></sup></h3>
                                                </b>
                                            </p>
                                            <div class="row">
                                                <div class="col-md-12" align="center">
                                                    <a href="/order/{{ $area->id }}/{{ $pitch->id }}/{{ $search }}"
                                                        class="btn btn-primary btn-round">
                                                        <i class="material-icons">favorite</i>
                                                        Đặt sân
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="tab-pane" id="san11">
                    <div class="row">
                        @foreach ($listPitch as $pitch)
                            @if ($pitch->pitch_type == 1)
                                <div class="col-md-4">
                                    <div class="card card-blog">
                                        <div class="card-image">
                                            <a href="#pablo">
                                                <img src="{{ asset('images/' . $pitch->image_path) }}">
                                            </a>
                                            <div class="colored-shadow"
                                                style="background-image: url(&quot;assets/img/examples/color1.jpg&quot;); opacity: 1;">
                                            </div>
                                            <div class="ripple-container"></div>
                                        </div>
                                        <div class="card-content">
                                            <h6 class="category text-rose">Trends</h6>
                                            <h4 class="card-title">
                                                <a href="#pablo">
                                                    <td>{{ $pitch->pitch_name }}</td>
                                                </a>
                                            </h4>
                                            <p class="card-description">
                                                {{ $pitch->area_name }}
                                                {{ $pitch->price }}
                                            </p>
                                            <div class="row">
                                                <div class="col-md-12" align="center">
                                                    <a href="/order/{{ $area->id }}/{{ $pitch->id }}/{{ $search }}"
                                                        class="btn btn-primary btn-round">
                                                        <i class="material-icons">favorite</i>
                                                        Đặt sân
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
