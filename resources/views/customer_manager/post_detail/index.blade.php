@extends('layouts.post')

@section('content')
    <div class="container">
        <div class="section section-text">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if ($message = Session::get('error'))
                        <div class="section cd-section section-notifications" id="notifications">
                            <div class="alert alert-danger">
                                <div>
                                    <div class="alert-icon">
                                        <i class="material-icons">check</i>
                                    </div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                    </button>
                                    <h3>{{ $message }}</h3>
                                </div>
                            </div>
                        </div>
                    @endif
                    <article>
                        <h3 class="title">{{ $post->title }}</h3>
                        <img src="{{ asset('images/' . $post->image_path) }}" width="100%" alt="">
                        <p>{{ $post->content }}</p>
                    </article>
                </div>
            </div>
        </div>

        <div class="section section-blog-info">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="blog-tags">
                                Tags:
                                <span class="label label-primary">Vietnamese football</span>
                                <span class="label label-primary">Cong Phuong</span>
                                <span class="label label-primary">World Football</span>
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title text-center">Bài viết khác</h2>
                    <br />
                    <div class="row">
                        @foreach ($listPost as $post)
                            <div class="col-md-4">
                                <div class="card card-blog">
                                    <div class="card-image">
                                        <a href="{{ route('post-detail', $post->id) }}">
                                            <img class="img img-raised" src="{{ asset('images/' . $post->image_path) }}"
                                                style="width:330px; height:330px" />
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        <h6 class="category text-info">{{ $post->date_submitted }}</h6>
                                        <h4 class="card-title">
                                            <a href="{{ route('post-detail', $post->id) }}">
                                                {{ $post->title }}
                                            </a>
                                        </h4>
                                        <p class="card-description"
                                            style="overflow: hidden;text-overflow: ellipsis;-webkit-line-clamp: 5; -webkit-box-orient: vertical; display: -webkit-box;">
                                            {{ $post->content }}
                                        </p>
                                        <a href="{{ route('post-detail', $post->id) }}">
                                            <h4>Xem thêm...</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
