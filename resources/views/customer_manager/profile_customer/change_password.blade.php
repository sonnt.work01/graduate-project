@extends('layouts.customer')

@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="section cd-section section-notifications" id="notifications">
                <div class="alert alert-success">
                    <div>
                        <div class="alert-icon">
                            <i class="material-icons">check</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>
                        <h3>{{ $message }}</h3>
                    </div>
                </div>
            </div>
        @elseif ($message = Session::get('error'))
            <div class="section cd-section section-notifications" id="notifications">
                <div class="alert alert-danger">
                    <div>
                        <div class="alert-icon">
                            <i class="material-icons">check</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>
                        <h3>{{ $message }}</h3>
                    </div>
                </div>
            </div>
        @endif
        <h1>Đổi mật khẩu</h1>
        <div class="row">
            <div class="col-md-12">
                <h3 id="error" style="color:red; font-weight: bold"></h3>
                <h3 id="success" style="color:rgb(49, 156, 0); font-weight: bold"></h3>
            </div>
        </div>
        {{-- validate --}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li><span style="font-size: 19px">{{ $error }}</span></li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form id="form_change_pass" data-url="{{ route('change-pass-cus-process', Auth::id()) }}" method="post">
            @csrf
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>
                            Mật khẩu hiện tại
                        </th>
                        <td><input id="old_password" type="password" name="old_password" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Mật khẩu mới
                        </th>
                        <td><input id="new_password" type="password" name="new_password" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Xác nhập lại mật khẩu
                        </th>
                        <td><input id="repeat_new_password" type="password" name="repeat_new_password" class="form-control"
                                required></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-info">Cập nhật</button>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_change_pass').submit(function(event) {
                event.preventDefault();
                let url = $(this).attr('data-url');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "post",
                    data: {
                        old_password: $('#old_password').val(),
                        new_password: $('#new_password').val(),
                        repeat_new_password: $('#repeat_new_password').val(),
                    },
                    success: function(response) {
                        $('#loader_custom').fadeOut();
                        console.log(response);

                        if (!response.hasError) {
                            $('#success').append(response.message);
                            setTimeout(function() {
                                window.location.reload();
                            }, 3000);
                        }
                        if (response.hasError) {
                            $('#error').append(response.message);
                            setTimeout(function() {
                                $('#error').empty();
                            }, 3000);
                        }
                    },
                    error: function(response) {
                        // console.log(response);
                        $('#error').append(response.responseJSON.errors
                            .new_password);
                        setTimeout(function() {
                            $('#error').empty();
                        }, 3000);
                    }
                });
            });
        });
    </script>
@endsection
