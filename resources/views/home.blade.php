@extends('layouts.customer')

@section('content')
    <style>
        .orderCSS {
            width: 100%;
            height: 135px;
            background-image: url('{{ asset('images/order.png') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .emptyCSS {
            width: 100%;
            height: 135px;
            background-image: url('{{ asset('images/empty.jpg') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .init {
            width: 100%;
            height: 135px;
            background-image: url('{{ asset('images/empty.jpg') }}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        img {
            width: 100px;
        }

        .form-group {
            margin: 0 !important;
        }

        #error {
            font-size: 18px;
            background-color: #D8000C;
            color: white;
            padding: 10px;
            font-weight: bold;
        }

        #loader_custom {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 999;
            /* or higher if necessary */
            top: 0;
            left: 0;
            text-indent: 100%;
            background: url('{{ asset('loader/image_processing20210909-1419-19deel7.gif') }}') center no-repeat;
        }

        .btn-group>.btn:first-child {
            margin-left: -20px !important;
        }

        .bootstrap-select>.dropdown-toggle {
            width: 112% !important;
        }

        .bootstrap-select>.dropdown-toggle {
            background: #ff7600 !important;
            color: white !important;
            font-size: 15px;
            padding: 15px;
        }
    </style>
    <div id="loader_custom" style="background-color: rgb(255, 255, 255)"></div>

    <div class="section">
        <div class="container">
            {{-- success --}}
            @if ($message = Session::get('success'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-success">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            {{-- error --}}
            @if ($message = Session::get('error'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-danger">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">clear</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <h3 class="section-title" style="text-transform: uppercase; font-weight: bold;">
                    Khung giờ đã đặt
                </h3>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>
                                Sân
                            </th>
                            <th>
                                Khung giờ đã đặt
                            </th>
                        </tr>
                        @foreach ($listPitchTime as $pitch)
                            <tr>
                                <th>
                                    {{ $pitch->pitch_name }}
                                </th>
                                @foreach ($listTime as $time)
                                    @if ($pitch->id == $time->id)
                                        <td>
                                            {{ date('H:i:s', strtotime($time->time_start)) . ' - ' . date('H:i:s', strtotime($time->time_end)) }}
                                        </td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                    </table>
                </div>
                {{-- div pitch --}}
                <div class="row">
                    <div class="col-md-12">
                        <b style="font-size: 18px; padding-left:32px; margin: 0">
                            <span style="color:red">*</span>Xin mời quý khách lựa chọn khung giờ <span
                                style="color: red">(tối thiểu 60 phút)</span> để tìm cho mình sân bóng
                            phù hợp
                        </b>
                    </div>
                </div>
                <div class="col-sm-8">
                    <h3 class="section-title" style="text-transform: uppercase; font-weight: bold;">
                        Sơ đồ sân Bách Khoa
                    </h3>

                    <div style="width:100%; height:50px; background-color: #c2c2c2; border-radius:5px">
                        <div style="text-align:center">
                            <h5><b>Khán đài B</b></h5>
                        </div>
                    </div>
                    <br>
                    <fieldset>
                        @foreach ($listPitch as $pitch)
                            @if ($pitch->category_id == 2)
                                <legend>Sân 11 - <a style="color: rgb(38, 0, 255)"
                                        href="/pitch-detail/{{ $pitch->area_id }}/{{ $pitch->id }}">Xem chi tiết</a>
                                    <div id="form_order11" class="form_order">
                                        <form action="/order/{{ $pitch->area_id }}/{{ $pitch->id }}" method="post">
                                            @csrf
                                            <input class="date_hidden" type="hidden" value="{{ $search }}"
                                                name="date">
                                            <input class="time_start_hidden" type="hidden" value="{{ $start }}"
                                                name="time_start">
                                            <input class="time_end_hidden" type="hidden" value="{{ $end }}"
                                                name="time_end">
                                            <input class="date_periodic_end" type="hidden" name="date_periodic_end">
                                            <input class="weekday_periodic" type="hidden" name="weekday_periodic">
                                            <input class="date_periodic_start" type="hidden" name="date_periodic_start">
                                            <button class="btn"
                                                style="background-color: #fffb07; color:black; font-weight:bold">
                                                Đặt sân 11
                                            </button>
                                        </form>
                                    </div>
                                </legend>
                            @endif
                            @if ($pitch->category_id == 1)
                                <div class="col-sm-6 cart-pitch">
                                    <div class="card card_pitch init" id="{{ $pitch->id }}">
                                        <div class="card-body">
                                            <h5 class="card-title"
                                                style="color: rgb(255, 217, 0);
                                                        text-align: center; margin-top: 0">
                                                {{ $pitch->pitch_name }} - <a style="color: rgb(255, 217, 0);"
                                                    href="/pitch-detail/{{ $pitch->area_id }}/{{ $pitch->id }}">Xem
                                                    chi tiết</a>
                                            </h5>
                                            <div id="form_order{{ $pitch->id }}" class="form_order">
                                                <form action="/order/{{ $pitch->area_id }}/{{ $pitch->id }}"
                                                    method="post">
                                                    @csrf
                                                    <input class="date_hidden" type="hidden" name="date">
                                                    <input class="time_start_hidden" type="hidden" name="time_start">
                                                    <input class="time_end_hidden" type="hidden" name="time_end">
                                                    <input class="date_periodic_end" type="hidden"
                                                        name="date_periodic_end">
                                                    <input class="weekday_periodic" type="hidden" name="weekday_periodic">
                                                    <input class="date_periodic_start" type="hidden"
                                                        name="date_periodic_start">
                                                    <button class="btn"
                                                        style="background-color: #fffb07; color:black; font-weight:bold">
                                                        Đặt sân
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </fieldset>
                    <div class="row">
                        <div class="col-sm-3">
                            <h5 style="text-align:center"><i>Cổng vào</i> <i
                                    class="fa-solid fa-circle-arrow-up-right"></i>
                            </h5>
                        </div>
                        <div class="col-sm-6">
                            <div style="width:100%; height:50px; background-color: #c2c2c2; border-radius:5px">
                                <div style="text-align:center">
                                    <h5><b>Khán đài A</b></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <h5 style="text-align:center"><i class="fa-solid fa-circle-up-left"></i> <i>Cổng vào</i></h5>
                        </div>
                    </div>
                    <div class="row"
                        style="border-top: 2px solid black; border-bottom: 2px solid black; margin-top: 5px;">
                        <h5><b>Đường Lê Thanh Nghị</b></h5>
                    </div>
                </div>

                {{-- div time --}}
                <br>
                <br>
                <div class="col-sm-4" style="border: 2px solid;">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="section-title" style="text-transform: uppercase; font-weight: bold;">
                                Chọn khung giờ
                            </h3>
                            <div class="row">
                                <p style="margin: 0 10px 0 16px; font-weight: bold">
                                    Giờ hoạt động: <span style="color:red;"> 5h30 sáng </span> đến <span
                                        style="color:red;"> 23h30 </span> hàng ngày
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 id="error"></h5>
                                </div>
                            </div>
                            <ul class="nav nav-pills nav-pills-warning">
                                <li class="active">
                                    <a href="#pill1" data-toggle="tab">Đặt thường</a>
                                </li>
                                <li><a href="#pill2" data-toggle="tab">Đặt định kì</a></li>
                            </ul>
                            <div class="tab-content tab-space">
                                <div class="tab-pane active" id="pill1">
                                    <div class="col-sm-12">
                                        <form id="form_filter" data-url="{{ route('pitch-filter') }}" method="post">
                                            @csrf
                                            <div class="row m-2">
                                                <label class="col-sm-6 label-on-left"><b style="color: black">Chọn ngày
                                                    </b><b style="color: red">*</b></label>
                                                <div class="col-sm-6">
                                                    <div class="form-group label-floating is-empty">
                                                        <input id="date" type="date" name="search"
                                                            value="{{ $search }}" min="{{ $today }}"
                                                            max="{{ $maxDate3Day }}" style="padding: 5px">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row m-2">
                                                <label class="col-sm-6 label-on-left"><b style="color: black">Chọn khung
                                                        giờ </b><b style="color: red">*</b></label>
                                                <div class="col-sm-6">
                                                    <div class="form-group label-floating is-empty">
                                                        <select id="time_order_normal" class="selectpicker">
                                                            @foreach ($arr_time as $item)
                                                                <option
                                                                    value="{{ $item['time_start'] }}-{{ $item['time_end'] }}">
                                                                    {{ $item['time_start'] }} - {{ $item['time_end'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-info btn_filter">Chọn</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pill2">
                                    <div class="col-sm-12">
                                        <p style="color:red; font-size:18px"><span>* Lưu ý: chọn ngày kết thúc phải tối
                                                thiểu đủ 1 tháng tính từ ngày bắt đầu</span></p>
                                        <form id="form_filter_periodic" data-url="{{ route('pitch-filter-periodic') }}"
                                            method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <b style="color: black">Chọn thứ trong tuần </b>
                                                    <select name="" id="weekday" class="selectpicker">
                                                        <option value="Monday">Thứ 2</option>
                                                        <option value="Tuesday">Thứ 3</option>
                                                        <option value="Wednesday">Thứ 4</option>
                                                        <option value="Thursday">Thứ 5</option>
                                                        <option value="Friday">Thứ 6</option>
                                                        <option value="Saturday">Thứ 7</option>
                                                        <option value="Sunday">Chủ nhật</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <b class="col-sm-6 label-on-left" style="color: black">Ngày bắt đầu<b
                                                        style="color: red">*</b></b>
                                                <div class="col-sm-6">
                                                    <input id="date_start_periodic" type="date"
                                                        min="{{ $today }}" name="date_start_periodic"
                                                        value="{{ $today }}" class="form-control datepicke"
                                                        required>
                                                </div>
                                                <b class="col-sm-6 label-on-left" style="color: black">Ngày kết thúc<b
                                                        style="color: red">*</b></b>
                                                <div class="col-sm-6">
                                                    <input id="date_end_periodic" type="date"
                                                        min="{{ $minDate1Month }}" value="{{ $minDate1Month }}"
                                                        name="date_end_periodic" class="form-control datepicke" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-6 label-on-left"><b style="color: black">Chọn khung
                                                        giờ</b><b style="color: red">*</b></label>
                                                <div class="col-sm-6">
                                                    <div class="form-group label-floating is-empty">
                                                        <select id="time_order_periodic" class="selectpicker">
                                                            @foreach ($arr_time as $item)
                                                                <option
                                                                    value="{{ $item['time_start'] }}-{{ $item['time_end'] }}">
                                                                    {{ $item['time_start'] }} - {{ $item['time_end'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div class="row">
                                                <label class="col-sm-6 label-on-left"><b style="color: black">Thời gian
                                                        bắt đầu trận</b><b style="color: red">*</b></label>
                                                <div class="col-sm-6">
                                                    <div class="form-group label-floating is-empty">
                                                        <input id="time_start_periodic" type="time" min="06:00"
                                                            max="22:45" name="time_start_periodic"
                                                            class="form-control datepicke" required>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            {{-- <div class="row">
                                                <label class="col-sm-6 label-on-left"><b style="color: black">Thời gian
                                                        kết thúc </b><b style="color: red">*</b></label>
                                                <div class="col-sm-6">
                                                    <div class="form-group label-floating is-empty">
                                                        <input id="time_end_periodic" type="time" min="07:00"
                                                            max="23:45" name="time_end_periodic"
                                                            class="form-control datepicke" required>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <button type="submit" class="btn btn-info btn_filter">Chọn</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h4><span style="color: red">*</span>
                        <strong style="font-size: 14px">Chú thích:</strong>
                    </h4>
                    <div class="col-sm-6">
                        <div class="card"
                            style="width: 100%;
                            color: white;
                            background-image: url('{{ asset('images/empty.jpg') }}');
                            background-position: center;
                            background-repeat: no-repeat;
                            background-size: cover;">
                            <div class="card-body">
                                <h6><b>Còn trống</b></h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card"
                            style="width: 100%;
                            color: white;
                            background-image: url('{{ asset('images/order.png') }}');
                            background-position: center;
                            background-repeat: no-repeat;
                            background-size: cover;">
                            <div class="card-body">
                                <h6><b>Đã có người đặt</b></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section -->
    <div class="section section-blog">
        <div class="container">
            <h3 class="section-title" style="text-transform: uppercase; font-weight: bold;">
                Bài Viết
            </h3>
            <div class="row">
                @foreach ($listPost as $post)
                    <div class="col-md-4 col-sm-4">
                        <div class="card card-background"
                            style="background-image: url({{ asset('images/' . $post->image_path) }}); width: 360px; height:360px">
                            <div class="card-content">
                                <h6 class="category text-info">{{ $post->date_submitted }}</h6>
                                <a href="{{ route('post-detail', $post->id) }}">
                                    <h3 class="card-title"
                                        style="overflow: hidden;text-overflow: ellipsis;-webkit-line-clamp: 2; -webkit-box-orient: vertical; display: -webkit-box;">
                                        {{ $post->title }}</h3>
                                </a>
                                <p class="card-description"
                                    style="overflow: hidden;text-overflow: ellipsis;-webkit-line-clamp: 5; -webkit-box-orient: vertical; display: -webkit-box;">
                                    {{ $post->content }}
                                </p>
                                <a href="{{ route('post-detail', $post->id) }}" class="btn btn-white btn-round">
                                    <i class="material-icons">subject</i> Đọc thêm
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-sm-10">
                </div>
                <div class="col-sm-2">
                    {{ $listPost->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- section -->
    <div class="page-header header-filter clear-filter header-small"
        style="background-image: url('{{ asset('images/1639195617.jpg') }}');" style="width:100%; heigth:500px">
    </div>

    {{-- Đặt thường --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#error').hide();
            $('.form_order').hide();
            $('#loader_custom').hide();

            $('#form_filter').submit(function(event) {

                event.preventDefault();
                let url = $(this).attr('data-url');
                if ($('#time_start').val() == null && $('#time_end').val()) {
                    $('.form_order').hide().attr('disabled', true);
                }
                $('#loader_custom').fadeIn();
                setTimeout(function() {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: url,
                        method: "post",
                        dataType: 'json',
                        data: {
                            date: $('#date').val(),
                            time_order_normal: $('#time_order_normal').val(),
                        },
                        success: function(response) {
                            $('#loader_custom').fadeOut();
                            console.log(response);

                            //min 60 minute
                            if (response.hasError) {
                                $('#error').show();
                                $('#error').append(response.message);
                                $('.form_order').hide();
                                setTimeout(function() {
                                    $('#error').empty();
                                    $('#error').hide();
                                }, 4000);
                            }

                            if (!response.hasError) {
                                $('.date_periodic_end').val("");
                                $('.weekday_periodic').val("");
                                $('.date_periodic_start').val("");

                                $('.date_hidden').val(response.date);
                                $('.time_start_hidden').val(response.start);
                                $('.time_end_hidden').val(response.end);


                                let arr_pitch = response.listPitch;
                                let arr_order = response.array_pitch;
                                let count = 0;
                                let arr = [];
                                for (let i = 0; i < arr_pitch.length; i++) {
                                    for (let j = 0; j < arr_order.length; j++) {
                                        if (arr_pitch[i].id === arr_order[j]) {
                                            arr.push(arr_pitch[i].id);
                                        }
                                    }
                                }
                                if (arr.length != 0) {
                                    $('#form_order11').hide();
                                } else {
                                    $('#form_order11').show();
                                }
                                for (let i = 0; i < arr_pitch.length; i++) {
                                    if (jQuery.inArray(arr_pitch[i].id, arr) === -1) {
                                        $('#' + arr_pitch[i].id).removeClass('init');
                                        $('#' + arr_pitch[i].id).removeClass(
                                            'orderCSS');
                                        $('#' + arr_pitch[i].id).addClass('emptyCSS');
                                        $('#form_order' + arr_pitch[i].id).show();
                                    } else {
                                        $('#' + arr_pitch[i].id).removeClass('init');
                                        $('#' + arr_pitch[i].id).removeClass(
                                            'emptyCSS');
                                        $('#' + arr_pitch[i].id).addClass('orderCSS');
                                        $('#form_order' + arr_pitch[i].id).hide();
                                    }
                                }
                            }
                        },
                        error: function(error, status) {
                            console.log(error);
                            console.log(status);
                        }
                    }, "json");
                }, 1000);
            });
        });
    </script>

    {{-- Đặt định kỳ --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#error').hide();
            $('.form_order').hide();
            $('#loader_custom').hide();
            $('#form_filter_periodic').submit(function(event) {
                event.preventDefault();
                let url = $(this).attr('data-url');
                if ($('#time_start_periodic').val() == null && $('#time_end_periodic').val()) {
                    $('.form_filter_periodic').hide().attr('disabled', true);
                }

                $('#loader_custom').fadeIn();
                setTimeout(function() {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: url,
                        method: "post",
                        data: {
                            weekday: $('#weekday').val(),
                            date_start_periodic: $('#date_start_periodic').val(),
                            date_end_periodic: $('#date_end_periodic').val(),
                            time_order_periodic: $('#time_order_periodic').val(),
                        },
                        success: function(response) {
                            $('#loader_custom').fadeOut();
                            console.log(response);

                            if (response.hasError) {
                                $('#error').show();
                                $('#error').append(response.message);
                                $('.form_order').hide();
                                setTimeout(function() {
                                    $('#error').empty();
                                    $('#error').hide();
                                }, 4000);
                            }

                            if (!response.hasError) {
                                $('.date_hidden').val("");
                                $('.time_start_hidden').val(response.start);
                                $('.time_end_hidden').val(response.end);
                                $('.date_periodic_end').val(response.second_date);
                                $('.weekday_periodic').val(response.weekday);
                                $('.date_periodic_start').val(response.first_date);

                                let arr_pitch = response.listPitch;
                                let arr_order = response.array_pitch;
                                let count = 0;
                                let arr = [];
                                for (let i = 0; i < arr_pitch.length; i++) {
                                    for (let j = 0; j < arr_order.length; j++) {
                                        if (arr_pitch[i].id === arr_order[j]) {
                                            arr.push(arr_pitch[i].id);
                                        }
                                    }
                                }
                                if (arr.length != 0) {
                                    $('#form_order11').hide();
                                } else {
                                    $('#form_order11').show();
                                }
                                for (let i = 0; i < arr_pitch.length; i++) {
                                    if (jQuery.inArray(arr_pitch[i].id, arr) === -1) {
                                        $('#' + arr_pitch[i].id).removeClass('init');
                                        $('#' + arr_pitch[i].id).removeClass(
                                            'orderCSS');
                                        $('#' + arr_pitch[i].id).addClass('emptyCSS');
                                        $('#form_order' + arr_pitch[i].id).show();
                                    } else {
                                        $('#' + arr_pitch[i].id).removeClass('init');
                                        $('#' + arr_pitch[i].id).removeClass(
                                            'emptyCSS');
                                        $('#' + arr_pitch[i].id).addClass('orderCSS');
                                        $('#form_order' + arr_pitch[i].id).hide();
                                    }
                                }
                            }
                        },
                    });
                }, 1000);
            });
        });
    </script>
@endsection
