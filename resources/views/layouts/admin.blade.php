<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/football-icon.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ADMIN - Bóng đá Bách Khoa</title>
    <link href="{{ asset('assets') }}/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('assets') }}/css/material-dashboard.css?v=1.2.1" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-latest.js"></script>

    <style>
        .sidebar[data-active-color="orange"] li.active>a {
            background-color: #ff9800;
            box-shadow: 0 4px 20px 0px rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(255 152 0 / 40%);
            color: black;
            font-weight: bold;
        }
    </style>

    <link rel="stylesheet" href="https://site-assets.fontawesome.com/releases/v6.0.0/css/all.css">

    {{-- calendar --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css" />
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="orange" data-background-color="black"
            data-image="{{ asset('assets') }}/img/sidebar-1.jpg">
            <div class="logo">
                <a href="" class="simple-text logo-normal">
                    Bóng đá Bách Khoa
                </a>
                <a href="" class="simple-text logo-mini">
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="{{ asset('assets') }}/img/faces/faceadmin.png" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <span>
                                {{ $admin->name }}
                                <b class="caret"></b>
                            </span>
                        </a>
                        <div class="clearfix"></div>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="{{ route('profile-admin') }}">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal"> Thông tin cá nhân </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('change-password-admin') }}">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal"> Đổi mật khẩu </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav">
                    <li>
                        <a href="{{ route('dashboard') }}">
                            <i class="material-icons">dashboard</i>
                            <p> Tổng quan </p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('order-admin') }}">
                            <i class="fa-solid fa-cart-shopping-fast"></i>
                            <p> Đặt sân hộ khách </p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('bill.index') }}">
                            <i class="material-icons">content_paste</i>
                            <p> Hoá đơn
                            </p>
                        </a>
                    </li>
                    @if ($admin->role == 1)
                        <li>
                            <a data-toggle="collapse" href="#company">
                                <i class="material-icons">apartment</i>
                                <p> Công ty
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse" id="company">
                                <ul class="nav">
                                    <li>
                                        <a href="{{ route('introduce.index') }}">
                                            <i class="material-icons">apartment</i>
                                            <p> Thông tin giới thiệu
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.index') }}">
                                            <i class="material-icons">person</i>
                                            <p> Quản lý nhân viên
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('area.index') }}">
                                            <i class="material-icons">place</i>
                                            <p> Quản lý Khu vực
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin_area.index') }}">
                                            <i class="material-icons">feed</i>
                                            <p>
                                                Phân quyền quản lý khu vực
                                            </p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a data-toggle="collapse" href="#listPitch">
                                <i class="material-icons">date_range</i>
                                <p> Quản lý sân
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse" id="listPitch">
                                <ul class="nav">
                                    <li>
                                        <a href="{{ route('pitch.index') }}">
                                            <i class="fa-solid fa-list"></i>
                                            <span class="sidebar-normal"> Danh sách sân </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('category.index') }}">
                                            <i class="fa-solid fa-list-ol"></i>
                                            <span class="sidebar-normal"> Thể loại </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('maintenances.index') }}">
                                            <i class="fa-solid fa-screwdriver-wrench"></i>
                                            <span class="sidebar-normal"> Bảo trì sân </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('post.index') }}">
                            <i class="material-icons">feed</i>
                            <p> Bài viết
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('log-out-admin') }}">
                            <i class="material-icons">logout</i>
                            <p> Đăng xuất
                            </p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ route('dashboard') }}"> Dashboard </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    @if ($bill->count() != 0)
                                        <span class="notification">1</span>
                                    @endif
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        @if ($bill->count() != 0)
                                            <a href="{{ route('bill.index') }}"><b>Có hóa đơn chưa được duyệt</b></a>
                                        @else
                                            <a href="#"><b>Không có thông báo</b></a>
                                        @endif
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <div class="card-content">
                                    <h3 class="card-title" style="font-size: 30px">Quản lý</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            @yield('content')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <p class="copyright pull-right">
                        Created by Sơn - Linh
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function() {
        $("#no_account").hide();
        $("#checkbox_no_account").change(function(e) {
            e.preventDefault();
            if ($(this).is(":checked")) {
                $("#no_account").show(300);
                $("#email").hide(300);
                $("#check").val(1);
            } else {
                $("#no_account").hide(200);
                $("#email").show(200);
                $("#check").val(0);
            }
        });

        let path = window.location.href;
        $("li a").each(function() {
            let href = $(this).attr('href');
            console.log(href);
            if (path.substring(0, href.length) === href) {
                $(this).closest('li').addClass('active');
            }
        });
    });
</script>
<!--   Core JS Files   -->
<script src="{{ asset('assets') }}/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/material.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="{{ asset('assets') }}/js/arrive.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="{{ asset('assets') }}/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{ asset('assets') }}/js/moment.min.js"></script>
<!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
<script src="{{ asset('assets') }}/js/chartist.min.js"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{ asset('assets') }}/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
<script src="{{ asset('assets') }}/js/bootstrap-notify.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="{{ asset('assets') }}/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="{{ asset('assets') }}/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin, full documentation here: https://refreshless.com/nouislider/ -->
<script src="{{ asset('assets') }}/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{ asset('assets') }}/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="{{ asset('assets') }}/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin, full documentation here: https://limonte.github.io/sweetalert2/ -->
<script src="{{ asset('assets') }}/js/sweetalert2.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{ asset('assets') }}/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="{{ asset('assets') }}/js/fullcalendar.min.js"></script>
<!-- Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="{{ asset('assets') }}/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="{{ asset('assets') }}/js/material-dashboard.js?v=1.2.1"></script>



<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [5, 10, 15, -1],
                [5, 10, 15, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Tìm kiếm",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });
        demo.initCharts();

        $('.card .material-datatables label').addClass('form-group');
    });
</script>

</html>
