{{-- footer --}}
<footer class="footer footer-black footer-big">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <h5>{{ $introduce->company }}</h5>
                    <p>{{ $introduce->content }}</p>
                </div>

                <div class="col-md-4">
                    <h5>Fanpage</h5>
                    <div class="social-feed">
                        <div class="feed-line">
                            <a href="https://www.facebook.com/bkfootball.vn" target="_blank" rel="noopener noreferrer">
                                <i class="fa-brands fa-facebook-square"></i>
                                <p>Bóng đá Bách Khoa</p>
                            </a>
                        </div>
                        {{-- <div class="feed-line">
                            <i class="fa-brands fa-instagram-square"></i>
                            <p>INSTAGRAM</p>
                        </div>
                        <div class="feed-line">
                            <i class="fa-brands fa-skype"></i>
                            <p>SKYPE</p>
                        </div> --}}
                    </div>
                </div>

                <div class="col-md-4">
                    <h5>Địa chỉ sân trên bản đồ</h5>
                    <div class="gallery-feed">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.748052772711!2d105.84537481429739!3d21.002734194043736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac7411ccafb1%3A0x8b64e3a9974068e2!2zU8OibiBW4bqtbiDEkOG7mW5nIELDoWNoIEtob2EgLSBMw6ogVGhhbmggTmdo4buL!5e0!3m2!1svi!2s!4v1648273006417!5m2!1svi!2s"
                            width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="copyright pull-right">
            Created by Sơn - Linh
        </div>
    </div>
</footer>
