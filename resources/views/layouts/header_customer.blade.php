{{-- navbar --}}
<style>
    .button {
        background: rgb(255, 187, 0);
        font-weight: bold !important;
        color: black !important;
        margin-right: 3px !important;
        border: 2px solid #ffffff !important;
        transition-duration: 0.5s !important;
    }

    .button:hover {
        background-color: white !important;
        color: black !important;
    }

    .parallelogram {
        transform: skew(-20deg);
    }

    .skew-fix {
        display: inline-block;
        transform: skew(20deg);
    }
</style>
<nav class="navbar navbar-primary navbar-fixed-top navbar-color-on" id="sectionsNav">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">BÓNG ĐÁ BÁCH KHOA</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ route('about-us') }}">
                        <i class="material-icons">apps</i> Giới thiệu
                    </a>
                </li>
                {{-- <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">view_day</i> Khu vực
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu dropdown-with-icons scroll">
                        @foreach ($listArea as $area)
                            <li>
                                <a href="/pitch_page/{{ $area->id }}">
                                    <i class="material-icons">location_on</i> {{ $area->area_name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li> --}}
                <li>
                    <a href="{{ route('contact-us') }}">
                        <i class="material-icons">perm_phone_msg</i> Liên hệ
                    </a>
                </li>
                <li>
                    <a href="{{ route('show-cart') }}">
                        <span class="material-icons">
                            shopping_basket
                        </span> Hàng chờ
                    </a>
                </li>
                @if (Auth::user() == '')
                    <li class="button-container">
                        <a href="/login-customer" class="button parallelogram">
                            <span class="skew-fix"> Đăng nhập </span>
                        </a>
                    </li>
                    <li class="button-container">
                        <a href="{{ route('customer-register') }}" class="button parallelogram">
                            <span class="skew-fix"> Đăng ký </span>
                        </a>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">account_circle</i> {{ Auth::user()->name }}
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-with-icons">
                            <li>
                                <a href="/profile_customer">
                                    <i class="material-icons">account_circle</i> Thông tin cá nhân
                                </a>
                            </li>
                            @if (Auth::user()->password != '')
                                <li>
                                    <a href="{{ route('change-pass-cus') }}">
                                        <i class="material-icons">vpn_key</i> Đổi mật khẩu
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('bill-page') }}">
                                    <i class="material-icons">assignment</i> Hoá đơn
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('log-out') }}">
                                    <i class="material-icons">logout</i> Đăng xuất
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>