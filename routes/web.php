<?php

use App\Http\Middleware\CheckRole;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

use App\Http\Middleware\CheckLoginAdmin;
use App\Http\Middleware\CheckLoginCustomer;
use App\Http\Controllers\Admin_manager\AreaController;
use App\Http\Controllers\Admin_manager\BillController;
use App\Http\Controllers\Admin_manager\PostController;
use App\Http\Controllers\Admin_manager\AdminController;
use App\Http\Controllers\Admin_manager\PitchController;
use App\Http\Controllers\Admin_manager\CategoryController;
use App\Http\Controllers\Admin_manager\CustomerController;
use App\Http\Controllers\Customer_manager\OrderController;
use App\Http\Controllers\Admin_manager\AdminAreaController;
use App\Http\Controllers\Admin_manager\DashboardController;
use App\Http\Controllers\Admin_manager\IntroduceController;
use App\Http\Controllers\Customer_manager\SocialController;

use App\Http\Controllers\Admin_manager\OrderAdminController;
use App\Http\Controllers\Customer_manager\ContactController;
use App\Http\Controllers\Admin_manager\MaintenanceController;
use App\Http\Controllers\Admin_manager\ProfileAdminController;
use App\Http\Controllers\Customer_manager\BillCustomerController;
use App\Http\Controllers\Customer_manager\PitchCustomerController;
use App\Http\Controllers\Admin_manager\AdminAuthenticateController;
use App\Http\Controllers\Admin_manager\BillPeriodicController;
use App\Http\Controllers\Customer_manager\ProfileCustomerController;
use App\Http\Controllers\Customer_manager\IntroduceCustomerController;
use App\Http\Controllers\Customer_manager\CustomerAuthenticateController;
use App\Models\BillPeriodic;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (App::environment() == 'production') {
    URL::forceScheme('https');
}

//home
Route::get('/', [HomeController::class, 'index'])->name('home');

//login admin
Route::get('/login-admin', [AdminAuthenticateController::class, 'indexLogin'])->name('login-admin');
Route::post('/login-admin-process', [AdminAuthenticateController::class, 'login'])->name('login-admin-process');


//quen mat khau admin
Route::get('/forget_password_admin', [AdminAuthenticateController::class, 'indexForgetPass']);
Route::post('/process_forget_pass_admin', [AdminAuthenticateController::class, 'forgetPassProcess']);

/**
 * login customer
 */
Route::get('/login-customer', [CustomerAuthenticateController::class, 'indexLogin'])->name('login-customer');
Route::post('/login', [CustomerAuthenticateController::class, 'login'])->name('login');

/**
 * quen mat khau khach hang
 */
Route::get('/forget_password', [CustomerAuthenticateController::class, 'forgetPassView'])->name('forget-pass-cus');
Route::post('/process_forget_pass_customer', [CustomerAuthenticateController::class, 'forgetPassCus']);
/**
 * Customer Register
 */
Route::get('/customer-register', [CustomerAuthenticateController::class, 'indexRegister'])->name('customer-register');
Route::post('/register', [CustomerAuthenticateController::class, 'register'])->name('register');
Route::get('/verify-register', [CustomerAuthenticateController::class, 'verifyRegister'])->name('verify-register');

/**
 * Login with google
 */
Route::get('login/google', [SocialController::class, 'redirectToGoogle'])->name('login-google');
Route::get('callback/google', [SocialController::class, 'callbackGoogle']);

//web
Route::get('/pitch_page/{id}', [PitchCustomerController::class, 'index'])->name('pitch_page');

//introduce
Route::get('/about-us', [IntroduceCustomerController::class, 'index'])->name('about-us');
Route::get('/contact-us', [ContactController::class, 'index'])->name('contact-us');

//post detail
Route::get('/post_detail/{id}', [HomeController::class, 'postDetail'])->name('post-detail');

//filter
Route::post('/pitch-filter', [PitchCustomerController::class, 'pitchFilter'])->name('pitch-filter');

//filter periodic
Route::post('/pitch-filter-periodic', [PitchCustomerController::class, 'filterOrderPeriodic'])->name('pitch-filter-periodic');

//pitch detail
Route::get('/pitch-detail/{area_id}/{pitch_id}', [PitchCustomerController::class, 'pitchDetail'])->name('pitch-detail');


//customer
Route::middleware([CheckLoginCustomer::class])->group(function () {
    //confirm order
    Route::get('/confirm', [OrderController::class, 'confirm'])->name('confirm');

    //confirm order periodic
    Route::get('/confirm-periodic', [OrderController::class, 'confirmPeriodic'])->name('confirm-periodic');

    //form order
    Route::post('/order/{area_id}/{pitch_id}', [OrderController::class, 'index'])->name('order');

    //order single
    Route::post('/order_process', [OrderController::class, 'store'])->name('order-process');
    //orderCart
    Route::post('/order_cart_process', [OrderController::class, 'storeCart'])->name('order-cart-process');

    // bill
    Route::get('/bill_page', [BillCustomerController::class, 'index'])->name('bill-page');
    Route::get('/bill_detail/{id}', [BillCustomerController::class, 'detail'])->name('bill-detail-customer');
    Route::delete('/destroy_process/{bill_id}', [BillCustomerController::class, 'destroy_process'])->name('bill_destroy');

    //bill periodic
    Route::get('/bill_periodic', [BillCustomerController::class, 'billPeriodic'])->name('bill-periodic-customer');
    Route::get('/bill_periodic_detail/{id}', [BillCustomerController::class, 'billPeriodicDetail'])->name('bill-periodic-detail-customer');
    Route::delete('/destroy_periodic/{id}', [BillCustomerController::class, 'destroyPeriodic'])->name('bill-periodic-destroy-customer');

    // Route::delete('/bill/destroy/{id}', [BillController::class, 'destroyPeriodicSingle'])->name('bill-periodic-destroy-single');

    // profile customer
    Route::resource('profile_customer', ProfileCustomerController::class);

    // change pass
    Route::get('/change_password', [CustomerAuthenticateController::class, 'changePassView'])->name('change-pass-cus');
    Route::post('/change_password_process/{id}', [CustomerAuthenticateController::class, 'changePassCus'])->name('change-pass-cus-process');

    //add to cart
    Route::get('/pitch/add-to-cart/{id}', [PitchCustomerController::class, 'addToCart'])->name('add-to-cart');
    Route::get('/pitch/show-cart', [PitchCustomerController::class, 'showCart'])->name('show-cart');
    Route::delete('/pitch/delete-cart-item/{id}', [PitchCustomerController::class, 'deleteCartItem'])->name('delete-cart-item');

    //logout
    Route::get('/log_out', [CustomerAuthenticateController::class, 'logout'])->name('log-out');

    //send bill to mail
    Route::get('send-bill-pdf-to-mail', [OrderController::class, 'billPdf'])->name('send-bill-pdf-to-mail');

    //point
    Route::post('point', [OrderController::class, 'point'])->name('point');
    //point cart
    Route::post('point-cart', [OrderController::class, 'point_cart'])->name('point-cart');
});

Route::get('/pdf', [OrderController::class, 'pdf']);

//admin 
Route::middleware([CheckLoginAdmin::class])->group(function () {
    //dashboard
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // chart doanh thu
    Route::post('/chart-filter', [DashboardController::class, 'chartFilter'])->name('chart-filter');

    // supperadmin
    Route::middleware([CheckRole::class])->group(function () {
        Route::resource('admin', AdminController::class);
        Route::resource('area', AreaController::class);
        Route::resource('introduce', IntroduceController::class);
        Route::resource('admin_area', AdminAreaController::class);

        Route::post('/pitch-filter-periodic-admin', [OrderAdminController::class, 'filterOrderPeriodic'])->name('pitch-filter-periodic-admin');
    });
    //pitch filter
    Route::post('/pitch_filter', [OrderAdminController::class, 'pitchFilter'])->name('pitch-filter-admin');

    //order
    Route::get('/order_admin', [OrderAdminController::class, 'index'])->name('order-admin');
    Route::post('/order_admin_form/{area_id}/{pitch_id}', [OrderAdminController::class, 'formOrder'])->name('order-admin-form');
    Route::post('/order_admin_process', [OrderAdminController::class, 'store'])->name('order-admin-process');
    Route::get('/order_admin_confirm/{id}', [OrderAdminController::class, 'confirm'])->name('order-admin-confirm');

    //pdf
    Route::get('/export-pdf/{id}', [BillController::class, 'exportPDF'])->name('export-pdf');

    //export pdf have account
    Route::get('/export-bill-pdf/{id}', [BillController::class, 'exportBillPDFAccount'])->name('export-bill-pdf');

    //bill periodic
    Route::get('/export-bill-periodic-pdf/{id}', [BillPeriodicController::class, 'exportBillPDFPeriodic'])->name('export-bill-pdf-periodic');
    
    //bill periodic no account
    Route::get('/export-bill-periodic-pdf-no-account/{id}', [BillPeriodicController::class, 'exportBillPeriodicPDFNoAccount'])->name('export-bill-periodic-pdf-no-account');

    //export pdf no account
    Route::get('/export-bill-pdf-no-account/{id}', [BillController::class, 'exportBillPDFNoAccount'])->name('export-bill-pdf-no-account');

    Route::resource('customer', CustomerController::class);

    // CRUD pitch
    Route::resource('pitch', PitchController::class);

    // status
    Route::get('status', [PitchController::class, 'statusIndex'])->name('status');
    Route::get('status_create', [PitchController::class, 'statusCreate'])->name('status_create');
    Route::post('status_store', [PitchController::class, 'statusStore'])->name('status_store');
    Route::get('status/edit/{id}', [PitchController::class, 'statusEdit'])->name('status_edit');
    Route::put('status/update/{id}', [PitchController::class, 'statusUpdate'])->name('status_update');


    // bill
    Route::resource('bill', BillController::class);
    Route::get('/bill-detail/{id}', [BillController::class, 'detail'])->name('bill-detail');
    //destroy periodic
    Route::delete('/bill/destroy/{id}', [BillController::class, 'destroyPeriodicSingle'])->name('bill-periodic-destroy-single');

    //bill periodic
    Route::get('/bill-periodic', [BillPeriodicController::class, 'index'])->name('bill-periodic');
    Route::get('/bill-periodic-detail/{id}', [BillPeriodicController::class, 'detail'])->name('bill-periodic-detail');
    Route::put('/bill-periodic/update/{id}', [BillPeriodicController::class, 'update'])->name('bill-periodic-update');
    Route::delete('/bill-periodic/destroy/{id}', [BillPeriodicController::class, 'destroy'])->name('bill-periodic-destroy');

    // CRUD category
    Route::resource('category', CategoryController::class);

    // CRUD maintenances
    Route::resource('maintenances', MaintenanceController::class);

    //CRUD post
    Route::resource('post', PostController::class);

    /**
     * profile admin
     */
    Route::get('/profile_admin', [ProfileAdminController::class, 'index'])->name('profile-admin');
    Route::get('/edit-profile-admin/{id}', [ProfileAdminController::class, 'edit'])->name('edit-profile-admin');
    Route::put('/edit-admin/{id}', [ProfileAdminController::class, 'update'])->name('edit-admin');

    /**
     * Change pass admin
     */
    Route::get('/change_password_admin', [AdminAuthenticateController::class, 'indexChangePass'])->name('change-password-admin');
    Route::post('/change-password-admin-process/{id}', [AdminAuthenticateController::class, 'changePassAdmin'])->name('change-password-admin-process');

    //logout
    Route::get('/log_out_admin', [AdminAuthenticateController::class, 'logOut'])->name('log-out-admin');
});
